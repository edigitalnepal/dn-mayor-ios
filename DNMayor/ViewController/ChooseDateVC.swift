//
//  ChooseDateVC.swift
//  DNMayor
//
//  Created by mac on 8/19/19.
//  Copyright © 2019 E-Digital Nepal. All rights reserved.
//

import UIKit
import DropDown

class ChooseDateVC: UIViewController {

    
    @IBOutlet var view_main: UIView!
    @IBOutlet var year_btn: UIButton!
    @IBOutlet var month_btn: UIButton!
    @IBOutlet var day_btn: UIButton!
    @IBOutlet var select_btn: UIButton!
    @IBOutlet var close_btn: UIButton!
    var status:Int!
    var callfrom:Int!
    
    let dropDownyear = DropDown()
    let dropDownmonth = DropDown()
    let dropDownday = DropDown()
    var date = ""
    var yearr = ""
    var monthh = ""
    var dayy = ""
    let year = ["2073","2074","2075", "2076", "2077"]
    let month=["Baishakh", "Jestha", "Ashad","Shrawan", "Bhadra", "Ashoj","Kartik", "Mangsir", "Push","Magh", "Falgun", "Chaitra"]
    let day32 = ["1" , "2" , "3","4" , "5" , "6","7" , "8" , "9","10" , "11" , "12","13" , "14" , "15","16" , "17" , "18","19" , "20" , "21","22" , "23" , "24","25" , "26" , "27","28" , "29" , "30","31" , "32"]
    let day31 = ["1" , "2" , "3","4" , "5" , "6","7" , "8" , "9","10" , "11" , "12","13" , "14" , "15","16" , "17" , "18","19" , "20" , "21","22" , "23" , "24","25" , "26" , "27","28" , "29" , "30","31"]
    let day30 = ["1" , "2" , "3","4" , "5" , "6","7" , "8" , "9","10" , "11" , "12","13" , "14" , "15","16" , "17" , "18","19" , "20" , "21","22" , "23" , "24","25" , "26" , "27","28" , "29" , "30"]
    let day29 = ["1" , "2" , "3","4" , "5" , "6","7" , "8" , "9","10" , "11" , "12","13" , "14" , "15","16" , "17" , "18","19" , "20" , "21","22" , "23" , "24","25" , "26" , "27","28" , "29"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dropDownyear.anchorView = year_btn
        dropDownmonth.anchorView = month_btn
        dropDownday.anchorView = day_btn
        dropDownday.dataSource = day32
        dropDownmonth.dataSource = month
        dropDownyear.dataSource = year
        yearr = DateInString.getCurrentYearNepali()
        let m = DateInString.getCurrentMonthNepali()
        monthh = String(m)
        dayy = DateInString.getCurrentDayNepali()
        self.year_btn.setTitle(yearr, for: .normal)
        self.month_btn.setTitle(month[m-1], for: .normal)
        self.day_btn.setTitle(dayy, for: .normal)
        
        view_main.layer.shadowRadius = 50
        view_main.layer.shadowColor = Color.lightGreay.cgColor
        view_main.layer.shadowOpacity = 1
        makeupButton()
        
        dropDownyear.selectionAction = { [unowned self] (index: Int, item: String) in
            self.year_btn.setTitle(item, for: .normal)
            self.yearr = item
            
            
        }
        dropDownmonth.selectionAction = { [unowned self] (index: Int, item: String) in
            self.month_btn.setTitle(item, for: .normal)
            self.monthh = String(index+1)
        }
        dropDownday.selectionAction = { [unowned self] (index: Int, item: String) in
            self.day_btn.setTitle(item, for: .normal)
            self.dayy = item
            
        }
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    func makeupButton(){
        select_btn.layer.backgroundColor = UIColor.white.cgColor
        select_btn.layer.cornerRadius = 5
        select_btn.layer.borderWidth = 1
        select_btn.layer.borderColor = Color.Blue.cgColor
        
        
        year_btn.layer.backgroundColor = UIColor.white.cgColor
        year_btn.layer.cornerRadius = 5
        year_btn.layer.borderWidth = 1
        year_btn.layer.borderColor = UIColor.black.cgColor
        
        month_btn.layer.backgroundColor = UIColor.white.cgColor
        month_btn.layer.cornerRadius = 5
        month_btn.layer.borderWidth = 1
        month_btn.layer.borderColor = UIColor.black.cgColor
        
        day_btn.layer.backgroundColor = UIColor.white.cgColor
        day_btn.layer.cornerRadius = 5
        day_btn.layer.borderWidth = 1
        day_btn.layer.borderColor = UIColor.black.cgColor
        
        
    }
    
    @IBAction func day_click(_ sender: Any) {
        dropDownday.show()
        
    }
    
    @IBAction func month_click(_ sender: Any) {
        dropDownmonth.show()
    }
    
    @IBAction func year_click(_ sender: Any) {
        dropDownyear.show()
    }
    
    @IBAction func close_click(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func select_click(_ sender: Any) {
        if yearr != "" && monthh != "" && dayy != ""{
            date = yearr+"-"+monthh+"-"+dayy
            if callfrom == 1{
                let dateinfo:[String: String] = ["date": date]
                nc.post(name: Notification.Name("adate"), object: nil, userInfo:dateinfo)
                dismiss(animated: true, completion: nil)
            }else{
                let dateinfo:[String: String] = ["date": date]
                nc.post(name: Notification.Name("tldate"), object: nil, userInfo:dateinfo)
                dismiss(animated: true, completion: nil)
            }
            
        }else{
            
            LibraryToast.notifyUser("ALERT", message:"Select Year,Month and Day" , vc: self)
        }
        
    }
    
    
    
}


