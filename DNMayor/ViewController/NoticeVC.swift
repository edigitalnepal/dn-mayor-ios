//
//  NoticeVC.swift
//  DNFounder
//
//  Created by Er. Aditya Raj on 5/13/18.
//  Copyright © 2018 Digital Nepal. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import DZNEmptyDataSet

class NoticeVC: UIViewController {




   
    let cellIdentifier = "NoticeCell"
    var responseData :[JSON] = [JSON]()
    var totolcount = 0
    var pageNumber = 10
    var type = Int()
    var refresh:Bool = false
    var refreshControl = UIRefreshControl()
      let error: UIImage = UIImage(named: "notice")!
    
    
    @IBOutlet weak var tableview_BeyaTable: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
      self.setNavigationBarItem()
        title="Notices"
        print("_________________" + String(self.type))
        self.refreshControl.attributedTitle = NSAttributedString(string: "Refreshing")
        refreshControl.addTarget(self, action: #selector(refresh(sender:)), for: .valueChanged)
        self.tableview_BeyaTable?.addSubview(refreshControl)
        
        //tabel view setup
        tableview_BeyaTable.delegate = self
        tableview_BeyaTable.dataSource = self
        tableview_BeyaTable.register(UINib(nibName: "NoticeCell", bundle: Bundle.main), forCellReuseIdentifier: cellIdentifier)
        tableview_BeyaTable.estimatedRowHeight = 60.0
        tableview_BeyaTable.rowHeight = UITableViewAutomaticDimension
        loadFromAPI(offSetNormal:10)
       
        
        // Do any additional setup after loading the view.
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @objc func refresh(sender:AnyObject) {
        pageNumber = 10
        refresh=true
        responseData.removeAll()
            loadFromAPI(offSetNormal:10)
       
        
        tableview_BeyaTable.reloadData()
        
    }
    func loadFromAPI(offSetNormal:Int){
        if refresh {
            
        }else{
            showHud("Loading...")
        }
        
        var paramater = [String:AnyObject]()
        let id = userDefaults.integer(forKey: "teamId")
      
            paramater = ["team_id":id as AnyObject,"start":offSetNormal-10 as AnyObject,"limit":10 as AnyObject]
       
        print(paramater)
        Alamofire.request(LibraryAPI.NoticUrl, method: .post, parameters: paramater)
            
            
            .responseJSON(completionHandler: { (response) -> Void in
                print(response)
                if(response.result.isSuccess) {
                    
                    let json = JSON(response.data)
                    self.totolcount = json["total"].intValue
                    print(self.totolcount)
                    let value : JSON! = json["notices"]
                    
                    if offSetNormal == 10 {
                        self.responseData.removeAll()
                        self.pageNumber=10
                    }
                    if let news = value {
                        for (_, subJson) in news {
                                                  print(subJson)
                            self.responseData.append(subJson)
                        }
                    }
        
                    self.tableview_BeyaTable.reloadData()
                    
                    
                    
                    self.hideHUD()
                } else{
                    self.hideHUD()
                     LibraryToast.notifyUser("ALERT", message:"Something Went Wrong Please Try Again" , vc: self)
                    
                    
                }
                self.refreshControl.endRefreshing()
                
                
            })
        self.tableview_BeyaTable.emptyDataSetSource = self
        self.tableview_BeyaTable.emptyDataSetDelegate = self
        refresh = false
        
        
    }
    
    func OpenNoticeFullView(id:Int!){

        let popOverVC = storyboard?.instantiateViewController(withIdentifier: "NoticeFullViewPopup") as! NoticeFullViewPopup
        popOverVC.date = responseData[id]["notice_date"] .stringValue
        popOverVC.heading = responseData[id]["heading"] .stringValue
        popOverVC.details = responseData[id]["message"] .stringValue
        self.navigationController?.pushViewController(popOverVC, animated: true)
    }
    
    
   
    
}
extension NoticeVC :UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return responseData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? NoticeCell
        cell?.selectionStyle = UITableViewCellSelectionStyle.none
        
        cell?.label_Date.text = DateInString.stringDate(date:  responseData[indexPath.row]["notice_date"] .stringValue)
        cell?.textview_Notice.text = responseData[indexPath.row]["heading"] .stringValue
        
        
        return cell!
    }
    
    
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
       OpenNoticeFullView(id: indexPath.row)
        
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == responseData.count - 1 { // last cell
         print(self.totolcount)
            print(self.responseData.count)
            if   self.totolcount > responseData.count{ // more items to fetch
                self.pageNumber = self.pageNumber+10
                loadFromAPI(offSetNormal: self.pageNumber)
            }
        }
 }
    
}
extension NoticeVC : DZNEmptyDataSetDelegate, DZNEmptyDataSetSource {
    
    
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let str = ""
        let attrs = [NSAttributedStringKey.font: UIFont.preferredFont(forTextStyle: UIFontTextStyle.headline)]
        return NSAttributedString(string: str, attributes: attrs)
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let str = "No Data Available"
        let attrs = [NSAttributedStringKey.font: UIFont.preferredFont(forTextStyle: UIFontTextStyle.body)]
        return NSAttributedString(string: str, attributes: attrs)
    }
    
    func image(forEmptyDataSet scrollView: UIScrollView) -> UIImage!{
        
        return  error
    }
    
    
    func buttonTitle(forEmptyDataSet scrollView: UIScrollView!, for state: UIControlState) -> NSAttributedString! {
        let str = "RELOAD"
        let attrs = [NSAttributedStringKey.font: UIFont.preferredFont(forTextStyle: UIFontTextStyle.body)]
        return NSAttributedString(string: str, attributes: attrs)
    }
 
    
    func emptyDataSetDidTapButton(_ scrollView: UIScrollView!) {
        refresh = false
        responseData.removeAll()
       loadFromAPI(offSetNormal:10)
       
    }
}
    




