//
//  ProfileViewController.swift
//  DNMayor
//
//  Created by mac on 8/19/19.
//  Copyright © 2019 E-Digital Nepal. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController {

    @IBOutlet var label_signupdate: UILabel!
    @IBOutlet var label_role: UILabel!
    
    @IBOutlet weak var label_address: UILabel!
    
    @IBOutlet weak var phone: UILabel!
    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var profile_Image: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationBarItem()
        self.title = "Profile"
        self.profile_Image.layer.cornerRadius = self.profile_Image.frame.size.width/2
        self.profile_Image.clipsToBounds = true
        if userDefaults.string(forKey: "image") != "" {
            let url = URL(string: "http://202.51.74.174/mis/files/school-logos/\(userDefaults.string(forKey: "image") ?? "")")
            self.profile_Image.kf.setImage(with: url)
        }
        name.text = userDefaults.string(forKey:  "name")
        
        //         college.text = userDefaults.string(forKey:  "school_name")
        phone.text = userDefaults.string(forKey:  "phone")
        
        if userDefaults.string(forKey:  "email") != ""{
            email.text = userDefaults.string(forKey:  "email")!
        }
        if userDefaults.string(forKey:  "mobile") != ""{
            label_role.text = userDefaults.string(forKey:  "mobile")!
        }
        if userDefaults.string(forKey:  "address") != ""{
            label_address.text = userDefaults.string(forKey:  "address")!
        }
//        if userDefaults.string(forKey:  "signedup_date") != ""{
//            let date = userDefaults.string(forKey:"signedup_date")
//            var dateonly = date!.components(separatedBy: " ")
//            self.label_signupdate.text = DateInString.stringDate(date: dateonly[0])
//        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

