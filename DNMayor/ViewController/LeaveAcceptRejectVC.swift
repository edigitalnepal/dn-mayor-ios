//
//  LeaveAcceptRejectVC.swift
//  DNFounder
//
//  Created by Er. Aditya Raj on 7/5/18.
//  Copyright © 2018 Digital Nepal. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class LeaveAcceptRejectVC: UIViewController {
    var data = JSON()
    @IBOutlet weak var date: UILabel!
    
    @IBOutlet var reject_btn: UIButton!
    @IBOutlet var accept_btn: UIButton!
    @IBOutlet weak var reason: UITextView!
    @IBOutlet weak var status: UILabel!
    @IBOutlet weak var by: UILabel!
    @IBOutlet weak var numberofleave: UILabel!
    @IBOutlet weak var type: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        makeUpButton()
        self.date.text = DateInString.stringDate(date: data["requested_date"].stringValue)
        self.numberofleave.text = String(data["no_of_leave_days"].intValue)
        self.reason.text = data["reason"].stringValue
        self.by.text = data["requested_by"].stringValue
        self.type.text = data["leave_type"].stringValue
         self.status.text = data["status"].stringValue
        if data["status"].stringValue == "Rejected" {
            self.status.textColor = Color.total_absent_student
            reject_btn.isHidden = true
            accept_btn.isHidden = false
            
        }else if data["status"].stringValue == "Approved" {
            self.status.textColor = Color.total_present
            accept_btn.isHidden = true
            reject_btn.isHidden = false
        }else{
            self.status.textColor = Color.disscount
            accept_btn.isHidden = false
            reject_btn.isHidden = false
        }
        

        // Do any additional setup after loading the view.
    }
    func makeUpButton(){
        reject_btn.layer.backgroundColor = UIColor.white.cgColor
        reject_btn.layer.shadowColor = UIColor.gray.cgColor
        reject_btn.layer.borderColor = Color.total_absent_student.cgColor
        reject_btn.layer.cornerRadius = 5
        reject_btn.layer.borderWidth = 1
        
        accept_btn.layer.backgroundColor = UIColor.white.cgColor
        accept_btn.layer.shadowColor = UIColor.gray.cgColor
        accept_btn.layer.borderColor = Color.total_present.cgColor
        accept_btn.layer.cornerRadius = 5
        accept_btn.layer.borderWidth = 1
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func cancelBtn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func loadFromAPI(status:String){
      
            showHud("Loading...")
       
        var paramater = [String:AnyObject]()
        let id = userDefaults.integer(forKey:  "Id")
        
        paramater = ["application_id":data["id"].intValue as AnyObject,"founder_id":id as AnyObject,"action":status as AnyObject]
        
        print(paramater)
        Alamofire.request(LibraryAPI.takeAction, method: .post, parameters: paramater)
            
            
            .responseJSON(completionHandler: { (response) -> Void in
                print(response)
                if(response.result.isSuccess) {
                    
                    let json = JSON(response.data)
                    let mesage = json["message"].stringValue
                    let alert = UIAlertController(title: "ALERT!  ", message: mesage, preferredStyle: .alert)
                    
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler:{ action in
                          nc.post(name: Notification.Name("gotoLeave"), object: nil)
                        self.dismiss(animated: true, completion: nil)
                    }))
                    
                    
                    self.present(alert, animated: true)
                    
                   
                self.hideHUD()
                } else{
                    self.hideHUD()
                     self.dismiss(animated: true, completion: nil)
                    LibraryToast.notifyUser("ALERT", message:"Something Went Wrong Please Try Again" , vc: self)
                    
                    
                }
            
                
                
            })
      
        
        
    }
    
    

    @IBAction func Reject(_ sender: Any) {
        loadFromAPI(status: "reject")
    }
    @IBAction func Accept(_ sender: Any) {
        loadFromAPI(status: "approve")
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
