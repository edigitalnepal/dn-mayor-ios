//
//  AttendanceMainVC.swift
//  DNFounder
//
//  Created by Er. Aditya Raj on 7/6/18.
//  Copyright © 2018 Digital Nepal. All rights reserved.
//

import UIKit
import Parchment

class AttendanceMainVC: UIViewController {
  var type = Int()
    
    @IBOutlet weak var viewMain: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavigationBarItem()
         self.title="Attendance Report"
        loadSlider(initial: 0)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func loadSlider(initial:Int) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let firstViewController = storyboard.instantiateViewController(withIdentifier: "AttendanceReportVC") as! AttendanceReportVC
        let secondViewController = storyboard.instantiateViewController(withIdentifier: "EmployeeAttendanceReportVC") as! EmployeeAttendanceReportVC
        firstViewController.type = type
        secondViewController.type = type
        let pagingViewController = FixedPagingViewController(viewControllers: [
            firstViewController,
            secondViewController
            ])
        
        // Make sure you add the PagingViewController as a child view
        // controller and contrain it to the edges of the view.
        addChildViewController(pagingViewController)
        viewMain.addSubview(pagingViewController.view)
        viewMain.constrainToEdges(pagingViewController.view)
        pagingViewController.didMove(toParentViewController: self)
        
        
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
