//
//  DailyRoutineViewController.swift
//  DNFounder
//
//  Created by Er. Aditya Raj on 5/14/18.
//  Copyright © 2018 Digital Nepal. All rights reserved.
//

import UIKit
import  Charts
import  Alamofire
import  SwiftyJSON

class DailyRoutineViewController: UIViewController {

    @IBOutlet weak var label_nodata: UILabel!
    @IBOutlet weak var image_Routine: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavigationBarItem()
        title="DailyRoutine"
      
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
          loadFromAPI(offSetNormal: 0)
    }
    func loadFromAPI(offSetNormal:Int){
        
        showHud("Loading...")
        
        var paramater = [String:AnyObject]()
        let id=userDefaults.integer(forKey:"teamId")
        
        paramater = ["team_id":id as AnyObject]
        
        print(paramater)
        Alamofire.request(LibraryAPI.dailyRoutineUrl, method: .post, parameters: paramater)
            
            .responseJSON(completionHandler: { (response) -> Void in
                print(response)
                if(response.result.isSuccess) {
                    let json = JSON(response.data)
               
                        //displaying image
                        let encodedImageData = json["image"].stringValue
                        let dataDecoded : Data = Data(base64Encoded: encodedImageData, options: .ignoreUnknownCharacters)!
                        let decodedimage = UIImage(data: dataDecoded)
                        self.image_Routine.image = decodedimage
                 
                 
                   
                    
                  self.hideHUD()
                } else{
                    self.hideHUD()
                    LibraryToast.notifyUser("ALERT", message:"Internet Or Sever Problem" , vc: self)
                    
                    
                }
                
                
                
            })
        
        
        
    }
    



}
