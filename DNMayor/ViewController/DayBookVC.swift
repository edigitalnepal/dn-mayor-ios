//
//  DayBookVC.swift
//  DNMayor
//
//  Created by mac on 8/19/19.
//  Copyright © 2019 E-Digital Nepal. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class DayBookVC: UIViewController {
    
    let cellIdentifier = "DaybookCashbook"
    let cellIdentifireBankTransactions = "BankTransactionTC"
    var responseData :[JSON] = [JSON]()
    var total:Decimal = 0
    var titleText = ""
    var type = 0
    @IBOutlet weak var l_TotalAmount: UITextView!
    
    @IBOutlet weak var tableview_DayBook: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = titleText+"(Rs.\(total))"
        //        l_TotalAmount.text = "Total Amount Rs. \(total)"
        
        //tabel view setup
        tableview_DayBook.delegate = self
        tableview_DayBook.dataSource = self
        tableview_DayBook.register(UINib(nibName: "DaybookCashbook", bundle: Bundle.main), forCellReuseIdentifier: cellIdentifier)
        tableview_DayBook.register(UINib(nibName: "BankTransactionTC", bundle: Bundle.main), forCellReuseIdentifier: cellIdentifireBankTransactions)
        tableview_DayBook.estimatedRowHeight = 40.0
        tableview_DayBook.rowHeight = UITableViewAutomaticDimension
        tableview_DayBook.reloadData()
        
    }
    
    
    
}
extension DayBookVC :UITableViewDataSource, UITableViewDelegate{
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return responseData.count+1
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if type != 3{
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? DaybookCashbook
            cell?.selectionStyle = UITableViewCellSelectionStyle.none
            
            if indexPath.row == 0{
                cell?.label_Sn.text = "SN"
                cell?.label_Sn.textColor = UIColor.black
                cell?.label_Date.text = "Date"
                cell?.label_Date.textColor = UIColor.black
                cell?.label_Particular.text = "Particular"
                cell?.label_Particular.textColor = UIColor.black
                cell?.label_Narration.text = "Narration"
                cell?.label_Narration.textColor = UIColor.black
                cell?.label_Amount.text = "Amount(Rs.)"
                cell?.label_Amount.textColor = UIColor.black
            }else{
                
                cell?.label_Sn.text = "\(indexPath.row)"
                if type == 1{
                    cell?.label_Amount.text = responseData[indexPath.row-1]["receivedAmount"].stringValue
                }else{
                    
                    cell?.label_Amount.text = responseData[indexPath.row-1]["paidAmount"].stringValue
                }
                if self.responseData[indexPath.row-1]["createdDate"].stringValue != ""{
                    cell?.label_Date.text = DateInString.stringDate(date:  self.responseData[indexPath.row-1]["createdDate"].stringValue)
                }else{
                    cell?.label_Date.text = DateInString.stringDate(date:  self.responseData[indexPath.row-1]["entryDate"].stringValue)
                }
                cell?.label_Narration.text = responseData[indexPath.row-1]["narration"].stringValue
                cell?.label_Particular.text = responseData[indexPath.row-1]["products"].stringValue
                
            }
            return cell!
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifireBankTransactions, for: indexPath) as? BankTransactionTC
            cell?.selectionStyle = UITableViewCellSelectionStyle.none
            if indexPath.row == 0{
                cell?.l_Date.text = "Date"
                cell?.l_Date.textColor = UIColor.black
                cell?.l_BankName.text = "Bank Name"
                cell?.l_BankName.textColor = UIColor.black
                cell?.l_BankAccount.text = "Bank Account"
                cell?.l_BankAccount.textColor = UIColor.black
                cell?.l_ConductedBy.text = "Conducted By"
                cell?.l_ConductedBy.textColor = UIColor.black
                cell?.l_Transactions.text = "Transactions"
                cell?.l_Transactions.textColor = UIColor.black
                cell?.l_Amount.text = "Amount(Rs.)"
                cell?.l_Amount.textColor = UIColor.black
                
            }else{
                
                cell?.l_Date.textColor = UIColor.gray
                cell?.l_BankName.textColor = UIColor.gray
                cell?.l_BankAccount.textColor = UIColor.gray
                cell?.l_ConductedBy.textColor = UIColor.gray
                cell?.l_Transactions.textColor = UIColor.gray
                
                cell?.l_Amount.textColor = UIColor.black
                if self.responseData[indexPath.row-1]["createdDate"].stringValue != ""{
                    cell?.l_Date.text = DateInString.stringDate(date:  self.responseData[indexPath.row-1]["createdDate"].stringValue)
                }else{
                    cell?.l_Date.text = DateInString.stringDate(date:  self.responseData[indexPath.row-1]["entryDate"].stringValue)
                }
                cell?.l_BankName.text = self.responseData[indexPath.row-1]["bankName"].stringValue
                cell?.l_BankAccount.text = self.responseData[indexPath.row-1]["bankAccNo"].stringValue
                cell?.l_ConductedBy.text = self.responseData[indexPath.row-1]["txnDoneBy"].stringValue
                cell?.l_Transactions.text = self.responseData[indexPath.row-1]["txnNature"].stringValue
                if self.responseData[indexPath.row-1]["txnNature"].stringValue ==  "Withdraw"{
                    cell?.l_Amount.text = self.responseData[indexPath.row-1]["paidAmount"].stringValue
                }else{
                    cell?.l_Amount.text = self.responseData[indexPath.row-1]["receivedAmount"].stringValue
                }
                
            }
            return cell!
        }
        
    }
    
    
    
    
    
    
    
}
