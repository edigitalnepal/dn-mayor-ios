//
//  CaseOrComplainFullVC.swift
//  DNFounder
//
//  Created by Er. Aditya Raj on 7/5/18.
//  Copyright © 2018 Digital Nepal. All rights reserved.
//

import UIKit
import SwiftyJSON

class CaseOrComplainFullVC: UIViewController {
    var data = JSON()

    @IBOutlet weak var label_Subject: UILabel!
    @IBOutlet weak var label_CreatedDate: UILabel!
    @IBOutlet weak var label_By: UILabel!
    @IBOutlet weak var label_Status: UILabel!
    @IBOutlet weak var textview_Details: UITextView!
    let sId = userDefaults.integer(forKey: "Id")
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Complain Details"
        label_CreatedDate.text = DateInString.stringDate(date:  data["created_date"] .stringValue)
        if data["complained_by"] .stringValue == String(sId) {
            label_By.text = "Complained By You"
        }else{
            label_By.text  = "Complained Againts You"
        }
        label_Subject.text = data["subject"] .stringValue
        if data["status"] .boolValue{
            label_Status.textColor = Color.total_present
            label_Status.text = "SOLVED"
        }else{
            label_Status.textColor = Color.total_absent_student
            label_Status.text = "PENDING"
        }
        textview_Details.text = data["description"] .stringValue

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
