//
//  LeaveFilterVC.swift
//  DNFounder
//
//  Created by Er. Aditya Raj on 7/6/18.
//  Copyright © 2018 Digital Nepal. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import DropDown

class LeaveFilterVC: UIViewController {
    
    
let notification = "gotoleave"
    @IBOutlet weak var month_Btn: UIButton!
    @IBOutlet weak var year_Btn: UIButton!
    @IBOutlet var select_btn: UIButton!
    let dropDownYear = DropDown()
    let dropDownMonth = DropDown()
    let year = ["2076","2075", "2074", "2073"]
   let month=["Baishak", "Jestha", "Ashad","Shrawan", "Bhadra", "Ashoj","Kartik", "Mangsir", "Poush","Magh", "Falgun", "Chaitra"]
    override func viewDidLoad() {
        super.viewDidLoad()
        dropDownYear.anchorView = year_Btn
        dropDownMonth.anchorView = month_Btn
        makeUpButton()
        
     
        dropDownYear.dataSource = year
        dropDownMonth.dataSource = month
         
        dropDownYear.selectionAction = { [unowned self] (index: Int, item: String) in
            self.year_Btn.setTitle(item, for: .normal)
            userDefaults.set(item, forKey: "yearLeave")
            
            
        }
        dropDownMonth.selectionAction = { [unowned self] (index: Int, item: String) in
            self.month_Btn.setTitle(item, for: .normal)
           
               userDefaults.set(String(index+1), forKey: "monthLeave")
            userDefaults.set(item, forKey: "monthLeavetitle")
        }

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if userDefaults.string(forKey: "yearLeave") != "2076"{
            year_Btn.setTitle(userDefaults.string(forKey: "yearLeave")!, for: .normal)
        }else{
           year_Btn.setTitle("2076", for: .normal)
        }
        if userDefaults.string(forKey: "monthLeave") != "1"{
            month_Btn.setTitle(userDefaults.string(forKey: "monthLeavetitle")!, for: .normal)
        }else{
            month_Btn.setTitle(month[0], for: .normal)
        }
     
    }
    func makeUpButton(){
      select_btn.layer.backgroundColor = UIColor.white.cgColor
        select_btn.layer.shadowColor = UIColor.gray.cgColor
        select_btn.layer.borderColor = Color.Blue.cgColor
        select_btn.layer.cornerRadius = 5
        select_btn.layer.borderWidth = 1
        
    }
    
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
 
    @IBAction func click_Month(_ sender: Any) {
        dropDownMonth.show()
    }
    @IBAction func click_Year(_ sender: Any) {
         dropDownYear.show()
    }
    @IBAction func cancel_Btn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
       
    }
    
    @IBAction func Select(_ sender: Any) {
         nc.post(name: Notification.Name("gotoLeave"), object: nil)
        self.dismiss(animated: true, completion: nil)
    }
 
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
