//
//  NewAccountReportVC.swift
//  DNMayor
//
//  Created by mac on 8/19/19.
//  Copyright © 2019 E-Digital Nepal. All rights reserved.
//

import UIKit

import  Charts
import Alamofire
import  SwiftyJSON

class NewAccountReportVC: UIViewController {
    @IBOutlet weak var label_Batch: UILabel!
    
    @IBOutlet weak var label_Fee: UILabel!
    @IBOutlet weak var label_Date: UILabel!
    @IBOutlet weak var mainview: UIView!
    @IBOutlet weak var label_receivedamt: UILabel!
    @IBOutlet weak var label_schoolarship: UILabel!
    @IBOutlet weak var label_penalty: UILabel!
    @IBOutlet weak var label_discount: UILabel!
    
    var fromDate = "2076-1-1"
    var toDate = DateInString.getCurrentDateNep()
    var batchId = "0"
    var feeId = "0"
    var batchName = "All"
    var feeName = "All"
    var condition = 1
    
    
    
    var penalty = PieChartDataEntry()
    var scholarship = PieChartDataEntry()
    var receivedamt = PieChartDataEntry()
    var dueamount = PieChartDataEntry()
    var disccount = PieChartDataEntry()
    var receiveableamt = PieChartDataEntry()
    var barentries:[BarChartDataEntry] = []
    var value:[Double] = []
    var baritem:[String] = []
    var year = DateInString.getCurrentYearNepali()
    var m = DateInString.getCurrentMonthNepali()
    var pieentires = [PieChartDataEntry]()
    var uiColorArray = [UIColor]()
    
    
    @IBOutlet weak var label_Filter: UILabel!
    
    @IBOutlet weak var barview: BarChartView!
    
    //    @IBOutlet weak var pieview: PieChartView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavigationBarItem()
        self.title = "Account Report"
        mainview.isHidden = true
        
        let button1 = UIBarButtonItem(image: UIImage(named: "fliter"), style: .plain, target: self, action: #selector(self.openFilter)) // action:#selector(Class.MethodName) for swift 3
        self.navigationItem.rightBarButtonItem  = button1
        
        
        
        //        pieview.usePercentValuesEnabled = true
        
        barview.layer.cornerRadius = 12
        barview.layer.borderColor = Color.lightGreay.cgColor
        barview.layer.borderWidth = 1
        
        
        penalty.label = "Penalty(%)"
        scholarship.label = "Scholarship(%)"
        receivedamt.label = "Received(%)"
        disccount.label = "Discount(%)"
        
        //        pieview.chartDescription!.text = ""
        baritem = ["penalty","scholarship","receivedamt","disccount"]
        barview.chartDescription?.text = ""
        label_schoolarship.textColor = Color.schoolarship
        label_receivedamt.textColor = Color.receivedamt
        label_discount.textColor = Color.disscount
        label_penalty.textColor = Color.penalty
        
        self.label_Fee.text = self.feeName
        self.label_Date.text = "\(self.fromDate) to \(self.toDate)"
        self.label_Batch.text = self.batchName
        loadFromAPI()
        
        
        // Do any additional setup after loading the view.
    }
    //    override func viewWillAppear(_ animated: Bool) {
    //        super.viewWillAppear(animated)
    //        label_Fee.center.x  -= view.bounds.width
    //        label_Date.center.x -= view.bounds.width
    //        label_Batch.center.x -= view.bounds.width
    //    }
    
    override func viewWillAppear(_ animated: Bool) {
        //        UIView.animate(withDuration: 0.5) {
        //
        //            self.label_Fee.center.x += self.view.bounds.width
        //            self.label_Date.center.x += self.view.bounds.width
        //            self.label_Batch.center.x += self.view.bounds.width
        //
        //        }
        
        
    }
    
    //    override func viewDidAppear(_ animated: Bool) {
    //
    //        label_Fee.center.x  -= view.bounds.width
    //        label_Date.center.x -= view.bounds.width
    //        label_Batch.center.x -= view.bounds.width
    //
    //    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @objc func openFilter(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let newAccount = storyboard.instantiateViewController(withIdentifier: "NewAccountingFilterVC") as! NewAccountingFilterVC
        self.navigationController?.pushViewController(newAccount, animated: true)
    }
    
    
    func loadFromAPI(){
        
        showHud("Loading...")
        
        var paramater = [String:AnyObject]()
        let id=userDefaults.integer(forKey:"teamId")
        
        
        paramater = ["teamId":id as AnyObject,"from":fromDate as AnyObject,"to":toDate as AnyObject,"feeParticularId":feeId as AnyObject,"batchId":batchId as AnyObject]
        
        print(LibraryAPI.newAccountReport)
        print(paramater)
        Alamofire.request(LibraryAPI.newAccountReport, method: .post, parameters: paramater)
            
            .responseJSON(completionHandler: { (response) -> Void in
                print(response)
                if(response.result.isSuccess) {
                    let j = JSON(response.data)
                    self.value.removeAll()
                    self.mainview.isHidden = false
                    if j["success"].boolValue{
                        let json = j["body"]
                        if json["penalty"].intValue == 0 && json["scholarship"].intValue == 0 && json["receivedAmount"].intValue == 0 && json["discount"].intValue == 0 {
                            if self.condition == 1{
                              
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    let newAccount = storyboard.instantiateViewController(withIdentifier: "AccountReportVC") as! AccountReportVC
                                    self.navigationController?.pushViewController(newAccount, animated: true)
                     
                            }
                            
                            
                        }else{
                            self.value.append(json["penalty"].doubleValue)
                            self.value.append(json["scholarship"].doubleValue)
                            self.value.append(json["receivedAmount"].doubleValue)
                            self.value.append(json["discount"].doubleValue)
                            
                            
                            self.disccount.value = json["discount"].doubleValue
                            self.penalty.value = json["penalty"].doubleValue
                            self.scholarship.value = json["scholarship"].doubleValue
                            self.receivedamt.value = json["receivedAmount"].doubleValue
                            
                            //adding value to label
                            
                            self.label_penalty.text = "Rs. " + String(json["penalty"].doubleValue)
                            
                            self.label_discount.text = "Rs. " + String(json["discount"].doubleValue)
                            self.label_receivedamt.text = "Rs. " + String(json["receivedAmount"].doubleValue)
                            self.label_schoolarship.text = "Rs. " + String(json["scholarship"].doubleValue)
                            
                            self.setBarChart(point: self.value, string: self.baritem)
                            //                        self.setPieChart()
                        }
                    }
                    
                    
                    self.hideHUD()
                } else{
                    self.hideHUD()
                    LibraryToast.notifyUser("ALERT", message:"Something Went Wrong Please Try Again" , vc: self)
                    
                    
                }
                
                
                
            })
        
        
        
    }
    
    
    
    func setBarChart(point:[Double],string:[String]){
        barview.noDataText = "No Data Available"
        //        barview.rightAxis.enabled = false
        barview.legend.enabled = false
        barview.leftAxis.enabled = false
        barview.xAxis.enabled = false
        
        var dataentryset:[BarChartDataEntry] = []
        var counter = 0.0
        for i in 0..<point.count{
            counter += 1.0
            let dataEntry = BarChartDataEntry(x: counter, y: point[i])
            dataentryset.append(dataEntry)
        }
        let chardDataset = BarChartDataSet(values: dataentryset, label: nil)
        let chartData = BarChartData()
        chartData.addDataSet(chardDataset)
        let legend = barview.legend
        legend.font = UIFont(name: "Verdana", size: 25.0)!
        barview.data = chartData
        //        chardDataset.drawValuesEnabled = false
        chardDataset.colors = [Color.penalty,Color.schoolarship,Color.receivedamt,Color.disscount]
        barview.animate(xAxisDuration: 2.0, yAxisDuration: 2.0)
        barview.sizeToFit()
        
        
    }
    
    
    func setPieChart(){
        pieentires.removeAll()
        uiColorArray.removeAll()
        if penalty.value != 0{
            pieentires.append(penalty)
            uiColorArray.append(Color.penalty)
        }
        if scholarship.value != 0{
            pieentires.append(scholarship)
            uiColorArray.append(Color.schoolarship)
        }
        if receivedamt.value != 0{
            pieentires.append(receivedamt)
            uiColorArray.append(Color.receivedamt)
        }
        
        if disccount.value != 0{
            pieentires.append(disccount)
            uiColorArray.append(Color.disscount)
        }
        
        
        
        let dataset = PieChartDataSet(values: pieentires, label: nil)
        let pieobject = PieChartData(dataSet: dataset)
        dataset.colors = uiColorArray
        dataset.xValuePosition = .outsideSlice
        pieobject.setValueTextColor(UIColor.black)
        //        pieview.data = pieobject
        //
        //        pieview.sizeToFit()
        
    }
    
    
    
    
}

