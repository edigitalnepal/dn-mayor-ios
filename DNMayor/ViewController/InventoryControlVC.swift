//
//  InventoryControlVC.swift
//  DNFounder
//
//  Created by Er. Aditya Raj on 5/15/18.
//  Copyright © 2018 Digital Nepal. All rights reserved.
//

import UIKit
import  Charts
import Alamofire
import  SwiftyJSON
import Spring

class InventoryControlVC: UIViewController,UIPickerViewDelegate,UIPickerViewDataSource,UIGestureRecognizerDelegate {

    @IBOutlet weak var view_main: UIView!
    @IBOutlet weak var designable_view: DesignableView!

    @IBOutlet var student_collection: UILabel!
    @IBOutlet var total_earning: UILabel!
    @IBOutlet weak var label_purchase: UILabel!
    @IBOutlet weak var barview: BarChartView!
   
    @IBOutlet weak var pickerview: UIPickerView!
    @IBOutlet weak var label_expendeture: UILabel!
    @IBOutlet weak var label_salary: UILabel!

    var purchaseP = PieChartDataEntry()
    var dueamntP = PieChartDataEntry()
    var expenditureP = PieChartDataEntry()
    var totalstudentP = PieChartDataEntry()
    var totalearningP = PieChartDataEntry()
    
     let filter = ["yearly", "monthly", "weekly", "todays"]

    var uiColorArray = [UIColor]()

    
    @IBOutlet weak var pieview: PieChartView!
    var pieentires = [PieChartDataEntry]()
    var barentries:[BarChartDataEntry] = []
    var value:[Double] = []
    var baritem:[String] = []
    @IBOutlet weak var imgae_filter: UIImageView!
    
    override func viewDidLoad() {
        pickerview.isHidden = true
        super.viewDidLoad()
        view_main.isHidden = true
        pickerview.delegate = self
        pickerview.dataSource = self
        pickerview.backgroundColor = Color.lightGreay
        pickerview.tintColor = Color.Blue
        
        self.setNavigationBarItem()
        
        let button1 = UIBarButtonItem(image: UIImage(named: "fliter"), style: .plain, target: self, action: #selector(self.openFilter)) // action:#selector(Class.MethodName) for swift 3
        self.navigationItem.rightBarButtonItem  = button1
    
        
        
        self.title = "Inventory"
        //setting pie chart name
        purchaseP.label = "purchase"
        dueamntP.label = "DueAmt"
        expenditureP.label = "Expenditure"
        totalstudentP.label = "StdCollection"
        pieview.chartDescription!.text = ""
        pieview.usePercentValuesEnabled = true
        baritem = ["purchase","DueAmt","Expenditure","StdCollection"]
        barview.chartDescription?.text = ""
        
        label_purchase.textColor = Color.disscount
        label_salary.textColor = Color.total_absent_student
        label_expendeture.textColor = Color.receivedamt
        total_earning.textColor = Color.Blue
        student_collection.textColor = Color.total_present
     
    // Do any additional setup after loading the view.
    }
    @objc func openFilter(){
        designable_view.isHidden = true
        pickerview.isHidden = false
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func reloadView(filter:String){
        self.value.removeAll()
        loadFromAPI(filter: filter)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        loadFromAPI(filter: "todays")
    }
    func loadFromAPI(filter:String){
        
        showHud("Loading...")
        
        var paramater = [String:AnyObject]()
        let id=userDefaults.integer(forKey:"teamId")
        
        paramater = ["team_id":id as AnyObject,"filter_type":filter as AnyObject]
        
        print(paramater)
        Alamofire.request(LibraryAPI.inventoryURl, method: .post, parameters: paramater)
            
            .responseJSON(completionHandler: { (response) -> Void in
                print(response)
                if(response.result.isSuccess) {
                    let json = JSON(response.data)
                    self.view_main.isHidden = false
                   self.label_salary.text = "Rs. " + String(json["due_amount"].doubleValue)
                    self.label_purchase.text = "Rs. " + String(json["total_purchase"].doubleValue)
                     self.label_expendeture.text = "Rs. " + String(json["total_expenditure"].doubleValue)
                    self.total_earning.text = "Rs. " + String(json["total_earnings"].doubleValue)
                    self.student_collection.text = "Rs. " + String(json["total_collection_from_student"].doubleValue)
                    
                        self.value.removeAll()
                        self.value.append(json["total_purchase"].doubleValue)
                       self.value.append(json["due_amount"].doubleValue)
                       self.value.append(json["total_expenditure"].doubleValue)
                    self.value.append(json["total_collection_from_student"].doubleValue)
                    self.value.append(json["total_earnings"].doubleValue)
                    
                    self.purchaseP.value = json["total_purchase"].doubleValue
                   self.dueamntP.value = json["due_amount"].doubleValue
                    self.expenditureP.value = json["total_expenditure"].doubleValue
                    self.totalstudentP.value = json["total_collection_from_student"].doubleValue
                      self.setPieChart()
                    self.setBarChart(point: self.value, string: self.baritem)
                  
                    
                    self.hideHUD()
                } else{
                    self.hideHUD()
                    LibraryToast.notifyUser("ALERT", message:"Something Went Wrong Please Try Again" , vc: self)
                    
                    
                }
                
                
                
            })
        
        
        
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return filter.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return filter[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        pickerView.isHidden = true
        designable_view.isHidden = false
        self.reloadView(filter: filter[row])
        
        
    }
    func setBarChart(point:[Double],string:[String]){
        barview.noDataText = "No Data Available"
        var dataentryset:[BarChartDataEntry] = []
        var counter = 0.0
        for i in 0..<point.count{
            counter += 1.0
            let dataEntry = BarChartDataEntry(x: counter, y: point[i])
            dataentryset.append(dataEntry)
            
            
        }
        let chardDataset = BarChartDataSet(values: dataentryset, label: nil)
        let chartData = BarChartData()
        chartData.addDataSet(chardDataset)
        barview.data = chartData
        chardDataset.colors = [Color.disscount,Color.total_absent_student,Color.receivedamt,Color.total_present,Color.Blue]
        barview.animate(xAxisDuration: 2.0, yAxisDuration: 2.0)
        
    
    }
    
  
    func setPieChart(){
        pieentires.removeAll()
        uiColorArray.removeAll()
        if purchaseP.value != 0{
            pieentires.append(purchaseP)
            uiColorArray.append(Color.disscount)
        }
        if dueamntP.value != 0{
            pieentires.append(dueamntP)
            uiColorArray.append(Color.total_absent_student)
        }
        if expenditureP.value != 0{
            pieentires.append(expenditureP)
            uiColorArray.append(Color.receivedamt)
        }
        if totalstudentP.value != 0{
            pieentires.append(totalstudentP)
            uiColorArray.append(Color.total_present)
        }
       
      
        let dataset = PieChartDataSet(values: pieentires, label: nil)
        let pieobject = PieChartData(dataSet: dataset)
        dataset.colors =  uiColorArray
        pieview.data = pieobject
        
    }

    

  

}
