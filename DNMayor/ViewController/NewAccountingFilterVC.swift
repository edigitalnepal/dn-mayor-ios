//
//  NewAccountingFilterVC.swift
//  DNMayor
//
//  Created by mac on 8/19/19.
//  Copyright © 2019 E-Digital Nepal. All rights reserved.
//



import UIKit
import Alamofire
import SwiftyJSON
import DropDown

class NewAccountingFilterVC: UIViewController {
    @IBOutlet weak var two_date: UITextField!
    
    @IBOutlet weak var tf_fromdate: UITextField!
    @IBOutlet weak var btn_feeparticular: UIButton!
    @IBOutlet weak var btn_batch: UIButton!
    
    let datePicker = UIDatePicker()
    let datePickertwo = UIDatePicker()
    var dateType = ""
    
    var chooseDateStatus = 1
    
    var batchType = [String]()
    var batchname = ""
    var batch = ""
    let batchdropdown = DropDown()
    var responseDataBatch :[JSON] = [JSON]()
    
    var feeType = [String]()
    var feename = ""
    var fee = ""
    let feedropdown = DropDown()
    var responseFeeType :[JSON] = [JSON]()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Account Report Filter"
        two_date.text = DateInString.getCurrentDateNep()
        tf_fromdate.text = DateInString.oneMonthLate()
        setNavigationBarItem()
        makeupButton()
        loadBatch()
        
        NotificationCenter.default.addObserver(self, selector: (#selector(self.FromDate(_:))), name: NSNotification.Name(rawValue: "adate"), object: nil)
        
        
        
        //click on textfield
        tf_fromdate.addTarget(self, action: #selector(myTargetFunction), for: .touchDown)
        two_date.addTarget(self, action: #selector(twodate), for: .touchDown)
        
        //dropdown setup batch
        batchdropdown.anchorView = btn_batch
        batchdropdown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.btn_batch.setTitle(item, for: .normal)
            self.batchname = item
            if item != "All"{
                self.batch = self.responseDataBatch[index-1]["id"].stringValue
            }else{
                self.batch = "0"
            }
            self.loadFeeParticular()
            
            print(self.batch)
        }
        
        //dropdown setup feetype
        feedropdown.anchorView = btn_feeparticular
        feedropdown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.btn_feeparticular.setTitle(item, for: .normal)
            self.feename = item
            if item != "All"{
                self.fee = self.responseFeeType[index-1]["id"].stringValue
            }else{
                self.fee = "0"
            }
            
        }
        
    }
    
    
    @IBAction func clickVIewReport(_ sender: Any) {
        if tf_fromdate.text! != "" && two_date.text! != "" && batch != "" && fee != ""{
            if tf_fromdate.text!.length > 10 || two_date.text!.length > 10 {
                LibraryToast.notifyUser("ALERT", message:"Select all above properly" , vc: self)
            }else{
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let newAccount = storyboard.instantiateViewController(withIdentifier: "NewAccountReportVC") as! NewAccountReportVC
                newAccount.batchId = batch
                newAccount.feeId = fee
                newAccount.batchName = batchname
                newAccount.feeName = feename
                newAccount.fromDate = tf_fromdate.text!
                newAccount.toDate = two_date.text!
                newAccount.condition = 2
                
                self.navigationController?.pushViewController(newAccount, animated: true)
            }
        }else{
            LibraryToast.notifyUser("ALERT", message:"Select all above properly" , vc: self)
        }
        
    }
    func compareDate(fromDate:String!,toDate:String!) ->Bool{
        if !(fromDate!.length > 10) && !(toDate!.length > 10){
            let fdate = fromDate.split(separator: "-")
            let tdate = toDate.split(separator: "-")
            let fyear:Int = Int(fdate[0])!
            let tyear:Int = Int(tdate[0])!
            let fmonth:Int = Int(fdate[1])!
            let tmonth:Int = Int(tdate[1])!
            let fday:Int = Int(fdate[2])!
            let tday:Int = Int(tdate[2])!
            print(fyear)
            print(tyear)
            if fyear > tyear {
                return true
            }else{
                if fyear == tyear && fmonth > tmonth{
                    return true
                }else{
                    if fmonth == tmonth && fday > tday{
                        return true
                    }else{
                        return false
                    }
                }
            }
            
        }else{
            return false
        }
        
        
        
        
    }
    
    @objc func FromDate(_ notification: NSNotification) {
        print(notification.userInfo!["date"] as! String)
        guard let name = notification.userInfo!["date"] else {
            
            return
        }
        if chooseDateStatus == 1{
            let date = name as! String
            if self.tf_fromdate.text! != ""{
                if compareDate(fromDate: tf_fromdate.text!, toDate: date){
                    self.two_date.text! = "To date must be greate then From date"
                    
                }else{
                    self.two_date.text = date
                    loadBatch()
                }
            }else{
                self.two_date.text = date
                
            }
            
        }else if chooseDateStatus == 2{
            let date = name as! String
            if self.two_date.text! != ""{
                if compareDate(fromDate:date, toDate: two_date.text!){
                    tf_fromdate.text = "From date must be less then To date"
                    //                    LibraryToast.notifyUser("ALERT", message:"From date must be less then To date", vc: self)
                }else{
                    self.tf_fromdate.text = date
                    loadBatch()
                }
            }else{
                self.tf_fromdate.text = date
                
            }
            
        }
        
        
    }
    
    
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        checkEnglishNeplai()
    }
    
    // MARK: - Check to Show english or nepali date picker
    func checkEnglishNeplai(){
        showHud("Loading...")
        var paramater = [String:AnyObject]()
        let id = userDefaults.integer(forKey:  "teamId")
        paramater = ["teamId":id as AnyObject]
        Alamofire.request(LibraryAPI.datetype, method: .post, parameters: paramater)
            .responseJSON(completionHandler: { (response) -> Void in
                print(response)
                if(response.result.isSuccess) {
                    let json = JSON(response.data)
                    let datas = json["body"]
                    if json["success"].boolValue{
                        self.dateType = datas["operationDateSetting"].stringValue
                    }else{
                        LibraryToast.notifyUser("ALERT", message:json["message"].stringValue, vc: self)
                        
                    }
                    
                    self.hideHUD()
                } else{
                    self.hideHUD()
                    let alert = UIAlertController(title: "ALERT!  ", message: "Check your internet connections and Try Again", preferredStyle: .alert)
                    
                    
                    alert.addAction(UIAlertAction(title: "Retry", style: .destructive, handler:{ action in
                        
                        self.checkEnglishNeplai()
                    }))
                    
                    self.present(alert, animated: true)
                    
                    
                }
                
                
            })
        
    }
    
    
    // MARK: - Fetch batch from api
    func loadBatch(){
        if tf_fromdate.text != "" && two_date.text != ""{
            showHud("Loading...")
            var paramater = [String:AnyObject]()
            let id = userDefaults.integer(forKey:  "teamId")
            paramater = ["startDate":tf_fromdate.text! as AnyObject,"endDate":two_date.text! as AnyObject,"teamId":id as AnyObject]
            
            print(paramater)
            Alamofire.request(LibraryAPI.fetchBatch, method: .post, parameters: paramater)
                .responseJSON(completionHandler: { (response) -> Void in
                    print(response)
                    if(response.result.isSuccess) {
                        let json = JSON(response.data)
                        self.responseDataBatch.removeAll()
                        self.batchType.removeAll()
                        if json["success"].boolValue{
                            self.responseDataBatch = json["body"].arrayValue
                            self.batchType.append("All")
                            self.batch = "0"
                            self.batchname = "All"
                            self.feeType.removeAll()
                            self.feename = "All"
                            self.fee = "0"
                            self.feedropdown.dataSource = self.feeType
                            self.btn_feeparticular.setTitle(self.feename, for: .normal)
                            self.btn_batch.setTitle(self.batchname, for: .normal)
                            
                            for i in self.responseDataBatch{
                                self.batchType.append(i["name"].stringValue)
                            }
                            self.batchdropdown.dataSource = self.batchType
                            self.loadFeeParticular()
                            
                        }else{
                            LibraryToast.notifyUser("ALERT", message:json["message"].stringValue, vc: self)
                            
                        }
                        
                        self.hideHUD()
                    } else{
                        self.hideHUD()
                        LibraryToast.notifyUser("ALERT", message:"Something Went Wrong Please Try Again" , vc: self)
                        
                        
                    }
                    
                    
                })
        }
        
    }
    
    // MARK: - Fetch feeparticular from api.
    func loadFeeParticular(){
        showHud("Loading...")
        var paramater = [String:AnyObject]()
        let id = userDefaults.integer(forKey:  "teamId")
        
        paramater = ["batchId":batch as AnyObject,"teamId":id as AnyObject]
        
        print(paramater)
        Alamofire.request(LibraryAPI.feeParticular, method: .post, parameters: paramater)
            .responseJSON(completionHandler: { (response) -> Void in
                print(response)
                if(response.result.isSuccess) {
                    let json = JSON(response.data)
                    self.responseFeeType.removeAll()
                    self.feeType.removeAll()
                    print(self.responseFeeType)
                    if json["success"].boolValue{
                        self.responseFeeType = json["body"].arrayValue
                        self.feeType.append("All")
                        self.btn_feeparticular.setTitle("All", for: .normal)
                        for i in self.responseFeeType{
                            self.feeType.append(i["name"].stringValue)
                        }
                        self.feedropdown.dataSource = self.feeType
                        
                    }else{
                        LibraryToast.notifyUser("ALERT", message:json["message"].stringValue, vc: self)
                        
                    }
                    
                    self.hideHUD()
                } else{
                    self.hideHUD()
                    LibraryToast.notifyUser("ALERT", message:"Something Went Wrong Please Try Again" , vc: self)
                    
                    
                }
                
                
            })
        
        
    }
    
    
    @IBAction func clickFeeParticular(_ sender: Any) {
        feedropdown.show()
        
    }
    
    @IBAction func clickBatch(_ sender: Any) {
        batchdropdown.show()
        
    }
    
    @objc func myTargetFunction(textField: UITextField) {
        chooseDateStatus = 2
        if dateType == "en"{
            showDatePicker()
            
        }else if dateType == "np"{
            let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ChooseDateVC") as! ChooseDateVC
            
            popOverVC.modalPresentationStyle = .overFullScreen
            popOverVC.modalTransitionStyle = .crossDissolve
            popOverVC.status = 2
            popOverVC.callfrom = 1
            self.present(popOverVC, animated: true, completion: nil)
            
        }
        
    }
    @objc func twodate(textField: UITextField) {
        chooseDateStatus = 1
        if dateType == "en"{
            showDatePickertwo()
        }else if dateType == "np"{
            let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ChooseDateVC") as! ChooseDateVC
            
            popOverVC.modalPresentationStyle = .overFullScreen
            popOverVC.modalTransitionStyle = .crossDissolve
            popOverVC.status = 1
            popOverVC.callfrom = 1
            self.present(popOverVC, animated: true, completion: nil)
            
        }
        
        
    }
    func showDatePicker(){
        //Formate Date
        datePicker.datePickerMode = .date
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        tf_fromdate.inputAccessoryView = toolbar
        tf_fromdate.inputView = datePicker
        
        
    }
    func showDatePickertwo(){
        //Formate Date
        datePickertwo.datePickerMode = .date
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePickerTwo));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        two_date.inputAccessoryView = toolbar
        two_date.inputView = datePickertwo
        
        
    }
    
    @objc func donedatePicker(){
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let date = formatter.string(from: datePicker.date)
        self.view.endEditing(true)
        if self.two_date.text! != ""{
            if compareDate(fromDate:date, toDate: two_date.text!){
                tf_fromdate.text = "From date must be less then To date"
                //                    LibraryToast.notifyUser("ALERT", message:"From date must be less then To date", vc: self)
            }else{
                self.tf_fromdate.text = date
                loadBatch()
            }
        }else{
            self.tf_fromdate.text = date
            
        }
        
        
        
        
    }
    @objc func donedatePickerTwo(){
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let date = formatter.string(from: datePickertwo.date)
        
        if self.tf_fromdate.text! != ""{
            if compareDate(fromDate: tf_fromdate.text!, toDate: date){
                self.two_date.text! = "To date must be greate then From date"
                //       LibraryToast.notifyUser("ALERT", message:"To date must be greate then From date", vc: self)
            }else{
                self.two_date.text = date
                loadBatch()
            }
        }else{
            self.two_date.text = date
            
        }
        self.view.endEditing(true)
        
        
        
        
    }
    
    func makeupButton(){
        btn_batch.layer.backgroundColor = UIColor.white.cgColor
        btn_batch.layer.cornerRadius = 8
        btn_batch.layer.borderWidth = 1
        btn_batch.layer.borderColor = UIColor.darkGray.cgColor
        btn_feeparticular.layer.backgroundColor = UIColor.white.cgColor
        btn_feeparticular.layer.cornerRadius = 8
        btn_feeparticular.layer.borderWidth = 1
        btn_feeparticular.layer.borderColor = UIColor.darkGray.cgColor
    }
    
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    
    
}
