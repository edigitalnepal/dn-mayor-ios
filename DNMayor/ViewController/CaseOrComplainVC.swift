//
//  CaseOrComplainVC.swift
//  DNFounder
//
//  Created by Er. Aditya Raj on 7/5/18.
//  Copyright © 2018 Digital Nepal. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import DZNEmptyDataSet

class CaseOrComplainVC: UIViewController {
    let cellIdentifier = "ComplainCell"
    var responseData :[JSON] = [JSON]()
    var totolcount = 0
    var pageNumber = 0
    var type = Int()
    var refresh:Bool = false
    var refreshControl = UIRefreshControl()
    let sId = userDefaults.integer(forKey: "Id")

 
    @IBOutlet weak var tableview_Case: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavigationBarItem()
        title="Case or Complain"
        print("_________________" + String(self.type))
        self.refreshControl.attributedTitle = NSAttributedString(string: "Refreshing")
        refreshControl.addTarget(self, action: #selector(refresh(sender:)), for: .valueChanged)
        self.tableview_Case?.addSubview(refreshControl)
        
        //tabel view setup
        tableview_Case.delegate = self
        tableview_Case.dataSource = self
        tableview_Case.register(UINib(nibName: "ComplainCell", bundle: Bundle.main), forCellReuseIdentifier: cellIdentifier)
        tableview_Case.estimatedRowHeight = 110.0
        tableview_Case.rowHeight = UITableViewAutomaticDimension
        loadFromAPI(offSetNormal:0)
        
        
        // Do any additional setup after loading the view.
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @objc func refresh(sender:AnyObject) {
        
        loadFromAPI(offSetNormal:0)
        refresh=true
        responseData.removeAll()
        
        tableview_Case.reloadData()
        
    }
    func loadFromAPI(offSetNormal:Int){
        if refresh {
            
        }else{
            showHud("Loading...")
        }
        var paramater = [String:AnyObject]()
        let id = userDefaults.integer(forKey:  "teamId")
        
        paramater = ["team_id":id as AnyObject]
        
        print(paramater)
        Alamofire.request(LibraryAPI.complainapi, method: .post, parameters: paramater)
            
            
            .responseJSON(completionHandler: { (response) -> Void in
                print(response)
                if(response.result.isSuccess) {
                    
                    let json = JSON(response.data)
                    let data = json["case_complains"].arrayValue
                    print(data)
                    
                    
                    self.responseData = json["case_complains"].arrayValue
                    self.tableview_Case.reloadData()
                    
                    
                    
                    self.hideHUD()
                } else{
                    self.hideHUD()
                    LibraryToast.notifyUser("ALERT", message:"Something Went Wrong Please Try Again" , vc: self)
                    
                    
                }
                self.refreshControl.endRefreshing()
                
                
            })
        self.tableview_Case.emptyDataSetSource = self
        self.tableview_Case.emptyDataSetDelegate = self
        refresh = false
        
        
    }
    
  
    
    
    
    
}
extension CaseOrComplainVC :UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return responseData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? ComplainCell
        cell?.selectionStyle = UITableViewCellSelectionStyle.none
        
        cell?.label_Date.text = DateInString.stringDate(date: responseData[indexPath.row]["created_date"] .stringValue)
        
        if responseData[indexPath.row]["complained_by"] .stringValue == String(sId) {
            cell?.label_By.text = "Complained By You"
        }else{
            cell?.label_By.text  = "Complained Againts You"
        }
        cell?.label_Title.text = responseData[indexPath.row]["subject"] .stringValue
        if responseData[indexPath.row]["status"] .boolValue{
            cell?.label_Status.textColor = Color.total_present
            cell?.label_Status.text = "SOLVED"
        }else{
            cell?.label_Status.textColor = Color.total_absent_student
            cell?.label_Status.text = "PENDING"
        }
        
        
        return cell!
    }
    
    
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let gunasoDetailVC = storyboard.instantiateViewController(withIdentifier: "CaseOrComplainFullVC") as! CaseOrComplainFullVC
        gunasoDetailVC.data = responseData[indexPath.row]
        
        self.navigationController?.pushViewController(gunasoDetailVC, animated: true)
        
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
    }
    
}
extension CaseOrComplainVC : DZNEmptyDataSetDelegate, DZNEmptyDataSetSource {
    
    
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let str = ""
        let attrs = [NSAttributedStringKey.font: UIFont.preferredFont(forTextStyle: UIFontTextStyle.headline)]
        return NSAttributedString(string: str, attributes: attrs)
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let str = "No Data Available"
        let attrs = [NSAttributedStringKey.font: UIFont.preferredFont(forTextStyle: UIFontTextStyle.body)]
        return NSAttributedString(string: str, attributes: attrs)
    }
    
    
    func buttonTitle(forEmptyDataSet scrollView: UIScrollView!, for state: UIControlState) -> NSAttributedString! {
        let str = "RELOAD"
        let attrs = [NSAttributedStringKey.font: UIFont.preferredFont(forTextStyle: UIFontTextStyle.body)]
        return NSAttributedString(string: str, attributes: attrs)
    }
    
    func emptyDataSetDidTapButton(_ scrollView: UIScrollView!) {
        refresh = false
        responseData.removeAll()
        loadFromAPI(offSetNormal:0)
        
    }
}
