//
//  StudentListVC.swift
//  DNFounder
//
//  Created by Er. Aditya Raj on 7/2/18.
//  Copyright © 2018 Digital Nepal. All rights reserved.
//

import UIKit
import SwiftyJSON
import DZNEmptyDataSet
import Alamofire

class StudentListVC: UIViewController {

    @IBOutlet weak var btn_Cancel: UIButton!
    let cellIdentifier = "StudentListCell"
    var responseData :[JSON] = [JSON]()
    var totolcount = 0
    var pageNumber = 0
    var type = Int()
    var refresh:Bool = false
    var refreshControl = UIRefreshControl()
    
    
    @IBOutlet weak var tableview_BeyaTable: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavigationBarItem()
        title="Notices"
        print("_________________" + String(self.type))
        self.refreshControl.attributedTitle = NSAttributedString(string: "Refreshing")
        refreshControl.addTarget(self, action: #selector(refresh(sender:)), for: .valueChanged)
        self.tableview_BeyaTable?.addSubview(refreshControl)
        
        //tabel view setup
        tableview_BeyaTable.delegate = self
        tableview_BeyaTable.dataSource = self
        tableview_BeyaTable.register(UINib(nibName: "StudentListCell", bundle: Bundle.main), forCellReuseIdentifier: cellIdentifier)
        tableview_BeyaTable.estimatedRowHeight = 60.0
        tableview_BeyaTable.rowHeight = UITableViewAutomaticDimension
        tableview_BeyaTable.reloadData()
//        loadFromAPI(offSetNormal:0)
        
        
        // Do any additional setup after loading the view.
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @objc func refresh(sender:AnyObject) {
        
        loadFromAPI(offSetNormal:0)
        refresh=true
        responseData.removeAll()
        
        tableview_BeyaTable.reloadData()
        
    }
    @IBAction func btn_Click(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func loadFromAPI(offSetNormal:Int){
        if refresh {
            
        }else{
            showHud("Loading...")
        }
        var paramater = [String:AnyObject]()
        let id = userDefaults.integer(forKey:  "teamId")
        
        paramater = ["team_id":id as AnyObject,"start":0 as AnyObject,"limit":30 as AnyObject]
        
        print(paramater)
        Alamofire.request(LibraryAPI.NoticUrl, method: .post, parameters: paramater)
            
            
            .responseJSON(completionHandler: { (response) -> Void in
                print(response)
                if(response.result.isSuccess) {
                    
                    let json = JSON(response.data)
                    let data = json["notices"].arrayValue
                    print(data)
                    
                    
                    self.responseData = json["notices"].arrayValue
                    self.tableview_BeyaTable.reloadData()
                    
                    
                    
                    self.hideHUD()
                } else{
                    self.hideHUD()
                    LibraryToast.notifyUser("ALERT", message:"Something Went Wrong Please Try Again" , vc: self)
                    
                    
                }
                self.refreshControl.endRefreshing()
                
                
            })
        self.tableview_BeyaTable.emptyDataSetSource = self
        self.tableview_BeyaTable.emptyDataSetDelegate = self
        refresh = false
        
        
    }
    
   
    
    
    
    
}
extension StudentListVC :UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return responseData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? StudentListCell
        cell?.selectionStyle = UITableViewCellSelectionStyle.none
        
        cell?.student_Id.text = responseData[indexPath.row]["student_id"] .stringValue
        cell?.student_Name.text = responseData[indexPath.row]["student_name"] .stringValue
        cell?.student_Phone.text = responseData[indexPath.row]["student_phone"] .stringValue
        if  responseData[indexPath.row]["status"] .boolValue{
            cell?.setColor(bool:true)
        }else{
           cell?.setColor(bool:false)
        }
        
        return cell!
    }
    
    
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
    }
    
}
extension StudentListVC : DZNEmptyDataSetDelegate, DZNEmptyDataSetSource {
    
    
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let str = ""
        let attrs = [NSAttributedStringKey.font: UIFont.preferredFont(forTextStyle: UIFontTextStyle.headline)]
        return NSAttributedString(string: str, attributes: attrs)
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let str = "No Data Available"
        let attrs = [NSAttributedStringKey.font: UIFont.preferredFont(forTextStyle: UIFontTextStyle.body)]
        return NSAttributedString(string: str, attributes: attrs)
    }
    
    
    func buttonTitle(forEmptyDataSet scrollView: UIScrollView!, for state: UIControlState) -> NSAttributedString! {
        let str = "RELOAD"
        let attrs = [NSAttributedStringKey.font: UIFont.preferredFont(forTextStyle: UIFontTextStyle.body)]
        return NSAttributedString(string: str, attributes: attrs)
    }
    
    func emptyDataSetDidTapButton(_ scrollView: UIScrollView!) {
        refresh = false
        responseData.removeAll()
        loadFromAPI(offSetNormal:0)
        
    }
}
