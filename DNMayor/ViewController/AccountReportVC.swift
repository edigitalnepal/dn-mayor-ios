//
//  AccountReportVC.swift
//  DNFounder
//
//  Created by Er. Aditya Raj on 5/15/18.
//  Copyright © 2018 Digital Nepal. All rights reserved.
//

import UIKit
import  Charts
import Alamofire
import  SwiftyJSON

class AccountReportVC: UIViewController,UIGestureRecognizerDelegate {
    
    @IBOutlet weak var mainview: UIView!
    @IBOutlet weak var label_receiveableamt: UILabel!
    @IBOutlet weak var label_discount: UILabel!
    @IBOutlet weak var label_deuamt: UILabel!
    @IBOutlet weak var label_receivedamt: UILabel!
    @IBOutlet weak var label_schoolarship: UILabel!
    @IBOutlet weak var label_penalty: UILabel!
    
    var penalty = PieChartDataEntry()
    var scholarship = PieChartDataEntry()
    var receivedamt = PieChartDataEntry()
    var dueamount = PieChartDataEntry()
    var disccount = PieChartDataEntry()
    var receiveableamt = PieChartDataEntry()
    var barentries:[BarChartDataEntry] = []
    var value:[Double] = []
    var baritem:[String] = []
    var pieentires = [PieChartDataEntry]()
    var uiColorArray = [UIColor]()
    let notification=Notification.Name("gotoAccount")

    @IBOutlet weak var image_filter: UIImageView!
    @IBOutlet weak var barview: BarChartView!
    
    @IBOutlet weak var pieview: PieChartView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavigationBarItem()
        self.title = "Account Report"
        mainview.isHidden = true
        
        pieview.usePercentValuesEnabled = true
        let button1 = UIBarButtonItem(image: UIImage(named: "fliter"), style: .plain, target: self, action: #selector(self.openFilter)) // action:#selector(Class.MethodName) for swift 3
        self.navigationItem.rightBarButtonItem  = button1
        
        penalty.label = "Penalty(%)"
        scholarship.label = "Scholarship(%)"
        receivedamt.label = "Received(%)"
        dueamount.label = "Due(%)"
        disccount.label = "Discount(%)"
        receiveableamt.label = "Receivable(%)"
        pieview.chartDescription!.text = ""
        baritem = ["penalty","scholarship","receivedamt","dueamount","disccount","receiveableamt"]
        barview.chartDescription?.text = ""
        label_schoolarship.textColor = Color.schoolarship
        label_receivedamt.textColor = Color.receivedamt
        label_discount.textColor = Color.disscount
        label_penalty.textColor = Color.penalty
        label_deuamt.textColor = Color.dueamount
        label_receiveableamt.textColor = Color.receiableamt
          nc.addObserver(self, selector: #selector(openInventory), name: Notification.Name("gotoAccount"), object: nil)
        if userDefaults.string(forKey: "year") != "" {
            userDefaults.set("2076", forKey: "year")
        }
        if userDefaults.string(forKey: "month") != "" {
            userDefaults.set("All", forKey: "month")
        }
        if userDefaults.string(forKey: "type") != "" {
          userDefaults.set("All", forKey: "type")
        }
        if userDefaults.string(forKey: "grade") != "" {
              userDefaults.set("All", forKey: "grade")
        }
       
       
      
        
        
       loadFromAPI(year: "2076", month: "All", type: "All", grade: "All")
        

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  
    
    func loadFromAPI(year:String,month:String,type:String,grade:String){
        
        showHud("Loading...")
        
        var paramater = [String:AnyObject]()
        let id = userDefaults.integer(forKey:"teamId")

        
        
        paramater = ["team_id":id as AnyObject,"year":year as AnyObject,"month":month as AnyObject,"fee_type":type as AnyObject,"grade":grade as AnyObject]
        
        print(LibraryAPI.accountdetails)
        print(paramater)
        Alamofire.request(LibraryAPI.accountdetails, method: .post, parameters: paramater)
            
            .responseJSON(completionHandler: { (response) -> Void in
                print(response)
                if(response.result.isSuccess) {
                    let json = JSON(response.data)
                    self.value.removeAll()
                  self.mainview.isHidden = false
                    self.value.append(json["penalty"].doubleValue)
                    self.value.append(json["scholarship"].doubleValue)
                    self.value.append(json["received_amount"].doubleValue)
                    self.value.append(json["receivable_due_amount"].doubleValue)
                    self.value.append(json["discount"].doubleValue)
                    self.value.append(json["receivable_amount"].doubleValue)
                    
                    self.receiveableamt.value = json["receivable_amount"].doubleValue
                    self.disccount.value = json["discount"].doubleValue
                    self.penalty.value = json["penalty"].doubleValue
                    self.scholarship.value = json["scholarship"].doubleValue
                    self.receivedamt.value = json["received_amount"].doubleValue
                    self.dueamount.value = json["receivable_due_amount"].doubleValue
                    //adding value to label
                    self.label_receiveableamt.text = "Rs. " + String(json["receivable_amount"].doubleValue)
                    self.label_penalty.text = "Rs. " + String(json["penalty"].doubleValue)
                      self.label_deuamt.text = "Rs. " + String(json["receivable_due_amount"].doubleValue)
                      self.label_discount.text = "Rs. " + String(json["discount"].doubleValue)
                      self.label_receivedamt.text = "Rs. " + String(json["received_amount"].doubleValue)
                      self.label_schoolarship.text = "Rs. " + String(json["scholarship"].doubleValue)
                    
                    self.setBarChart(point: self.value, string: self.baritem)
                    self.setPieChart()
                    
                    self.hideHUD()
                } else{
                    self.hideHUD()
                    LibraryToast.notifyUser("ALERT", message:"Something Went Wrong Please Try Again" , vc: self)
                    
                    
                }
                
                
                
            })
        
        
        
    }
    @objc func openFilter(){
        let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "InventoryFilterPopupViewController") as! InventoryFilterPopupViewController
        popOverVC.modalPresentationStyle = .overFullScreen
        popOverVC.modalTransitionStyle = .crossDissolve
        self.present(popOverVC, animated: true, completion: nil)
        
        
    }
    @objc func openInventory(){
    
      
       
        loadFromAPI(year: userDefaults.string(forKey: "year")!, month: userDefaults.string(forKey: "month")!, type: userDefaults.string(forKey: "type")!, grade: userDefaults.string(forKey: "grade")!)
        
        
        
    }
  
    func setBarChart(point:[Double],string:[String]){
        barview.noDataText = "No Data Available"
        var dataentryset:[BarChartDataEntry] = []
        var counter = 0.0
        for i in 0..<point.count{
            counter += 1.0
            let dataEntry = BarChartDataEntry(x: counter, y: point[i])
            dataentryset.append(dataEntry)
        }
        let chardDataset = BarChartDataSet(values: dataentryset, label: nil)
        let chartData = BarChartData()
        chartData.addDataSet(chardDataset)
        barview.data = chartData
        chardDataset.colors = [Color.penalty,Color.schoolarship,Color.receivedamt,Color.dueamount,Color.disscount,Color.receiableamt]
        barview.animate(xAxisDuration: 2.0, yAxisDuration: 2.0)
        
        
    }
    
 
    func setPieChart(){
        pieentires.removeAll()
        uiColorArray.removeAll()
        if penalty.value != 0{
            pieentires.append(penalty)
            uiColorArray.append(Color.penalty)
        }
        if scholarship.value != 0{
            pieentires.append(scholarship)
             uiColorArray.append(Color.schoolarship)
        }
        if receivedamt.value != 0{
            pieentires.append(receivedamt)
             uiColorArray.append(Color.receivedamt)
        }
        if dueamount.value != 0{
            pieentires.append(dueamount)
             uiColorArray.append(Color.dueamount)
        }
        if disccount.value != 0{
            pieentires.append(disccount)
             uiColorArray.append(Color.disscount)
        }
        if receiveableamt.value != 0{
            pieentires.append(receiveableamt)
             uiColorArray.append(Color.receiableamt)
        }
      
       
        let dataset = PieChartDataSet(values: pieentires, label: nil)
        let pieobject = PieChartData(dataSet: dataset)
        dataset.colors = uiColorArray
            pieview.data = pieobject
        pieview.sizeToFit()
        
    }

    
 

}
