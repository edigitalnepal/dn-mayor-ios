//
//  TodaysCollectionVC.swift
//  DNFounder
//
//  Created by Er. Aditya Raj on 7/5/18.
//  Copyright © 2018 Digital Nepal. All rights reserved.
//

import UIKit
import Charts
import Alamofire
import SwiftyJSON

class TodaysCollectionVC: UIViewController {

    @IBOutlet weak var collection_PieChart: PieChartView!
    @IBOutlet weak var label_Transportation: UILabel!
    @IBOutlet weak var label_Hostel: UILabel!
    @IBOutlet weak var label_Library: UILabel!
    @IBOutlet weak var label_Academic: UILabel!
    var academic = PieChartDataEntry(value: 0)
    var hostel = PieChartDataEntry(value: 0)
    var library = PieChartDataEntry(value: 0)
    var transportation = PieChartDataEntry(value: 0)
    var refresh:Bool = false
    var refreshControl = UIRefreshControl()
    var uicolorarray = [UIColor]()
    
    
    var numentries = [PieChartDataEntry]()
   
    var point:[Double] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavigationBarItem()
        
        
        self.title = "Today's Collection"
        collection_PieChart.chartDescription?.text = ""
        collection_PieChart.usePercentValuesEnabled = true
        transportation.label = "Transportation"
        library.label = "Library"
        hostel.label = "Hostel"
        academic.label = "Academic"
        self.label_Academic.textColor = Color.totalstuden
        self.label_Hostel.textColor = Color.first
        self.label_Library.textColor = Color.total_present
        self.label_Transportation.textColor = Color.dueamount
        
       
      

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
         loadFromAPI(offSetNormal: 0)
    }
    func setPieChart(){
        numentries.removeAll()
        uicolorarray.removeAll()
        if academic.value != 0{
            numentries.append(academic)
            uicolorarray.append(Color.totalstuden)
            }
        if hostel.value != 0{
            numentries.append(hostel)
            uicolorarray.append(Color.first)
            
        }
        if library.value != 0{
            numentries.append(library)
            uicolorarray.append(Color.total_present)
            
        }
        if transportation.value != 0{
            numentries.append(transportation)
            uicolorarray.append(Color.dueamount)
            
        }
      
        let dataset = PieChartDataSet(values: numentries, label: nil)
        let pieobject = PieChartData(dataSet: dataset)
        dataset.colors = uicolorarray
        collection_PieChart.data = pieobject
        
    }

    func loadFromAPI(offSetNormal:Int){
        
        showHud("Loading...")
        
        var paramater = [String:AnyObject]()
        let id=userDefaults.integer(forKey:"teamId")
        
        paramater = ["team_id":id as AnyObject]
        
        print(paramater)
        Alamofire.request(LibraryAPI.todaysCollection, method: .post, parameters: paramater)
            
            .responseJSON(completionHandler: { (response) -> Void in
                print(response)
                if(response.result.isSuccess) {
                    let json = JSON(response.data)
                    self.academic.value = json["academic_collection"].doubleValue
                    self.hostel.value = json["hostel_collection"].doubleValue
                    self.library.value = json["library_collection"].doubleValue
                    self.transportation.value = json["transportation_collection"].doubleValue
                    self.label_Academic.text = "Rs. " + String(json["academic_collection"].doubleValue)
                     self.label_Hostel.text = "Rs. " + String(json["hostel_collection"].doubleValue)
                     self.label_Library.text = "Rs. " + String(json["library_collection"].doubleValue)
                     self.label_Transportation.text = "Rs. " + String(json["transportation_collection"].doubleValue)
                    self.setPieChart()
                    self.hideHUD()
                } else{
                    self.hideHUD()
                    LibraryToast.notifyUser("ALERT", message:"Something Went Wrong Please Try Again" , vc: self)
                    
                    
                }
                
                
                
            })
        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
