//
//  ExaminationFullView.swift
//  DNFounder
//
//  Created by Er. Aditya Raj on 6/29/18.
//  Copyright © 2018 Digital Nepal. All rights reserved.
//

import UIKit
import Charts
import Alamofire
import SwiftyJSON

class ExaminationFullView: UIViewController {
    @IBOutlet weak var view_main: UIView!
    var data = JSON()
    @IBOutlet weak var barChart: BarChartView!
    @IBOutlet weak var totalstudent: UILabel!
    @IBOutlet weak var absent: UILabel!
    @IBOutlet weak var examattendent: UILabel!
    @IBOutlet weak var fail: UILabel!
    @IBOutlet weak var pass: UILabel!
    @IBOutlet weak var thirddivision: UILabel!
    @IBOutlet weak var seconddivision: UILabel!
    @IBOutlet weak var firstdivision: UILabel!
    @IBOutlet weak var distinction: UILabel!
    let values:[String] = ["a","b","c","d","e","f","g","h","i"]
    var point:[Double] = []
    var responseData = JSON()
    override func viewDidLoad() {
        super.viewDidLoad()
        view_main.isHidden = true
        self.title = "Details"
        distinction.textColor = Color.distinction
        firstdivision.textColor = Color.first
        seconddivision.textColor = Color.second
        fail.textColor = Color.failed
        pass.textColor = Color.pass
        examattendent.textColor = Color.examattend
        totalstudent.textColor = Color.totalstuden
        thirddivision.textColor = Color.third
        absent.textColor = Color.Blue
        loadFromAPI(offSetNormal: 0)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func loadFromAPI(offSetNormal:Int){
        showHud("Loading...")
        var paramater = [String:AnyObject]()
        let teamId = userDefaults.integer(forKey:  "teamId")
        let id = data["id"].intValue
        
        paramater = ["team_id":teamId as AnyObject,"exam_id":id as AnyObject]
        
        
        print(paramater)
        Alamofire.request(LibraryAPI.examfullview, method: .post, parameters: paramater)
            
            
            .responseJSON(completionHandler: { (response) -> Void in
                print(response)
                if(response.result.isSuccess) {
                    let json = JSON(response.data)
                    self.responseData = json["report"]
                   self.hideHUD()
                    self.view_main.isHidden = false
                    self.setDatatoArray()
                    
                    
                } else{
                    self.hideHUD()
                    LibraryToast.notifyUser("ALERT", message:"Something Went Wrong Please Try Again" , vc: self)
                    }
                })
        
        
        
    }
    func setDatatoArray() {
        self.point.append(responseData["distinction"].doubleValue)
       self.point.append(responseData["first_division"].doubleValue)
        self.point.append(responseData["second_division"].doubleValue)
        self.point.append(responseData["third_division"].doubleValue)
        self.point.append(responseData["passed_students"].doubleValue)
         self.point.append(responseData["failed_students"].doubleValue)
        self.point.append(responseData["total_students"].doubleValue - responseData["exam_attended_students"].doubleValue)
        self.point.append(responseData["exam_attended_students"].doubleValue)
        self.point.append(responseData["total_students"].doubleValue)
        
   
       distinction.text = String(self.responseData["distinction"].intValue)
        firstdivision.text = String(self.responseData["first_division"].intValue)
        seconddivision.text = String(self.responseData["second_division"].intValue)
       fail.text = String(self.responseData["failed_students"].intValue)
        pass.text = String(self.responseData["passed_students"].intValue)
        examattendent.text = String(responseData["exam_attended_students"].intValue)
        totalstudent.text = String(self.responseData["total_students"].intValue)
        thirddivision.text = String(self.responseData["third_division"].intValue)
        absent.text = String(self.responseData["total_students"].intValue - self.responseData["exam_attended_students"].intValue)
        self.setBarChart(point: point, string: values)
    }
    func setBarChart(point:[Double],string:[String]){
        barChart.noDataText = "No Data Available"
        barChart.chartDescription?.text = ""
        var dataentryset:[BarChartDataEntry] = []
        var counter = 0.0
        for i in 0..<point.count{
            counter += 1.0
            let dataEntry = BarChartDataEntry(x: counter, y: point[i])
            dataentryset.append(dataEntry)
            
            
        }
        let chardDataset = BarChartDataSet(values: dataentryset, label: nil)
        let chartData = BarChartData()
        chartData.addDataSet(chardDataset)
        barChart.data = chartData
        chardDataset.colors = [Color.distinction,Color.first,Color.second,Color.third,Color.pass,Color.failed,Color.Blue,Color.examattend,Color.totalstuden]
        barChart.animate(xAxisDuration: 2.0, yAxisDuration: 2.0)
        }
    



}
