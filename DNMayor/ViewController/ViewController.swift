//
//  ViewController.swift
//  DNFounder
//
//  Created by Er. Aditya Raj on 5/11/18.
//  Copyright © 2018 Digital Nepal. All rights reserved.
//

import UIKit

enum VersionError: Error {
    case invalidBundleInfo
    case invalidResponse
    case FileCorrupted
}


class ViewController: UIViewController,UIGestureRecognizerDelegate {

    
    @IBOutlet weak var view_AttendanceRprt: UIView!
    @IBOutlet weak var view_ExmReport: UIView!
    @IBOutlet weak var view_DialyRtn: UIView!
    @IBOutlet weak var view_Notice: UIView!
    @IBOutlet weak var view_Inventory: UIView!
    @IBOutlet weak var view_AccountReport: UIView!
    
    @IBOutlet weak var img_examination: UIImageView!
    @IBOutlet weak var img_notice: UIImageView!
    @IBOutlet weak var img_inventory: UIImageView!
    @IBOutlet weak var img_accountreport: UIImageView!
    @IBOutlet weak var img_attendance: UIImageView!
    @IBOutlet weak var img_TodaysAccount: UIImageView!
    @IBOutlet weak var label_welcome: UILabel!
    
    let currentVersion  = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
    
    
    func isUpdateAvailable() throws -> Bool {
        guard let info = Bundle.main.infoDictionary,
            let currentVersion = info["CFBundleShortVersionString"] as? String,
            let identifier = info["CFBundleIdentifier"] as? String,
            let url = URL(string: "http://itunes.apple.com/lookup?bundleId=\(identifier)") else {
                throw VersionError.invalidBundleInfo
                
        }
        let data = try Data(contentsOf: url)
        guard let json = try JSONSerialization.jsonObject(with: data, options: [.allowFragments]) as? [String: Any] else {
            throw VersionError.invalidResponse
        }
        if let result = (json["results"] as? [Any])?.first as? [String: Any], let version = result["version"] as? String {
            print("version in app store", version,currentVersion);
            
            return version != currentVersion
        }
        return false
        
    }
    func popupUpdateDialogue(){
        //        var versionInfo = ""
        //        do {
        ////            versionInfo = try self.globalObjectHome.getAppStoreVersion()
        //        }catch {
        //            print(error)
        //        }
        
        
        let alertMessage = "A new version of is availabe in appstore ";
        let alert = UIAlertController(title: "New Version Available", message: alertMessage, preferredStyle: UIAlertControllerStyle.alert)
        
        let okBtn = UIAlertAction(title: "Update", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            if let url = URL(string: "https://apps.apple.com/us/app/digital-nepal-mayor/id1457831087?ls=1"),
                UIApplication.shared.canOpenURL(url){
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        })
        let noBtn = UIAlertAction(title:"Skip this Version" , style: .destructive, handler: {(_ action: UIAlertAction) -> Void in
        })
        alert.addAction(okBtn)
        alert.addAction(noBtn)
        self.present(alert, animated: true, completion: nil)
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavigationBarItem()
        
        label_welcome.text = "Welcome \(userDefaults.string(forKey: "name")!)"
        imageDesign()
        DispatchQueue.global().async {
            do {
                let update = try self.isUpdateAvailable()
                
                print("update",update)
                DispatchQueue.main.async {
                    if update{
                        self.popupUpdateDialogue();
                    }
                    
                }
            } catch {
                print(error)
            }
        }

         self.title="Home"
        let button1 = UIBarButtonItem(image: UIImage(named: "d"), style: .plain, target: self, action: #selector(self.logout))
        self.navigationItem.rightBarButtonItem  = button1
  
        
        //click Notice
        let notice = UITapGestureRecognizer(target: self, action: #selector(ViewController.noticeTapped(gesture:)))
        // add it to the image view;
        view_Notice.addGestureRecognizer(notice)
        
        //click examination
        let examination = UITapGestureRecognizer(target: self, action: #selector(ViewController.exmTapped(gesture:)))
        // add it to the image view;
        view_ExmReport.addGestureRecognizer(examination)
        
        //click dailyRoutine
        let dailyRoutine = UITapGestureRecognizer(target: self, action: #selector(ViewController.rtnTapped(gesture:)))
        // add it to the image view;
        view_DialyRtn.addGestureRecognizer(dailyRoutine)
        
        //click inventoryrpt
        let inventoryrpt = UITapGestureRecognizer(target: self, action: #selector(ViewController.inventoryTapped(gesture:)))
        // add it to the image view;
        view_Inventory.addGestureRecognizer(inventoryrpt)
        
        //click attendance
        let attendance = UITapGestureRecognizer(target: self, action: #selector(ViewController.attendanceTapped(gesture:)))
        // add it to the image view;
        view_AttendanceRprt.addGestureRecognizer(attendance)
        
        //click accountrpt
        let accountrpt = UITapGestureRecognizer(target: self, action: #selector(ViewController.accountrptTapped(gesture:)))
        // add it to the image view;
        view_AccountReport.addGestureRecognizer(accountrpt)
        
    }
    func imageDesign(){
        img_notice.layer.cornerRadius = img_notice.frame.size.width/2
        img_notice.clipsToBounds = true
        
        img_inventory.layer.cornerRadius = img_inventory.frame.size.width/2
        img_inventory.clipsToBounds = true
        
        img_attendance.layer.cornerRadius = img_attendance.frame.size.width/2
        img_attendance.clipsToBounds = true
        
        img_examination.layer.cornerRadius = img_examination.frame.size.width/2
        img_examination.clipsToBounds = true
        
        img_accountreport.layer.cornerRadius = img_accountreport.frame.size.width/2
        img_accountreport.clipsToBounds = true
        
        img_TodaysAccount.layer.cornerRadius = img_TodaysAccount.frame.size.width/2
        img_TodaysAccount.clipsToBounds = true
    }
    @objc func logout(){
       
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "MayorMainViewController");
        let navController = UINavigationController(rootViewController: myVC!)
        
        self.navigationController?.present(navController, animated: true, completion: nil)
     
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @objc func noticeTapped(gesture: UIGestureRecognizer) {
        print("CLICKED")
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let gunasoDetailVC = storyboard.instantiateViewController(withIdentifier: "NoticeVC") as! NoticeVC
        
        self.navigationController?.pushViewController(gunasoDetailVC, animated: true)
        
        
    }
    @objc func exmTapped(gesture: UIGestureRecognizer) {
        print("CLICKED")
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let gunasoDetailVC = storyboard.instantiateViewController(withIdentifier: "ExaminationVC") as! ExaminationVC
        
        self.navigationController?.pushViewController(gunasoDetailVC, animated: true)
        
        
    }
    @objc func rtnTapped(gesture: UIGestureRecognizer) {
        print("CLICKED")
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let gunasoDetailVC = storyboard.instantiateViewController(withIdentifier: "DayBookMainVC") as! DayBookMainVC
        
        self.navigationController?.pushViewController(gunasoDetailVC, animated: true)
        
        
    }
    @objc func inventoryTapped(gesture: UIGestureRecognizer) {
        print("CLICKED")
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let gunasoDetailVC = storyboard.instantiateViewController(withIdentifier: "InventoryControlVC") as! InventoryControlVC
        
        self.navigationController?.pushViewController(gunasoDetailVC, animated: true)
        
        
    }
    @objc func attendanceTapped(gesture: UIGestureRecognizer) {
        print("CLICKED")
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let gunasoDetailVC = storyboard.instantiateViewController(withIdentifier: "AttendanceMainVC") as! AttendanceMainVC
        
        self.navigationController?.pushViewController(gunasoDetailVC, animated: true)
        
        
    }
    @objc func accountrptTapped(gesture: UIGestureRecognizer) {
        print("CLICKED")
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let gunasoDetailVC = storyboard.instantiateViewController(withIdentifier: "NewAccountReportVC") as! NewAccountReportVC
        
        self.navigationController?.pushViewController(gunasoDetailVC, animated: true)
        
        
    }
 


}

