//
//  DayBookMainVC.swift
//  DNFounder
//
//  Created by mac on 8/5/19.
//  Copyright © 2019 E-Digital Nepal. All rights reserved.
//

import UIKit
import  SwiftyJSON
import Alamofire

class DayBookMainVC: UIViewController,UIGestureRecognizerDelegate {
    @IBOutlet weak var l_Opeaningbalance: UILabel!
    
    @IBOutlet weak var l_BankTransactionsTotal: UILabel!
    @IBOutlet weak var l_TotalExpenditure: UILabel!
    @IBOutlet weak var l_Totalncome: UILabel!
    var income :[JSON] = [JSON]()
    var expenditure :[JSON] = [JSON]()
    var bankTransactions:[JSON] = [JSON]()
    
    @IBOutlet weak var l_ClosingBalance: UILabel!
    @IBOutlet weak var l_BalanceAmount: UITextView!
    @IBOutlet weak var l_PaidAmount: UITextView!
    @IBOutlet weak var txt_ReceivedAmt: UITextView!
    @IBOutlet weak var txt_Date: UITextView!
    
    @IBOutlet weak var view_BankTransaction: UIView!
    @IBOutlet weak var view_Expenditure: UIView!
    @IBOutlet weak var view_Income: UIView!
    
    var incomeAmt:Decimal = 0.0
    var dl:Decimal = 0.0
    var withdraw:Decimal = 0.0
    var expenditureAmt:Decimal = 0.0
    var bankTransactionAmt:Decimal = 0.0
    var opeaningBalance:Decimal = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavigationBarItem()
        
        self.title = "Day Book"
        let button1 = UIBarButtonItem(image: UIImage(named: "reload"), style: .plain, target: self, action: #selector(self.openFilter))
        self.navigationItem.rightBarButtonItem  = button1
        
        //click income
        let income = UITapGestureRecognizer(target: self, action: #selector(DayBookMainVC.incomeTap(gesture:)))
        view_Income.addGestureRecognizer(income)
        
        //click expenditure
        let exp = UITapGestureRecognizer(target: self, action: #selector(DayBookMainVC.expenditureTap(gesture:)))
        view_Expenditure.addGestureRecognizer(exp)
        
        //click banktransactions
        let bank = UITapGestureRecognizer(target: self, action: #selector(DayBookMainVC.bankTap(gesture:)))
        view_BankTransaction.addGestureRecognizer(bank)
        
        
        
        loadDayBook()
        
        // Do any additional setup after loading the view.
    }
    @objc func openFilter(){
        dl = 0.0
        withdraw = 0.0
        expenditureAmt = 0.0
        incomeAmt = 0.0
        bankTransactionAmt = 0.0
        loadDayBook()
        
    }
    
    
    @objc func incomeTap(gesture: UIGestureRecognizer) {
        
        if self.incomeAmt != 0{
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let dailyBook = storyboard.instantiateViewController(withIdentifier: "DayBookVC") as! DayBookVC
            dailyBook.responseData = self.income
            dailyBook.total = self.incomeAmt
            dailyBook.titleText = "Received/Sales/Cash"
            dailyBook.type = 1
            self.navigationController?.pushViewController(dailyBook, animated: true)
        }else{
            LibraryToast.notifyUser("ALERT", message:"Not Available!" , vc: self)
        }
        
        
    }
    
    @objc func expenditureTap(gesture: UIGestureRecognizer) {
        if self.expenditureAmt != 0{
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let dailyBook = storyboard.instantiateViewController(withIdentifier: "DayBookVC") as! DayBookVC
            dailyBook.responseData = self.expenditure
            dailyBook.total = self.expenditureAmt
            dailyBook.type = 2
            dailyBook.titleText = "Payment/Expenditure/Purchase"
            self.navigationController?.pushViewController(dailyBook, animated: true)
            
        }else{
            LibraryToast.notifyUser("ALERT", message:"Not Available!" , vc: self)
        }
        
        
    }
    @objc func bankTap(gesture: UIGestureRecognizer) {
        
        if self.bankTransactionAmt != 0{
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let dailyBook = storyboard.instantiateViewController(withIdentifier: "DayBookVC") as! DayBookVC
            dailyBook.responseData = self.bankTransactions
            dailyBook.total = self.bankTransactionAmt
            dailyBook.type = 3
            dailyBook.titleText = "Bank Transactions"
            self.navigationController?.pushViewController(dailyBook, animated: true)
            
        }else{
            LibraryToast.notifyUser("ALERT", message:"Not Available!" , vc: self)
        }
        
        
    }
    
    func totalIncome(){
        for i in income{
            self.incomeAmt = self.incomeAmt+Decimal(string: i["receivedAmount"].stringValue)!
            
        }
        print(self.incomeAmt)
    }
    
    func totalExpenditure(){
        for i in self.expenditure{
            self.expenditureAmt = self.expenditureAmt+Decimal(string: i["paidAmount"].stringValue)!
            
        }
        print(self.expenditureAmt)
    }
    func bankTransactionTotal (){
        for i in self.bankTransactions{
            if i["txnNature"].stringValue == "Withdraw"{
                self.withdraw = self.withdraw+Decimal(string: i["paidAmount"].stringValue)!
                self.bankTransactionAmt = self.bankTransactionAmt+Decimal(string: i["paidAmount"].stringValue)!
            }else{
                self.dl = self.dl+Decimal(string: i["receivedAmount"].stringValue)!
                self.bankTransactionAmt = self.bankTransactionAmt+Decimal(string: i["receivedAmount"].stringValue)!
            }
            
            
        }
        
    }
    
    
    
    func loadDayBook(){
        showHud("Loading...")
        var paramater = [String:AnyObject]()
        let id=userDefaults.integer(forKey:"teamId")
        print(id)
        paramater = ["teamId":id as AnyObject,"operationDateSetting":"np" as AnyObject]
        Alamofire.request(LibraryAPI.daybook, method: .post, parameters: paramater)
            
            .responseJSON(completionHandler: { (response) -> Void in
                print(response)
                if(response.result.isSuccess) {
                    let j = JSON(response.data)
                    self.bankTransactions.removeAll()
                    self.income.removeAll()
                    self.expenditure.removeAll()
                    if j["success"].boolValue{
                        let body = j["body"]
                        self.opeaningBalance = Decimal(string: body["openingBalance"][0]["closingBalance"].stringValue)!
                        self.expenditure = body["expenditure"].arrayValue
                        self.income = body["income"].arrayValue
                        self.bankTransactions = body["bankTransactions"].arrayValue
                        if self.expenditure.count != 0{
                            self.totalExpenditure()
                        }
                        if self.income.count != 0{
                            self.totalIncome()
                        }
                        if self.bankTransactions.count != 0{
                            self.bankTransactionTotal()
                        }
                        
                        self.l_Totalncome.text = "Rs. \(self.incomeAmt)"
                        self.l_TotalExpenditure.text = "Rs. \(self.expenditureAmt)"
                        self.l_BankTransactionsTotal.text = "Rs. \(self.bankTransactionAmt)"
                        self.txt_ReceivedAmt.text = "\(self.incomeAmt+self.dl)"
                        self.l_PaidAmount.text = "\(self.expenditureAmt+self.withdraw)"
                        self.l_BalanceAmount.text = "\(self.incomeAmt+self.dl-self.expenditureAmt-self.withdraw)"
                        self.l_Opeaningbalance.text = "Rs. \(self.opeaningBalance)"
                        self.l_ClosingBalance.text = "Rs. \(self.opeaningBalance+self.incomeAmt+self.dl-self.expenditureAmt-self.withdraw)"
                        self.txt_Date.text = DateInString.getCurrentDateNep()
                        
                        
                    }else{
                        LibraryToast.notifyUser("ALERT", message:j["message"].stringValue , vc: self)
                    }
                    
                    
                    self.hideHUD()
                } else{
                    self.hideHUD()
                    LibraryToast.notifyUser("ALERT", message: "Check your internet connection and Reload" , vc: self)
                    //                    let alert = UIAlertController(title: "ALERT!  ", message: "Check your internet connections and Try Again", preferredStyle: .alert)
                    //
                    //
                    //                    alert.addAction(UIAlertAction(title: "Retry", style: .destructive, handler:{ action in
                    //
                    //                       self.loadDayBook()
                    //                    }))
                    //
                    //                    self.present(alert, animated: true)
                }
                
                
                
            })
        
        
    }
    
}

