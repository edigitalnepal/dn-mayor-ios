//
//  AttendanceReportVC.swift
//  DNFounder
//
//  Created by Er. Aditya Raj on 5/14/18.
//  Copyright © 2018 Digital Nepal. All rights reserved.
//

import UIKit
import Charts
import SwiftyJSON
import Alamofire
import DZNEmptyDataSet
class AttendanceReportVC: UIViewController {
    let cellIdentifier = "ExaminationCell"
    var responseData :[JSON] = [JSON]()
    var totolcount = 0
    var pageNumber = 0
    var type = Int()
    var refresh:Bool = false
    var refreshControl = UIRefreshControl()
    var uicolorarray = [UIColor]()
    let values:[String] = ["a","b","c"]
    var point:[Double] = []
    
    @IBOutlet weak var tableview_Attendance: UITableView!
    
    @IBOutlet weak var btn_Detail: UIButton!
    
    @IBOutlet weak var todays_Date: UILabel!
    @IBOutlet weak var piechart_Attendance: PieChartView!
    
    var total_absent_student = PieChartDataEntry(value: 0)
    var total_attendance_done = PieChartDataEntry(value: 0)
    var total_classes = PieChartDataEntry(value: 0)
    var total_present = PieChartDataEntry(value: 0)
    var total_students = PieChartDataEntry(value: 0)
 

    var numentries = [PieChartDataEntry]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavigationBarItem()
        self.title="Attendance Report"
        
        piechart_Attendance.chartDescription?.text = ""
        total_absent_student.label = "Absent(%)"
        total_present.label = "Present(%)"
        piechart_Attendance.noDataText = "No Data Available"
        piechart_Attendance.usePercentValuesEnabled = true
        piechart_Attendance.entryLabelColor = UIColor.black
        
        
        tableview_Attendance.delegate = self
        tableview_Attendance.dataSource = self
        tableview_Attendance.register(UINib(nibName: "ExaminationCell", bundle: Bundle.main), forCellReuseIdentifier: cellIdentifier)
        tableview_Attendance.estimatedRowHeight = 600.0
        tableview_Attendance.rowHeight = 600.0
        
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        loadFromAPI(offSetNormal: 0)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func setPieChart(){
        
        numentries.removeAll()
        uicolorarray.removeAll()
        if total_absent_student.value != 0{
          numentries.append(total_absent_student)
            uicolorarray.append(Color.total_absent_student)
        }
    
        if total_present.value != 0{
            numentries.append(total_present)
            uicolorarray.append(Color.total_present)
        }
        
      
       
        let dataset = PieChartDataSet(values: numentries, label: nil)
        let pieobject = PieChartData(dataSet: dataset)
        dataset.colors = uicolorarray

        piechart_Attendance.data = pieobject
        
    }
    
    @IBAction func click_Details(_ sender: Any) {
        
    }
    
 
    func loadFromAPI(offSetNormal:Int){
    
            showHud("Loading...")
   
        var paramater = [String:AnyObject]()
        let id=userDefaults.integer(forKey:"teamId")
        
        paramater = ["team_id":id as AnyObject]
        
        print(paramater)
        Alamofire.request(LibraryAPI.attendanceUrl, method: .post, parameters: paramater)
            
            .responseJSON(completionHandler: { (response) -> Void in
                print(response)
                if(response.result.isSuccess) {
                    
                    let json = JSON(response.data)
                    self.responseData = json["datas"].arrayValue
                    if self.responseData.count != 0{
                        self.total_absent_student.value = json["total_absent_student"].doubleValue
                        self.total_classes.value = json["total_classes"].doubleValue
                        self.total_present.value = json["total_present"].doubleValue
                        self.total_students.value = json["total_students"].doubleValue
                        self.todays_Date.text = self.responseData[0]["attendance_date"].stringValue
                        self.setPieChart()
                        self.tableview_Attendance.reloadData()
                    }else{
                      LibraryToast.notifyUser("ALERT", message:"No Data Available" , vc: self)
                    }
                
                    
              self.hideHUD()
                } else{
                    self.hideHUD()
                      LibraryToast.notifyUser("ALERT", message:"Something Went Wrong Please Try Again" , vc: self)
                    
                    
                }
              
                
                
            })
    
        
        
    }
    



}
extension AttendanceReportVC :UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return responseData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? ExaminationCell
        cell?.selectionStyle = UITableViewCellSelectionStyle.none
        point.removeAll()
        self.point.append(self.responseData[indexPath.row]["total_student"].doubleValue)
        self.point.append(self.responseData[indexPath.row]["absent_student"].doubleValue)
        self.point.append(self.responseData[indexPath.row]["present_student"].doubleValue)
        
        cell?.label_ExamName.text = self.responseData[indexPath.row]["classroom_name"].stringValue
        cell?.label_Absent.text = String(self.responseData[indexPath.row]["absent_student"].intValue)
        cell?.label_Present.text = String(self.responseData[indexPath.row]["present_student"].intValue)
        cell?.label_totalstuden.text = String(self.responseData[indexPath.row]["total_student"].intValue)
        
       
         cell?.setBarChart(point: point, string: values)
        
        return cell!
    }
    
    
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        


        let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "StudentListVC") as! StudentListVC
        popOverVC.responseData = responseData[indexPath.row]["student_list"] .arrayValue

        popOverVC.modalPresentationStyle = .overFullScreen
        popOverVC.modalTransitionStyle = .crossDissolve
        self.present(popOverVC, animated: true, completion: nil)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 600.0
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 600.0
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
    }
    
}
extension AttendanceReportVC : DZNEmptyDataSetDelegate, DZNEmptyDataSetSource {
    
    
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let str = ""
        let attrs = [NSAttributedStringKey.font: UIFont.preferredFont(forTextStyle: UIFontTextStyle.headline)]
        return NSAttributedString(string: str, attributes: attrs)
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let str = "N0 Data Available"
        let attrs = [NSAttributedStringKey.font: UIFont.preferredFont(forTextStyle: UIFontTextStyle.body)]
        return NSAttributedString(string: str, attributes: attrs)
    }
    
    
    func buttonTitle(forEmptyDataSet scrollView: UIScrollView!, for state: UIControlState) -> NSAttributedString! {
        let str = "RELOAD"
        let attrs = [NSAttributedStringKey.font: UIFont.preferredFont(forTextStyle: UIFontTextStyle.body)]
        return NSAttributedString(string: str, attributes: attrs)
    }
    
    func emptyDataSetDidTapButton(_ scrollView: UIScrollView!) {
        refresh = false
        responseData.removeAll()
        loadFromAPI(offSetNormal:0)
        
    }
}

