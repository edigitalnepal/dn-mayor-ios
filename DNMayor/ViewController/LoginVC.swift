//
//  LoginVC.swift
//  DNFounder
//
//  Created by Er. Aditya Raj on 5/13/18.
//  Copyright © 2018 Digital Nepal. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
class LoginVC: UIViewController {
     let userDefaults = UserDefaults.standard
    var fromwhere = false
    @IBOutlet var view_password: UIView!
    @IBOutlet var btn_see: UIButton!
    @IBOutlet weak var btn_login: UIButton!
    @IBOutlet weak var image_arrow: UIImageView!
    @IBOutlet weak var textfield_password: UITextField!
    @IBOutlet weak var textfield_username: UITextField!
    @IBOutlet weak var login_specific_view: UIView!
    
    @IBOutlet weak var login_text_view: UIView!
    @IBOutlet weak var login_main_view: UIView!
    var iconClick : Bool!
    let blind: UIImage = UIImage(named: "blind")!
    let see: UIImage = UIImage(named: "see")!
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Login"
        iconClick = true
        self.image_arrow.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
        view_password.layer.cornerRadius = 5
        
        
        let rectShape = CAShapeLayer()
        rectShape.bounds = login_text_view.frame
        rectShape.position = login_text_view.center
        rectShape.path = UIBezierPath(roundedRect: login_text_view.bounds, byRoundingCorners: [.topRight,.bottomRight], cornerRadii: CGSize(width: 20, height: 20)).cgPath
        login_text_view.layer.mask = rectShape
        login_text_view.backgroundColor = Color.colorFb

        login_specific_view.backgroundColor = Color.colorFb
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  


    
    @IBAction func see_btn_click(_ sender: Any) {
        
        if(iconClick == true) {
            textfield_password.isSecureTextEntry = false
            iconClick = false
            btn_see.setImage(blind, for: .normal)
        } else {
            textfield_password.isSecureTextEntry = true
            iconClick = true
            btn_see.setImage(see, for: .normal)
        }
    }
    
    @IBAction func click_LoginBtn(_ sender: Any) {
      getDataFromTextField()
    }
    override func viewDidAppear(_ animated: Bool) {
        
        if fromwhere{
            navigationItem.hidesBackButton = true;
        }

        let status:Bool = userDefaults.bool(forKey: "status")
        if  status{
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let mayorMainViewController = storyboard.instantiateViewController(withIdentifier: "MayorMainViewController") as! MayorMainViewController
        self.navigationController?.pushViewController(mayorMainViewController, animated: true)
        }
    }
    
 
    func getDataFromTextField(){
        
        if textfield_username.text != nil && textfield_username.text != "" && textfield_password.text != nil && textfield_password.text != "" {
            login(username: textfield_username.text!, password: textfield_password.text!)
        } else {
            LibraryToast.notifyUser("ALERT", message:"UserName or Password is Empty" , vc: self)

        }
        
    }
    func login( username: String, password:String  )  {
       
            var paramater = [String:AnyObject]()
            showHud("Loading....")
            paramater = ["usernameOrEmail":username as AnyObject, "password":password as AnyObject]
        print(paramater)
        Alamofire.request(LibraryAPI.loginUrl, method: .post, parameters: paramater)
            
            
            .responseJSON(completionHandler: { (response) -> Void in
                print(response)
                if(response.result.isSuccess) {
                    
                    let json = JSON(response.data)
                    let body = json["body"]
                
                   
    
                    if json["success"].boolValue == true {
                        self.userDefaults.setValue(body["id"].intValue, forKey: "SuperId")
                    
                        self.userDefaults.setValue(body["name"].stringValue, forKey: "Sname")
                        self.userDefaults.setValue(true, forKey: "status")
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let mayorMainViewController = storyboard.instantiateViewController(withIdentifier: "MayorMainViewController") as! MayorMainViewController
                self.navigationController?.pushViewController(mayorMainViewController, animated: true)
                     
                        
                    }else{
                          LibraryToast.notifyUser("ALERT", message:"Incorrect UserName or Password" , vc: self)
                    }
                
                    self.hideHUD()
                   
                   
                } else{
                    self.hideHUD()
                        LibraryToast.notifyUser("ALERT", message:"Internet Connection Problem or Server is down" , vc: self)
                    
                    
                }
                
            })
                
            
            
      
        
    }


}
