//
//  MayorMainViewController.swift
//  DNFounder
//
//  Created by digital nepal on 3/22/19.
//  Copyright © 2019 E-Digital Nepal. All rights reserved.
//

import UIKit
import Charts
import Alamofire
import SwiftyJSON
import Kingfisher


class MayorMainViewController: UIViewController {
        let userDefaults = UserDefaults.standard
    //design poperty
    @IBOutlet var pieChart_basicInfo: PieChartView!
    @IBOutlet var guardianse: UILabel!
    @IBOutlet var l_staff: UILabel!
    @IBOutlet var l_student: UILabel!
    @IBOutlet var l_school: UILabel!
     @IBOutlet weak var tableview_SchoolList: UITableView!
 
    @IBOutlet var barChart_StaffAttendance: BarChartView!
    @IBOutlet var barChart_StudentAttendace: BarChartView!
    
    //basic info piechart
    var student = PieChartDataEntry()
    var staff = PieChartDataEntry()
    var guardiance = PieChartDataEntry()
    var pieentires = [PieChartDataEntry]()
    var uiColorArray = [UIColor]()
    
    //attendace report
    var studentAttendanceData:[JSON] = [JSON]()
    var staffAttendanceData:[JSON] = [JSON]()
    let months = ["Baishak","Jestha","Ashad","Shrawan","Bhadra","Ashoj","Kartik","Mangsir","Poush","Magh","Falgun","Chaitra"]
    let maleColor = [UIColor.red,Color.male]
    let femaleColor = [UIColor.red,Color.female]
    let otherColor = [UIColor.red,Color.other]
    
    //tableview
    let cellIdentifier = "SchoolListTableViewCell"
    var responsDataOfSchoolList :[JSON] = [JSON]()
    var refresh:Bool = false
    var refreshControl = UIRefreshControl()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Welcome to \(UserDefaults.standard.string(forKey: "Sname") ?? "")"
        let button1 = UIBarButtonItem(image: UIImage(named: "logout"), style: .plain, target: self, action: #selector(self.logout)) // action:#selector(Class.MethodName) for swift 3
        self.navigationItem.rightBarButtonItem  = button1
        loadFromAPI()
        loadSchoolist()
        
        //refresh setup
        self.refreshControl.attributedTitle = NSAttributedString(string: "Refreshing")
        refreshControl.addTarget(self, action: #selector(refresh(sender:)), for: .valueChanged)
        self.tableview_SchoolList?.addSubview(refreshControl)
        
        //tabel view setup
        tableview_SchoolList.delegate = self
        tableview_SchoolList.dataSource = self
        tableview_SchoolList.register(UINib(nibName: "SchoolListTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: cellIdentifier)
        tableview_SchoolList.rowHeight = UITableViewAutomaticDimension
        tableview_SchoolList.estimatedRowHeight = 120
       
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
         navigationItem.hidesBackButton = true;
       
    }
    override func viewDidDisappear(_ animated: Bool) {
    navigationItem.hidesBackButton = false
    }
    
    @objc func refresh(sender:AnyObject) {
        refresh=true
        loadFromAPI()
        loadSchoolist()
    }
    
    func loadFromAPI(){
        
        if (!refresh){
           showHud("Loading...")
        }
        
       
        
        var paramater = [String:AnyObject]()
        let id=userDefaults.integer(forKey:"SuperId")
        
        
        paramater = ["superUserId":id as AnyObject]
        
    
        print(paramater)
        Alamofire.request(LibraryAPI.superUserBasicInfo, method: .post, parameters: paramater)
            
            .responseJSON(completionHandler: { (response) -> Void in
                print(response)
                if(response.result.isSuccess) {
                    let json = JSON(response.data)
                    let success = json["success"].boolValue
                    if (success){
                        let body = json["body"]
                        
                        //setting basicinfo
                        self.showBasicInfo(guardians: body["totalGuardians"].stringValue, school: body["totalSchools"].stringValue, staff: body["totalStaffs"].stringValue, student: body["totalStudents"].stringValue)
                        
                        //setting piechart
                        self.guardiance.value = body["totalGuardians"].doubleValue
                        self.student.value = body["totalStudents"].doubleValue
                        self.staff.value = body["totalStaffs"].doubleValue
                        self.setPieChart()
                        
                        
                        
                    }
                    print(json)
           
//                    self.setPieChart()
                    
                    self.hideHUD()
                } else{
                    self.hideHUD()
                    LibraryToast.notifyUser("ALERT", message:"Something Went Wrong Please Try Again" , vc: self)
                    
                    
                }
                
                
                
            })
        
        
        
    }
    func loadSchoolist(){

        if (!refresh){
            showHud("Loading...")
        }
        var paramater = [String:AnyObject]()
        let id=userDefaults.integer(forKey:"SuperId")
        paramater = ["superUserId":id as AnyObject]
        print(paramater)
        Alamofire.request(LibraryAPI.fetchSchoollist, method: .post, parameters: paramater)
            
            .responseJSON(completionHandler: { (response) -> Void in
                print(response)
                if(response.result.isSuccess) {
                    let json = JSON(response.data)
                    let success = json["success"].boolValue
                    if (success){
                        self.responsDataOfSchoolList.removeAll()
                       self.responsDataOfSchoolList = json["body"].arrayValue
                        if (self.responsDataOfSchoolList.count != 0){
                           self.tableview_SchoolList.reloadData()
                            
                        }
                    }
                    self.loadStudentAttendance()
        
                    self.hideHUD()
                } else{
                    self.hideHUD()
                    LibraryToast.notifyUser("ALERT", message:"Something Went Wrong Please Try Again" , vc: self)
                    
                    
                }
                self.refreshControl.endRefreshing()
                
                
                
            })
        
        
        
    }
    @objc func logout(){
        logoutDialog()

      
    }
    func loadStudentAttendance(){
        
        if (!refresh){
            showHud("Loading...")
        }
        
        var paramater = [String:AnyObject]()
       let id=userDefaults.integer(forKey:"SuperId")
        
        
        paramater = ["superUserId":id as AnyObject,"filterType":"yearly" as AnyObject]
        
        
        print(paramater)
        Alamofire.request(LibraryAPI.studentOveralAttendance, method: .post, parameters: paramater)
            
            .responseJSON(completionHandler: { (response) -> Void in
                print(response)
                if(response.result.isSuccess) {
                    
                    let json = JSON(response.data)
                    let success = json["success"].boolValue
                    if (success){
                        self.studentAttendanceData.removeAll()
                        self.studentAttendanceData = json["body"].arrayValue
                        self.setBarchartStudentAttendance()
                        
                        
                    }
                    self.loadStaffAttendance()
                    self.hideHUD()
                } else{
                    self.hideHUD()
                    LibraryToast.notifyUser("ALERT", message:"Something Went Wrong Please Try Again" , vc: self)
                    
                    
                }
                
                
                
            })
        
        
        
    }
    func setBarchartStudentAttendance(){
        barChart_StudentAttendace.noDataText = "You need to provide data for the chart."
        barChart_StudentAttendace.chartDescription?.text = ""
        //legend
        let legend = barChart_StudentAttendace.legend
        legend.enabled = false
        legend.horizontalAlignment = .right
        legend.verticalAlignment = .top
        legend.orientation = .horizontal
        legend.drawInside = true
        legend.yOffset = 7.0;
        legend.xOffset = 12.0;
        legend.yEntrySpace = 0.0;
        
        
        let xaxis = barChart_StudentAttendace.xAxis
        xaxis.drawGridLinesEnabled = true
        xaxis.labelPosition = .bottom
        xaxis.labelFont = UIFont.systemFont(ofSize: 10.0)
        xaxis.centerAxisLabelsEnabled = true
        xaxis.valueFormatter = IndexAxisValueFormatter(values:self.months)
        xaxis.granularity = 1
        
        
        let leftAxisFormatter = NumberFormatter()
        leftAxisFormatter.maximumFractionDigits = 1
        
        let yaxis = barChart_StudentAttendace.leftAxis
        yaxis.spaceTop = 0.15
        yaxis.axisMinimum = 0
        yaxis.drawGridLinesEnabled = false
        
        barChart_StudentAttendace.rightAxis.enabled = false
        barChart_StudentAttendace.noDataText = "You need to provide data for the chart."
        var dataEntries: [BarChartDataEntry] = []
        var dataEntries1: [BarChartDataEntry] = []
        var dataEntries2: [BarChartDataEntry] = []
        
        for i in 0..<self.studentAttendanceData.count{
            let item = self.studentAttendanceData[i]
            //            self.months.append(self.studentAttendanceData[i]["month"].stringValue)
            let dataEntry = BarChartDataEntry(x: Double(i), yValues:  [item["absentMales"].doubleValue,item["presentMales"].doubleValue])
            dataEntries.append(dataEntry)
            let dataEntry1 = BarChartDataEntry(x: Double(i), yValues:  [item["absentFemales"].doubleValue,item["presentFemales"].doubleValue])
            dataEntries1.append(dataEntry1)
            let dataEntry2 = BarChartDataEntry(x: Double(i), yValues:  [item["absentOthers"].doubleValue,item["presentOthers"].doubleValue])
            dataEntries2.append(dataEntry2)
            
        }
        
        
        
        let chartDataSet = BarChartDataSet(values: dataEntries, label: "Male")
        let chartDataSet1 = BarChartDataSet(values: dataEntries1, label: "Female")
        let chartDataSet2 = BarChartDataSet(values: dataEntries2, label: "Other")
        chartDataSet.colors = maleColor
        chartDataSet1.colors = femaleColor
        chartDataSet2.colors = otherColor
        
        
        
        let dataSets: [BarChartDataSet] = [chartDataSet,chartDataSet1,chartDataSet2]
        
        
        //let chartData = BarChartData(dataSet: chartDataSet)
        
        let chartData = BarChartData(dataSets: dataSets)
        
        
        let groupSpace = 0.2275
        let barSpace = 0.03
        let barWidth = 0.2275
        
        // (0.3 + 0.05) * 2 + 0.3 = 1.00 -> interval per "group"
        
        //        let groupCount = self.months.count
        let startYear = 0
        
        
        chartData.barWidth = barWidth;
        barChart_StudentAttendace.xAxis.axisMinimum = Double(startYear)
        let gg = chartData.groupWidth(groupSpace: groupSpace, barSpace: barSpace)
        print("Groupspace: \(gg)")
        barChart_StudentAttendace.xAxis.axisMaximum =  Double(startYear) + gg * Double(months.count)
        
        chartData.groupBars(fromX: Double(startYear), groupSpace: groupSpace, barSpace: barSpace)
        //        chartData.groupWidth(groupSpace: groupSpace, barSpace: barSpace)
        barChart_StudentAttendace.data = chartData
        barChart_StudentAttendace.zoomToCenter(scaleX: 2, scaleY: 0)
    }
    
    func setBarchartStaffAttendance(){
        barChart_StaffAttendance.noDataText = "You need to provide data for the chart."
        barChart_StaffAttendance.chartDescription?.text = ""
        //legend
        let legend = barChart_StaffAttendance.legend
        legend.enabled = false
        legend.horizontalAlignment = .right
        legend.verticalAlignment = .top
        legend.orientation = .horizontal
        legend.drawInside = true
        legend.yOffset = 7.0;
        legend.xOffset = 12.0;
        legend.yEntrySpace = 0.0;
        
        
        let xaxis = barChart_StaffAttendance.xAxis
        xaxis.drawGridLinesEnabled = true
        xaxis.labelPosition = .bottom
        xaxis.labelFont = UIFont.systemFont(ofSize: 10.0)
        xaxis.centerAxisLabelsEnabled = true
        xaxis.valueFormatter = IndexAxisValueFormatter(values:self.months)
        xaxis.granularity = 1
        
        
        let leftAxisFormatter = NumberFormatter()
        leftAxisFormatter.maximumFractionDigits = 1
        
        let yaxis = barChart_StaffAttendance.leftAxis
        yaxis.spaceTop = 0.15
        yaxis.axisMinimum = 0
        yaxis.drawGridLinesEnabled = false
        
        barChart_StaffAttendance.rightAxis.enabled = false
        barChart_StaffAttendance.noDataText = "You need to provide data for the chart."
        var dataEntries: [BarChartDataEntry] = []
        var dataEntries1: [BarChartDataEntry] = []
        var dataEntries2: [BarChartDataEntry] = []
        
        for i in 0..<self.staffAttendanceData.count{
            let item = self.staffAttendanceData[i]
            //            self.months.append(self.studentAttendanceData[i]["month"].stringValue)
            let dataEntry = BarChartDataEntry(x: Double(i), yValues:  [item["absentMales"].doubleValue,item["presentMales"].doubleValue])
            dataEntries.append(dataEntry)
            let dataEntry1 = BarChartDataEntry(x: Double(i), yValues:  [item["absentFemales"].doubleValue,item["presentFemales"].doubleValue])
            dataEntries1.append(dataEntry1)
            let dataEntry2 = BarChartDataEntry(x: Double(i), yValues:  [item["absentOthers"].doubleValue,item["presentOthers"].doubleValue])
            dataEntries2.append(dataEntry2)
            
        }
        
        
        
        let chartDataSet = BarChartDataSet(values: dataEntries, label: "Male")
        let chartDataSet1 = BarChartDataSet(values: dataEntries1, label: "Female")
        let chartDataSet2 = BarChartDataSet(values: dataEntries2, label: "Other")
        chartDataSet.colors = maleColor
        chartDataSet1.colors = femaleColor
        chartDataSet2.colors = otherColor
        
        
        
        let dataSets: [BarChartDataSet] = [chartDataSet,chartDataSet1,chartDataSet2]
        
        
        //let chartData = BarChartData(dataSet: chartDataSet)
        
        let chartData = BarChartData(dataSets: dataSets)
        
        
        let groupSpace = 0.2275
        let barSpace = 0.03
        let barWidth = 0.2275
        
        // (0.3 + 0.05) * 2 + 0.3 = 1.00 -> interval per "group"
        
        //        let groupCount = self.months.count
        let startYear = 0
        
        
        chartData.barWidth = barWidth;
        barChart_StaffAttendance.xAxis.axisMinimum = Double(startYear)
        let gg = chartData.groupWidth(groupSpace: groupSpace, barSpace: barSpace)
        print("Groupspace: \(gg)")
        barChart_StaffAttendance.xAxis.axisMaximum =  Double(startYear) + gg * Double(months.count)
        
        chartData.groupBars(fromX: Double(startYear), groupSpace: groupSpace, barSpace: barSpace)
        //        chartData.groupWidth(groupSpace: groupSpace, barSpace: barSpace)
        barChart_StaffAttendance.data = chartData
        barChart_StaffAttendance.zoomToCenter(scaleX: 2, scaleY: 0)
    }
    
    
    func loadStaffAttendance(){
        
        if (!refresh){
            showHud("Loading...")
        }
        
        var paramater = [String:AnyObject]()
        let id=userDefaults.integer(forKey:"SuperId")
        
        
        paramater = ["superUserId":id as AnyObject,"filterType":"yearly" as AnyObject]
        
        
        print(paramater)
        Alamofire.request(LibraryAPI.staffOveralAttendance, method: .post, parameters: paramater)
            
            .responseJSON(completionHandler: { (response) -> Void in
                print(response)
                if(response.result.isSuccess) {
                    
                    let json = JSON(response.data)
                    let success = json["success"].boolValue
                    if (success){
                        self.staffAttendanceData.removeAll()
                        self.staffAttendanceData = json["body"].arrayValue
                        self.setBarchartStaffAttendance()
                        
                        
                    }
                    self.hideHUD()
                } else{
                    self.hideHUD()
                    LibraryToast.notifyUser("ALERT", message:"Something Went Wrong Please Try Again" , vc: self)
                    
                    
                }
                
                
                
            })
        
        
        
    }
   
    
    func setPieChart(){
        student.label = "Students(%)"
        staff.label = "Staff(%)"
        guardiance.label = "Guardians(%)"
        pieChart_basicInfo.chartDescription?.text = ""
        pieChart_basicInfo.usePercentValuesEnabled = true
        pieentires.removeAll()
        uiColorArray.removeAll()
        
        if student.value != 0{
            pieentires.append(student)
            uiColorArray.append(Color.total_absent_student)
        }
        if staff.value != 0{
            pieentires.append(staff)
            uiColorArray.append(Color.total_present)
        }
        if guardiance.value != 0{
            pieentires.append(guardiance)
            uiColorArray.append(Color.disscount)
        }
     
        let dataset = PieChartDataSet(values: pieentires, label: nil)
        let pieobject = PieChartData(dataSet: dataset)
        dataset.colors = uiColorArray
        pieChart_basicInfo.data = pieobject
        pieChart_basicInfo.sizeToFit()
        pieChart_basicInfo.rotationEnabled = true
        
    }
    
    func showBasicInfo(guardians:String!,school:String!,staff:String!,student:String!){
        l_school.text = school
         l_staff.text = staff
         l_student.text = student
         guardianse.text = guardians
    }
    
    func logoutDialog(){
        let alert = UIAlertController(title: "Logout", message: "Do you want to logout",preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: { _ in
        
        }))
        alert.addAction(UIAlertAction(title: "Logout",
                                      style: UIAlertActionStyle.default,
                                      handler: {(_: UIAlertAction!) in
                                        DispatchQueue.main.async {
                                            self.userDefaults.removeObject(forKey:"status")
                                            self.userDefaults.synchronize()
                                            
                                        }
                                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                        let gunasoDetailVC = storyboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                                        gunasoDetailVC.fromwhere = true
                                        
                                        self.navigationController?.pushViewController(gunasoDetailVC, animated: true)
                                      
                                      
                                        
                                        
        }))
        self.present(alert, animated: true, completion: nil)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension MayorMainViewController :UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return responsDataOfSchoolList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? SchoolListTableViewCell
        cell?.selectionStyle = UITableViewCellSelectionStyle.none
        let item = responsDataOfSchoolList[indexPath.row]
        let logo = item["logo"].stringValue
        if logo.isEmpty {
              cell?.image_SchoolImage.image = UIImage(named:"logo")
        }
        else{
            let url = URL(string: "http://202.51.74.174/mis/files/school-logos/\(item["logo"])")
            cell?.image_SchoolImage.kf.setImage(with: url)
        }
        cell?.t_address.text = item["address"].stringValue
        cell?.l_schoolname.text = item["name"].stringValue
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
                print("clicked")
        let item = self.responsDataOfSchoolList[indexPath.row]
        
        DispatchQueue.main.async {
            self.userDefaults.setValue(item["teamId"].intValue, forKey: "teamId")
            self.userDefaults.setValue(item["mainSchoolAccountId"].intValue, forKey: "Id")
            self.userDefaults.setValue(item["address"].stringValue, forKey: "address")
            self.userDefaults.setValue(item["name"].stringValue, forKey: "name")
             self.userDefaults.setValue(item["email"].stringValue, forKey: "email")
            self.userDefaults.setValue(item["phone"].stringValue, forKey: "phone")
             self.userDefaults.setValue(item["mobile"].stringValue, forKey: "mobile")
            self.userDefaults.setValue(item["logo"].stringValue, forKey: "image")
            self.userDefaults.setValue(item["id"].stringValue, forKey: "schoolId")
        }

 
                let alert = UIAlertController(title: "View as", message: "View Report gives overal school dashboard and login gives detail school report",preferredStyle: UIAlertControllerStyle.alert)

                alert.addAction(UIAlertAction(title: "View Report", style: UIAlertActionStyle.default, handler: { _ in
                    //Cancel Action
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let mayorMainViewController = storyboard.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
                    self.navigationController?.pushViewController(mayorMainViewController, animated: true)
                }))
                alert.addAction(UIAlertAction(title: "Login",
                                              style: UIAlertActionStyle.default,
                                              handler: {(_: UIAlertAction!) in

                                                appDelegate.createMenuView()


                }))
                self.present(alert, animated: true, completion: nil)
    }
    
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
  
    
}
