//
//  LeftMenuViewController.swift
//  DNFounder
//
//  Created by Er. Aditya Raj on 5/11/18.
//  Copyright © 2018 Digital Nepal. All rights reserved.
//
import UIKit
enum LeftMenu: Int {
    
    case home = 0
    case accountreport
    case transactionlog
    case inventoryreport
    case notices
    case examination
    case daily
    case attendance
    case complain
    case leave
    //    case accountcollection

 

}


protocol LeftMenuProtocol : class {
    func changeViewController(_ menu: LeftMenu)
}

class LeftViewController : UIViewController, LeftMenuProtocol {
    var window:UIWindow?
    let userDefaults = UserDefaults.standard
    
    @IBOutlet weak var label_Email: UILabel!
    @IBOutlet weak var label_Name: UILabel!
    @IBOutlet weak var view_headerview: UIView!
  
    @IBOutlet weak var tableview: UITableView!
    
    @IBOutlet weak var image_profile: UIImageView!
    
    var menus = [String]()
    var menuImage = [String]()
    var mainViewController: UIViewController!
    var dailyRoutine:UIViewController!
     var attendance:UIViewController!
    var inventoryreport:UIViewController!
    var notices:UIViewController!
    var ExaminationVC:UIViewController!
    var AccountReportVC:UIViewController!
    var TodaysCollectionVC:UIViewController!
    var CaseOrComplainVC:UIViewController!
    var LeaveApplicationVC:UIViewController!
    var profile:UIViewController!
    var transactionLog:UIViewController!
    

    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //main
        let viewcontroller = storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        self.mainViewController = UINavigationController(rootViewController: viewcontroller)

      
        menuImage = ["home.png","account.png","transaction.png","inventory.png","notice.png","examinationReport.png","dailyroutine.png","attendanceReport.png","complain.png","dailyroutine.png"]
        menus = ["Home","Account Report","Transaction Log","Inventory","Notice Section","Examination Report","Daily Routine","Attendance Report","Complain & Case Report","Leave Application"]
        tableview.reloadData()
        
        
    }
    @IBAction func clickGotoProfile(_ sender: Any) {
        self.slideMenuController()?.changeMainViewController(self.profile, close: true)
        
    }
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableview.delegate = self
        tableview.dataSource = self
        self.view_headerview.backgroundColor = Color.Blue
        self.view.backgroundColor = UIColor.white
        self.image_profile.layer.cornerRadius = self.image_profile.frame.size.width/2
        self.image_profile.clipsToBounds = true
        let logo = userDefaults.string(forKey: "image")
        if (logo!.isEmpty) {
            self.image_profile.image = UIImage(named:"logo")
        }
        else{
            let url = URL(string: "http://202.51.74.174/mis/files/school-logos/\(logo ?? "")")
            self.image_profile.kf.setImage(with: url)
        }
//        let encodedImageData = userDefaults.string(forKey: "image")
//        let dataDecoded : Data = Data(base64Encoded: encodedImageData!, options: .ignoreUnknownCharacters)!
//        let decodedimage = UIImage(data: dataDecoded)
//        self.image_profile.image = decodedimage
        self.label_Name.text = userDefaults.string(forKey: "name")
      
        
        let profileView = UITapGestureRecognizer(target: self, action: #selector(LeftViewController.clickGotoProfile(_:)))
        view_headerview.addGestureRecognizer(profileView)
        
        let dr = storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        self.dailyRoutine = UINavigationController(rootViewController: dr)
        
        let attendance = storyboard?.instantiateViewController(withIdentifier: "AttendanceMainVC") as! AttendanceMainVC
        //attendance
        self.attendance = UINavigationController(rootViewController: attendance)
        let inventoryreport = storyboard?.instantiateViewController(withIdentifier: "InventoryControlVC") as! InventoryControlVC
        self.inventoryreport = UINavigationController(rootViewController: inventoryreport)
        //notice
        let notices = storyboard?.instantiateViewController(withIdentifier: "NoticeVC") as! NoticeVC
        self.notices = UINavigationController(rootViewController: notices)
        //examination
        let examination = storyboard?.instantiateViewController(withIdentifier: "ExaminationVC") as! ExaminationVC
        self.ExaminationVC = UINavigationController(rootViewController: examination)
        //account
        let accountrpt = storyboard?.instantiateViewController(withIdentifier: "AccountReportVC") as! AccountReportVC
        self.AccountReportVC = UINavigationController(rootViewController: accountrpt)
        //TodaysCollectionVC
        let TodaysCollectionVC = storyboard?.instantiateViewController(withIdentifier: "TodaysCollectionVC") as! TodaysCollectionVC
        self.TodaysCollectionVC = UINavigationController(rootViewController: TodaysCollectionVC)
        //CaseOrComplainVC
        let CaseOrComplainVC = storyboard?.instantiateViewController(withIdentifier: "CaseOrComplainVC") as! CaseOrComplainVC
        self.CaseOrComplainVC = UINavigationController(rootViewController: CaseOrComplainVC)
        //LeaveApplicationVC
        let LeaveApplicationVC = storyboard?.instantiateViewController(withIdentifier: "LeaveApplicationVC") as! LeaveApplicationVC
        self.LeaveApplicationVC = UINavigationController(rootViewController: LeaveApplicationVC)
        
        let p = storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        self.profile = UINavigationController(rootViewController: p)
        
        //Transaction Log
        let tl = storyboard?.instantiateViewController(withIdentifier: "TransactionLogFilterVC") as! TransactionLogFilterVC
        self.transactionLog = UINavigationController(rootViewController: tl)
       
    }
    
    func changeViewController(_ menu: LeftMenu) {
        switch menu {
        case .home:
            self.slideMenuController()?.changeMainViewController(self.mainViewController, close: true)
        case .accountreport:
            self.slideMenuController()?.changeMainViewController(self.AccountReportVC, close: true)
        case .attendance:
             self.slideMenuController()?.changeMainViewController(self.attendance, close: true)
        case .daily:
            self.slideMenuController()?.changeMainViewController(self.dailyRoutine, close: true)
        case .complain:
            self.slideMenuController()?.changeMainViewController(self.CaseOrComplainVC, close: true)
        case .inventoryreport:
            self.slideMenuController()?.changeMainViewController(self.inventoryreport, close: true)
   
        case .examination:
            self.slideMenuController()?.changeMainViewController(self.ExaminationVC, close: true)
        case .leave:
            self.slideMenuController()?.changeMainViewController(self.LeaveApplicationVC, close: true)
        case .notices:
            self.slideMenuController()?.changeMainViewController(self.notices, close: true)
    
           
        case .transactionlog:
            self.slideMenuController()?.changeMainViewController(self.transactionLog, close: true)
  
        }
    }

    
    func buttonActionLogout() {
        
        userDefaults.removeObject(forKey:"status")
        userDefaults.synchronize()
    
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "LoginVC")
        UIApplication.shared.keyWindow?.rootViewController = viewController
        
        
        
        
    }
}

extension LeftViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let menu = LeftMenu(rawValue: indexPath.row) {
            self.changeViewController(menu)
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if self.tableview == scrollView {
            
        }
    }
}

extension LeftViewController : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menus.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let menu = LeftMenu(rawValue: indexPath.row) {
            switch menu {
            case .home,.accountreport,.transactionlog,.inventoryreport,.notices,.examination,.daily, .attendance,.complain,.leave:
                let cell = tableview.dequeueReusableCell(withIdentifier: "LeftMenuContorllerCell") as! LeftMenuContorllerCell
                cell.label_menuName.text = (menus[indexPath.row])
                cell.separatorInset = UIEdgeInsets.zero;
                //cell.imageView_menuIcon.image = UIImage(named: "brand")
                cell.imageView_menuIcon.image = UIImage(named: (menuImage[indexPath.row]))?.maskWithColor(color: Color.Blue)
                let myCustomSelectionColorView = UIView()
                myCustomSelectionColorView.backgroundColor = UIColor.clear
                cell.selectedBackgroundView = myCustomSelectionColorView
                return cell
                
      
            }
        }
        return UITableViewCell()
    }
    
    
    
}

extension UIImage {
    
    func maskWithColor(color: UIColor) -> UIImage? {
        
        let maskLayer = CALayer()
        maskLayer.bounds = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        maskLayer.backgroundColor = color.cgColor
        maskLayer.doMask(by: self)
        let maskImage = maskLayer.toImage()
        return maskImage
    }
    
}


extension CALayer {
    func doMask(by imageMask: UIImage) {
        let maskLayer = CAShapeLayer()
        maskLayer.bounds = CGRect(x: 0, y: 0, width: imageMask.size.width, height: imageMask.size.height)
        bounds = maskLayer.bounds
        maskLayer.contents = imageMask.cgImage
        maskLayer.frame = CGRect(x: 0, y: 0, width: frame.size.width, height: frame.size.height)
        mask = maskLayer
    }
    
    func toImage() -> UIImage?
    {
        UIGraphicsBeginImageContextWithOptions(bounds.size,
                                               isOpaque,
                                               UIScreen.main.scale)
        guard let context = UIGraphicsGetCurrentContext() else {
            UIGraphicsEndImageContext()
            return nil
        }
        render(in: context)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
}
