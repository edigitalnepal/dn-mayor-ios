//
//  LeaveApplicationVC.swift
//  DNFounder
//
//  Created by Er. Aditya Raj on 7/5/18.
//  Copyright © 2018 Digital Nepal. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import DZNEmptyDataSet

class LeaveApplicationVC: UIViewController {
    @IBOutlet weak var tableview_Leave: UITableView!
  
    let cellIdentifier = "LeaveCell"
    var responseData :[JSON] = [JSON]()
    var totolcount = 0
    var pageNumber = 0
    var type = Int()
    var refresh:Bool = false
    var refreshControl = UIRefreshControl()
    let notification=Notification.Name("gotoLeave")

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavigationBarItem()
   
            userDefaults.set("2076", forKey: "yearLeave")
    
            userDefaults.set("1", forKey: "monthLeave")
        userDefaults.set("1", forKey: "monthLeavetitle")
  
        
      
        print("_________________" + String(self.type))
        self.refreshControl.attributedTitle = NSAttributedString(string: "Refreshing")
        refreshControl.addTarget(self, action: #selector(refresh(sender:)), for: .valueChanged)
        self.tableview_Leave?.addSubview(refreshControl)
        
        let button1 = UIBarButtonItem(image: UIImage(named: "fliter"), style: .plain, target: self, action: #selector(self.openFilter)) // action:#selector(Class.MethodName) for swift 3
         self.navigationItem.rightBarButtonItem  = button1
        
        nc.addObserver(self, selector: #selector(openInventory), name: Notification.Name("gotoLeave"), object: nil)
        
        //tabel view setup
        tableview_Leave.delegate = self
        tableview_Leave.dataSource = self
        tableview_Leave.register(UINib(nibName: "LeaveCell", bundle: Bundle.main), forCellReuseIdentifier: cellIdentifier)
        tableview_Leave.estimatedRowHeight = 60.0
        tableview_Leave.rowHeight = UITableViewAutomaticDimension
           loadFromAPI(year: userDefaults.string(forKey: "yearLeave")!, month: userDefaults.string(forKey: "monthLeave")!)
        

        // Do any additional setup after loading the view.
    }

  
    @objc func openFilter(){
        
        
        let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LeaveFilterVC") as! LeaveFilterVC
        popOverVC.modalPresentationStyle = .overFullScreen
        popOverVC.modalTransitionStyle = .crossDissolve
        self.present(popOverVC, animated: true, completion: nil)
        
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        title = "Leave Application"
    }
    @objc func refresh(sender:AnyObject) {
        refresh=true
        responseData.removeAll()
       loadFromAPI(year: userDefaults.string(forKey: "yearLeave")!, month: userDefaults.string(forKey: "monthLeave")!)
        
    }
    func loadFromAPI(year:String,month:String){
        if refresh {
            
        }else{
            showHud("Loading...")
        }
        var paramater = [String:AnyObject]()
        let id = userDefaults.integer(forKey:  "teamId")
        
        paramater = ["team_id":id as AnyObject,"year":year as AnyObject,"month":month as AnyObject]
        
        print(paramater)
        Alamofire.request(LibraryAPI.leaveurl, method: .post, parameters: paramater)
            
            
            .responseJSON(completionHandler: { (response) -> Void in
                print(response)
                if(response.result.isSuccess) {
                    
                    let json = JSON(response.data)
                    self.responseData = json["applications"].arrayValue
                    self.tableview_Leave.reloadData()
                    
                    
                    
                    self.hideHUD()
                } else{
                    self.hideHUD()
                    LibraryToast.notifyUser("ALERT", message:"Something Went Wrong Please Try Again" , vc: self)
                    
                    
                }
                self.refreshControl.endRefreshing()
                
                
            })
        self.tableview_Leave.emptyDataSetSource = self
        self.tableview_Leave.emptyDataSetDelegate = self
        refresh = false
        
        
    }
    @objc func openInventory(){
          self.title = "Leave Application"
        loadFromAPI(year: userDefaults.string(forKey: "yearLeave")!, month: userDefaults.string(forKey: "monthLeave")!)
    
        
    }
   
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension LeaveApplicationVC :UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return responseData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? LeaveCell
        cell?.selectionStyle = UITableViewCellSelectionStyle.none
        
        let status = responseData[indexPath.row]["status"] .stringValue
        if status == "Approved"{
            cell?.status.textColor = Color.total_present
            cell?.status.text = "APPROVED"
            
        }else if status == "Rejected"{
            cell?.status.textColor = Color.total_absent_student
            cell?.status.text = "REJECTED"
        }else{
            cell?.status.textColor = Color.disscount
            cell?.status.text = "PENDING"
        }
        cell?.requested_By.text = responseData[indexPath.row]["requested_by"] .stringValue
        
        
        return cell!
    }
    
    
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LeaveAcceptRejectVC") as! LeaveAcceptRejectVC
        popOverVC.data = responseData[indexPath.row]
        popOverVC.modalPresentationStyle = .overFullScreen
        popOverVC.modalTransitionStyle = .crossDissolve
        self.present(popOverVC, animated: true, completion: nil)
        
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
    }
    
}
extension LeaveApplicationVC : DZNEmptyDataSetDelegate, DZNEmptyDataSetSource {
    
    
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let str = ""
        let attrs = [NSAttributedStringKey.font: UIFont.preferredFont(forTextStyle: UIFontTextStyle.headline)]
        return NSAttributedString(string: str, attributes: attrs)
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let str = "No Data Available"
        let attrs = [NSAttributedStringKey.font: UIFont.preferredFont(forTextStyle: UIFontTextStyle.body)]
        return NSAttributedString(string: str, attributes: attrs)
    }
    
    
    func buttonTitle(forEmptyDataSet scrollView: UIScrollView!, for state: UIControlState) -> NSAttributedString! {
        let str = "RELOAD"
        let attrs = [NSAttributedStringKey.font: UIFont.preferredFont(forTextStyle: UIFontTextStyle.body)]
        return NSAttributedString(string: str, attributes: attrs)
    }
    
    func emptyDataSetDidTapButton(_ scrollView: UIScrollView!) {
        refresh = false
        responseData.removeAll()
        loadFromAPI(year: userDefaults.string(forKey: "yearLeave")!, month: userDefaults.string(forKey: "monthLeave")!)

        
    }
}
