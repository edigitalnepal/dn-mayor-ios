//
//  TransactionLogMainVC.swift
//  DNMayor
//
//  Created by mac on 8/19/19.
//  Copyright © 2019 E-Digital Nepal. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class TransactionLogMainVC: UIViewController {
    var fdate = ""
    var tdate = ""
    var filterText = ""
    var operationDateSetting = ""
    var salesAmt:Decimal = 0
    var purchaseAmt:Decimal = 0
    var feepaymentAmt:Decimal = 0
    var expenditureAmt:Decimal = 0
    var bankTransactionAmt:Decimal = 0
    var staffpayrollAmt:Decimal = 0
    
    var sales :[JSON] = [JSON]()
    var expenditure :[JSON] = [JSON]()
    var bankTransactions:[JSON] = [JSON]()
    var purchase :[JSON] = [JSON]()
    var staffPayroll :[JSON] = [JSON]()
    var feePayment:[JSON] = [JSON]()
    
    @IBOutlet weak var l_GrandTotal: UITextView!
    @IBOutlet weak var view_FeePayment: UIView!
    @IBOutlet weak var view_Sales: UIView!
    @IBOutlet weak var view_BankTransactions: UIView!
    @IBOutlet weak var view_Purchase: UIView!
    @IBOutlet weak var view_StaffPayroll: UIView!
    @IBOutlet weak var view_Expenditure: UIView!
    
    @IBOutlet weak var l_Date: UILabel!
    
    @IBOutlet weak var l_Expenditure: UILabel!
    @IBOutlet weak var l_Purchase: UILabel!
    @IBOutlet weak var l_Sales: UILabel!
    @IBOutlet weak var l_FeePayment: UILabel!
    @IBOutlet weak var l_StaffPayroll: UILabel!
    @IBOutlet weak var l_BankTransactions: UILabel!
    
    @IBOutlet weak var l_StaffPayroolTotal: UITextView!
    @IBOutlet weak var l_PurchaseTotal: UITextView!
    @IBOutlet weak var l_BankTransactionsTotal: UITextView!
    @IBOutlet weak var l_ExpenditureTotal: UITextView!
    @IBOutlet weak var l_SalesTotal: UITextView!
    @IBOutlet weak var l_FeePaymentTotal: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavigationBarItem()
        self.title = "Transactions Log"
        l_Date.text = "\(fdate) To \(tdate)"
        loadTransactionsLog()
        //click expenditure
        let exp = UITapGestureRecognizer(target: self, action: #selector(TransactionLogMainVC.expenditureTap(gesture:)))
        view_Expenditure.addGestureRecognizer(exp)
        //banktransaction
        let bnkt = UITapGestureRecognizer(target: self, action: #selector(TransactionLogMainVC.bankTap(gesture:)))
        view_BankTransactions
            .addGestureRecognizer(bnkt)
        //purchase
        let purchase = UITapGestureRecognizer(target: self, action: #selector(TransactionLogMainVC.purchaseTap(gesture:)))
        view_Purchase
            .addGestureRecognizer(purchase)
        
        //sales
        let sales = UITapGestureRecognizer(target: self, action: #selector(TransactionLogMainVC.salesTap(gesture:)))
        view_Sales
            .addGestureRecognizer(sales)
        
        //staffpayrooll
        let staffpayrooll = UITapGestureRecognizer(target: self, action: #selector(TransactionLogMainVC.staffpayroolTap(gesture:)))
        view_StaffPayroll
            .addGestureRecognizer(staffpayrooll)
        
        //feepayment
        let feepayment = UITapGestureRecognizer(target: self, action: #selector(TransactionLogMainVC.feepaymentTap(gesture:)))
        view_FeePayment
            .addGestureRecognizer(feepayment)
        
        let button1 = UIBarButtonItem(image: UIImage(named: "fliter"), style: .plain, target: self, action: #selector(self.openFilter)) // action:#selector(Class.MethodName) for swift 3
        self.navigationItem.rightBarButtonItem  = button1
    }
    
    @objc func expenditureTap(gesture: UIGestureRecognizer) {
        if self.expenditureAmt != 0{
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let tl = storyboard.instantiateViewController(withIdentifier: "TransactionLogDetailsVC") as! TransactionLogDetailsVC
            tl.responseData = self.expenditure
            tl.total = self.expenditureAmt
            tl.type = 1
            tl.titletext = "Expenditure"
            self.navigationController?.pushViewController(tl, animated: true)
            
        }else{
            LibraryToast.notifyUser("ALERT", message:"Not Available!" , vc: self)
        }
    }
    @objc func feepaymentTap(gesture: UIGestureRecognizer) {
        if self.feepaymentAmt != 0{
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let tl = storyboard.instantiateViewController(withIdentifier: "TransactionLogDetailsVC") as! TransactionLogDetailsVC
            tl.responseData = self.feePayment
            tl.total = self.feepaymentAmt
            tl.type = 3
            tl.titletext = "Fee Payment"
            self.navigationController?.pushViewController(tl, animated: true)
            
        }else{
            LibraryToast.notifyUser("ALERT", message:"Not Available!" , vc: self)
        }
    }
    @objc func staffpayroolTap(gesture: UIGestureRecognizer) {
        if self.staffpayrollAmt != 0{
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let tl = storyboard.instantiateViewController(withIdentifier: "TransactionLogDetailsVC") as! TransactionLogDetailsVC
            tl.responseData = self.staffPayroll
            tl.total = self.staffpayrollAmt
            tl.type = 2
            tl.titletext = "Staff Payroll"
            self.navigationController?.pushViewController(tl, animated: true)
            
        }else{
            LibraryToast.notifyUser("ALERT", message:"Not Available!" , vc: self)
        }
    }
    @objc func purchaseTap(gesture: UIGestureRecognizer) {
        if self.purchaseAmt != 0{
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let tl = storyboard.instantiateViewController(withIdentifier: "TransactionLogDetailsVC") as! TransactionLogDetailsVC
            tl.responseData = self.purchase
            tl.total = self.purchaseAmt
            tl.type = 1
            tl.titletext = "Purchase"
            self.navigationController?.pushViewController(tl, animated: true)
            
        }else{
            LibraryToast.notifyUser("ALERT", message:"Not Available!" , vc: self)
        }
    }
    @objc func salesTap(gesture: UIGestureRecognizer) {
        if self.salesAmt != 0{
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let tl = storyboard.instantiateViewController(withIdentifier: "TransactionLogDetailsVC") as! TransactionLogDetailsVC
            tl.responseData = self.sales
            tl.total = self.salesAmt
            tl.type = 1
            tl.titletext = "Sales"
            self.navigationController?.pushViewController(tl, animated: true)
            
        }else{
            LibraryToast.notifyUser("ALERT", message:"Not Available!" , vc: self)
        }
    }
    
    
    @objc func bankTap(gesture: UIGestureRecognizer) {
        
        if self.bankTransactionAmt != 0{
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let dailyBook = storyboard.instantiateViewController(withIdentifier: "DayBookVC") as! DayBookVC
            dailyBook.responseData = self.bankTransactions
            dailyBook.total = self.bankTransactionAmt
            dailyBook.type = 3
            dailyBook.titleText = "Bank Transactions"
            self.navigationController?.pushViewController(dailyBook, animated: true)
            
        }else{
            LibraryToast.notifyUser("ALERT", message:"Not Available!" , vc: self)
        }
        
        
    }
    
    
    @objc func openFilter(){
        _ = navigationController?.popViewController(animated: true)
        
    }
    func setDataInVew(){
        l_Expenditure.text = "Rs. \(expenditureAmt)"
        l_Sales.text = "Rs. \(salesAmt)"
        l_BankTransactions.text = "Rs. \(bankTransactionAmt)"
        l_Purchase.text = "Rs. \(purchaseAmt)"
        l_StaffPayroll.text = "Rs. \(staffpayrollAmt)"
        l_FeePayment.text = "Rs. \(feepaymentAmt)"
        
        l_ExpenditureTotal.text = "\(expenditureAmt)"
        l_SalesTotal.text = "\(salesAmt)"
        l_BankTransactionsTotal.text = "\(bankTransactionAmt)"
        l_PurchaseTotal.text = "\(purchaseAmt)"
        l_StaffPayroolTotal.text = "\(staffpayrollAmt)"
        l_FeePaymentTotal.text = "\(feepaymentAmt)"
        
        l_GrandTotal.text = "Rs. \(expenditureAmt+salesAmt+bankTransactionAmt+purchaseAmt+staffpayrollAmt+feepaymentAmt)"
        
    }
    
    
    func totalSales(){
        for i in sales{
            self.salesAmt = self.salesAmt+Decimal(string: i["totalAmount"].stringValue)!
        }
        
    }
    
    func totalExpenditure(){
        for i in self.expenditure{
            self.expenditureAmt = self.expenditureAmt+Decimal(string: i["totalAmount"].stringValue)!
            
        }
        
    }
    func totalPurchase(){
        for i in self.purchase{
            self.purchaseAmt = self.purchaseAmt+Decimal(string: i["totalAmount"].stringValue)!
            
        }
        
    }
    func totalFeePayment(){
        for i in self.feePayment{
            self.feepaymentAmt = self.feepaymentAmt+Decimal(string: i["totalAmount"].stringValue)!
        }
    }
    func totalStaffPayroll(){
        for i in self.staffPayroll{
            self.staffpayrollAmt = self.staffpayrollAmt+Decimal(string: i["totalAmount"].stringValue)!
        }
    }
    func bankTransactionTotal (){
        for i in self.bankTransactions{
            if i["txnNature"].stringValue == "Withdraw"{
                self.bankTransactionAmt = self.bankTransactionAmt+Decimal(string: i["paidAmount"].stringValue)!
            }else{
                self.bankTransactionAmt = self.bankTransactionAmt+Decimal(string: i["receivedAmount"].stringValue)!
            }
            
            
        }
        
    }
    
    
    
    func loadTransactionsLog(){
        showHud("Loading...")
        var paramater = [String:AnyObject]()
        let id=userDefaults.integer(forKey:"teamId")
        print(id)
        paramater = ["teamId":id as AnyObject,"fromDate":fdate as AnyObject,"toDate":tdate as AnyObject,"filter":filterText as AnyObject,"operationDateSetting":operationDateSetting as AnyObject]
        print(paramater)
        Alamofire.request(LibraryAPI.transactionsLog, method: .post, parameters: paramater)
            .responseJSON(completionHandler: { (response) -> Void in
                print(response)
                if(response.result.isSuccess) {
                    let j = JSON(response.data)
                    if j["success"].boolValue{
                        let body = j["body"]
                        self.expenditure = body["expenditure"].arrayValue
                        self.sales = body["sales"].arrayValue
                        self.bankTransactions = body["bankTransaction"].arrayValue
                        self.feePayment = body["feePayment"].arrayValue
                        self.purchase = body["purchase"].arrayValue
                        self.staffPayroll = body["staffPayroll"].arrayValue
                        
                        if self.expenditure.count != 0{
                            self.totalExpenditure()
                        }
                        if self.sales.count != 0{
                            self.totalSales()
                        }
                        if self.bankTransactions.count != 0{
                            self.bankTransactionTotal()
                        }
                        if self.staffPayroll.count != 0{
                            self.totalStaffPayroll()
                        }
                        if self.feePayment.count != 0{
                            self.totalFeePayment()
                        }
                        if self.purchase.count != 0{
                            self.totalPurchase()
                        }
                        self.setDataInVew()
                        
                    }else{
                        LibraryToast.notifyUser("ALERT", message:j["message"].stringValue , vc: self)
                    }
                    
                    
                    self.hideHUD()
                } else{
                    self.hideHUD()
                    let alert = UIAlertController(title: "ALERT!  ", message: "Check your internet connections and Try Again", preferredStyle: .alert)
                    
                    
                    alert.addAction(UIAlertAction(title: "Retry", style: .destructive, handler:{ action in
                        
                        
                    }))
                    
                    self.present(alert, animated: true)
                }
                
                
                
            })
        
        
    }
    
    
    
    
}
