//
//  TransactionLogDetailsVC.swift
//  DNMayor
//
//  Created by mac on 8/19/19.
//  Copyright © 2019 E-Digital Nepal. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON


class TransactionLogDetailsVC: UIViewController {
    var responseData :[JSON] = [JSON]()
    var titletext = ""
    var type = 0
    var total:Decimal = 0
    var filterText = ""
    
    @IBOutlet weak var tabelview: UITableView!
    @IBOutlet weak var l_seventh: UITextView!
    @IBOutlet weak var l_sixth: UITextView!
    @IBOutlet weak var l_fifth: UITextView!
    @IBOutlet weak var l_fourth: UITextView!
    @IBOutlet weak var l_third: UITextView!
    @IBOutlet weak var l_second: UITextView!
    @IBOutlet weak var l_first: UITextView!
    @IBOutlet weak var l_Total: UITextView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = titletext+"(Rs.\(total))"
        //        self.l_Total.text = "\(total)"
        
        print(responseData)
        
        tabelview.delegate = self
        tabelview.dataSource = self
        tabelview.estimatedRowHeight = 40.0
        tabelview.rowHeight = UITableViewAutomaticDimension
        tabelview.reloadData()
    }
    
    
    
    
}
extension TransactionLogDetailsVC :UITableViewDataSource, UITableViewDelegate{
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return responseData.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "TransactionsLogTableViewCell", for: indexPath) as? TransactionsLogTableViewCell
        cell?.selectionStyle = UITableViewCellSelectionStyle.none
        let i = responseData[indexPath.row]
        if type == 1{
            var date:String!
            if i["createdDate"].stringValue != ""{
                date = i["createdDate"].stringValue
            }else{
                date =  i["entryDate"].stringValue
            }
            let datetobeconverted = date.components(separatedBy: " ")
            cell?.l_first.text = DateInString.stringDate(date: datetobeconverted[0])
            cell?.l_second.text = i["itemName"].stringValue
            cell?.l_third.text = i["quantity"].stringValue
            cell?.l_fourth.text = i["rate"].stringValue
            cell?.l_fifth.text = i["tax"].stringValue
            cell?.l_sixth.text = i["discount"].stringValue
            cell?.l_seventh.text = i["totalAmount"].stringValue
        }else if type == 2{
            l_first.text = "Date"
            l_second.text = "Staff Name"
            l_third.text = "Department"
            l_fourth.text = "Designation"
            l_fifth.text = "Total(Rs)"
            l_sixth.text = "Paid(Rs)"
            l_seventh.text = "Due(Rs)"
            let date = i["paidDate"].stringValue
            
            cell?.l_first.text = date
            cell?.l_second.text = i["staffName"].stringValue
            cell?.l_third.text = i["department"].stringValue
            cell?.l_fourth.text = i["designation"].stringValue
            cell?.l_fifth.text = i["totalAmount"].stringValue
            cell?.l_sixth.text = i["paidAmount"].stringValue
            cell?.l_seventh.text = i["dueAmount"].stringValue
            
        }
        else if type == 3{
            l_first.text = "Receipt No."
            l_second.text = "Received From"
            l_third.text = "Received Date"
            l_fourth.text = "Received By"
            l_fifth.text = "Total(Rs)"
            l_sixth.text = "Received(Rs)"
            l_seventh.text = "Due(Rs)"
            let date = i["receivedDate"].stringValue
            let datetobeconverted = date.components(separatedBy: " ")
            cell?.l_first.text = i["receiptNumber"].stringValue
            cell?.l_second.text = i["studentName"].stringValue
            cell?.l_third.text = DateInString.stringDate(date: datetobeconverted[0])
            cell?.l_fourth.text = i["receivedBy"].stringValue
            cell?.l_fifth.text = i["totalAmount"].stringValue
            cell?.l_sixth.text = i["receivedAmount"].stringValue
            cell?.l_seventh.text = i["dueAmount"].stringValue
            
        }
        
        
        return cell!
        
        
    }
    
}







