//
//  TransactionLogFilterVC.swift
//  DNMayor
//
//  Created by mac on 8/19/19.
//  Copyright © 2019 E-Digital Nepal. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class TransactionLogFilterVC: UIViewController {
    @IBOutlet weak var tf_fromdate: UITextField!
    @IBOutlet weak var two_date: UITextField!
    
    @IBOutlet weak var tb_Filter: UITableView!
    let datePicker = UIDatePicker()
    let datePickertwo = UIDatePicker()
    var dateType = ""
    var chooseDateStatus = 1
    var checkstatus = [true,false,false,false,false,false,false]
    let selectedimgae: UIImage = UIImage(named: "check")!
    let unselectedimgae: UIImage = UIImage(named: "uncheck")!
    
    let cellIdentifier = "CheckBoxCell"
    
    let filterData = ["All","BankTransactions","StaffPayroll","AcademicFee","Sales","Purchase","Expenditure"]
    let imagedata = ["all","transaction","staffpayroll","account","sales","purchase","inventory"]
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavigationBarItem()
        self.title = "Transactions Log Filter"
        two_date.text = DateInString.getCurrentDateNep()
        tf_fromdate.text = DateInString.oneMonthLate()
        
        //click on textfield
        tf_fromdate.addTarget(self, action: #selector(myTargetFunction), for: .touchDown)
        two_date.addTarget(self, action: #selector(twodate), for: .touchDown)
        
        NotificationCenter.default.addObserver(self, selector: (#selector(self.FromDate(_:))), name: NSNotification.Name(rawValue: "tldate"), object: nil)
        
        tb_Filter.delegate = self
        tb_Filter.dataSource = self
        tb_Filter.estimatedRowHeight = 30.0
        tb_Filter.rowHeight = 45.0
        tb_Filter.register(UINib(nibName: cellIdentifier, bundle: Bundle.main), forCellReuseIdentifier: cellIdentifier)
        tb_Filter.reloadData()
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        checkEnglishNeplai()
    }
    
    // MARK: - Check to Show english or nepali date picker
    func checkEnglishNeplai(){
        showHud("Loading...")
        var paramater = [String:AnyObject]()
        let id = userDefaults.integer(forKey:  "teamId")
        paramater = ["teamId":id as AnyObject]
        Alamofire.request(LibraryAPI.datetype, method: .post, parameters: paramater)
            .responseJSON(completionHandler: { (response) -> Void in
                print(response)
                if(response.result.isSuccess) {
                    let json = JSON(response.data)
                    let datas = json["body"]
                    if json["success"].boolValue{
                        self.dateType = datas["operationDateSetting"].stringValue
                    }else{
                        LibraryToast.notifyUser("ALERT", message:json["message"].stringValue, vc: self)
                        
                    }
                    
                    self.hideHUD()
                } else{
                    self.hideHUD()
                    let alert = UIAlertController(title: "ALERT!  ", message: "Check your internet connections and Try Again", preferredStyle: .alert)
                    
                    
                    alert.addAction(UIAlertAction(title: "Retry", style: .destructive, handler:{ action in
                        
                        self.checkEnglishNeplai()
                    }))
                    
                    self.present(alert, animated: true)
                    
                    
                }
                
                
            })
        
    }
    
    @objc func myTargetFunction(textField: UITextField) {
        chooseDateStatus = 2
        if dateType == "en"{
            showDatePicker()
            
        }else if dateType == "np"{
            let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ChooseDateVC") as! ChooseDateVC
            
            popOverVC.modalPresentationStyle = .overFullScreen
            popOverVC.modalTransitionStyle = .crossDissolve
            popOverVC.status = 2
            popOverVC.callfrom = 2
            self.present(popOverVC, animated: true, completion: nil)
            
        }
        
    }
    @IBAction func clickVIewReport(_ sender: Any) {
        if tf_fromdate.text! != "" && two_date.text! != "" && self.checkstatus.contains(true){
            if tf_fromdate.text!.length > 10 || two_date.text!.length > 10 {
                LibraryToast.notifyUser("ALERT", message:"Select all above properly" , vc: self)
            }else{
                var array:[String] = [String]()
                
                for i in 0..<self.checkstatus.count{
                    if self.checkstatus[i]{
                        array.append(filterData[i])
                    }
                }
                let stringRepresentation = array.joined(separator: ",")
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let tl = storyboard.instantiateViewController(withIdentifier: "TransactionLogMainVC") as! TransactionLogMainVC
                tl.fdate = tf_fromdate.text!
                tl.tdate = two_date.text!
                tl.filterText = stringRepresentation
                tl.operationDateSetting = dateType
                self.navigationController?.pushViewController(tl, animated: true)
            }
        }else{
            LibraryToast.notifyUser("ALERT", message:"Select all above properly" , vc: self)
        }
        
    }
    
    @objc func FromDate(_ notification: NSNotification) {
        print(notification.userInfo!["date"] as! String)
        guard let name = notification.userInfo!["date"] else {
            
            return
        }
        if chooseDateStatus == 1{
            let date = name as! String
            if self.tf_fromdate.text! != ""{
                if compareDate(fromDate: tf_fromdate.text!, toDate: date){
                    self.two_date.text! = "To date must be greate then From date"
                    
                }else{
                    self.two_date.text = date
                    
                }
            }else{
                self.two_date.text = date
                
            }
            
        }else if chooseDateStatus == 2{
            let date = name as! String
            if self.two_date.text! != ""{
                if compareDate(fromDate:date, toDate: two_date.text!){
                    tf_fromdate.text = "From date must be less then To date"
                    //                    LibraryToast.notifyUser("ALERT", message:"From date must be less then To date", vc: self)
                }else{
                    self.tf_fromdate.text = date
                    
                }
            }else{
                self.tf_fromdate.text = date
                
            }
            
        }
        
        
    }
    @objc func twodate(textField: UITextField) {
        chooseDateStatus = 1
        if dateType == "en"{
            showDatePickertwo()
        }else if dateType == "np"{
            let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ChooseDateVC") as! ChooseDateVC
            
            popOverVC.modalPresentationStyle = .overFullScreen
            popOverVC.modalTransitionStyle = .crossDissolve
            popOverVC.status = 1
            popOverVC.callfrom = 2
            self.present(popOverVC, animated: true, completion: nil)
            
        }
        
        
    }
    func showDatePicker(){
        //Formate Date
        datePicker.datePickerMode = .date
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        tf_fromdate.inputAccessoryView = toolbar
        tf_fromdate.inputView = datePicker
        
        
    }
    func showDatePickertwo(){
        //Formate Date
        datePickertwo.datePickerMode = .date
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePickerTwo));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        two_date.inputAccessoryView = toolbar
        two_date.inputView = datePickertwo
        
        
    }
    
    func compareDate(fromDate:String!,toDate:String!) ->Bool{
        let fdate = fromDate.split(separator: "-")
        let tdate = toDate.split(separator: "-")
        let fyear:Int = Int(fdate[0])!
        let tyear:Int = Int(tdate[0])!
        let fmonth:Int = Int(fdate[1])!
        let tmonth:Int = Int(tdate[1])!
        let fday:Int = Int(fdate[2])!
        let tday:Int = Int(tdate[2])!
        print(fyear)
        print(tyear)
        if fyear > tyear {
            return true
        }else{
            if fyear == tyear && fmonth > tmonth{
                return true
            }else{
                if fmonth == tmonth && fday > tday{
                    return true
                }else{
                    return false
                }
            }
        }
        
        
        
    }
    
    @objc func donedatePicker(){
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let date = formatter.string(from: datePicker.date)
        self.view.endEditing(true)
        if self.two_date.text! != ""{
            if compareDate(fromDate:date, toDate: two_date.text!){
                tf_fromdate.text = "From date must be less then To date"
                //                    LibraryToast.notifyUser("ALERT", message:"From date must be less then To date", vc: self)
            }else{
                self.tf_fromdate.text = date
                
            }
        }else{
            self.tf_fromdate.text = date
            
        }
        
        
        
        
    }
    @objc func donedatePickerTwo(){
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let date = formatter.string(from: datePickertwo.date)
        
        if self.tf_fromdate.text! != ""{
            if compareDate(fromDate: tf_fromdate.text!, toDate: date){
                self.two_date.text! = "To date must be greate then From date"
                //       LibraryToast.notifyUser("ALERT", message:"To date must be greate then From date", vc: self)
            }else{
                self.two_date.text = date
                
            }
        }else{
            self.two_date.text = date
            
        }
        self.view.endEditing(true)
        
        
        
        
    }
    
    
    
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    
    
    
    
}
extension TransactionLogFilterVC :UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filterData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? CheckBoxCell
        cell?.selectionStyle = UITableViewCellSelectionStyle.none
        cell?.l_Value.text = filterData[indexPath.row]

        
        
        if checkstatus[indexPath.row]{
            cell?.check_btn.setImage(selectedimgae, for: .normal)
            cell?.s = true
        }else{
            cell?.s = false
            cell?.check_btn.setImage(unselectedimgae, for: .normal)
        }
        cell?.btn_yes = {
            if self.checkstatus[0] && indexPath.row > 0{
                self.checkstatus[indexPath.row] = true
                cell?.check_btn.setImage(self.selectedimgae, for: .normal)
                cell?.s = true
                self.checkstatus[0] = false
                let indexPath = IndexPath(row: 0, section: 0)
                self.tb_Filter.reloadRows(at: [indexPath], with: .top)
            }else if !self.checkstatus[0] && indexPath.row == 0{
                self.checkstatus[indexPath.row] = true
                self.tb_Filter.reloadRows(at: [indexPath], with: .top)
                //                cell?.check_btn.setImage(self.selectedimgae, for: .normal)
                cell?.s = true
                for i in 0..<self.checkstatus.count{
                    if i != 0 {
                        self.checkstatus[i] = false
                        let indexPath = IndexPath(row: i, section: 0)
                        self.tb_Filter.reloadRows(at: [indexPath], with: .top)
                        
                        
                    }
                }
                
            }else{
                self.checkstatus[indexPath.row] = true
                self.tb_Filter.reloadRows(at: [indexPath], with: .top)
                //                cell?.check_btn.setImage(self.selectedimgae, for: .normal)
                cell?.s = true
            }
            
            print(self.checkstatus)
        }
        cell?.btn_no = {
            self.checkstatus[indexPath.row] = false
            self.tb_Filter.reloadRows(at: [indexPath], with: .top)
            cell?.s = false
            print(self.checkstatus)
        }
        
        cell?.layoutIfNeeded()
        return cell!
    }
    
    
    
    
    
    
    
}
