//
//  InventoryFilterPopupViewController.swift
//  DNFounder
//
//  Created by Er. Aditya Raj on 5/16/18.
//  Copyright © 2018 Digital Nepal. All rights reserved.
//

import UIKit
import  Spring
import Alamofire
import  SwiftyJSON
import DropDown
let nc = NotificationCenter.default
class InventoryFilterPopupViewController: UIViewController {
    
    @IBOutlet var ok_btn: UIButton!
    @IBOutlet weak var btn_grade: DesignableButton!
    @IBOutlet weak var btn_type: DesignableButton!
    @IBOutlet weak var btn_Month: DesignableButton!
    @IBOutlet weak var btn_Year: DesignableButton!
    @IBOutlet weak var cancel_btn: UIButton!
    @IBOutlet var mainview: UIView!
    let notification = "gotoaccount"
    
    let dropDownYear = DropDown()
    let dropDownMonth = DropDown()
    let dropDownType = DropDown()
    let dropDownGrade = DropDown()
    var responseDatagrade :[JSON] = [JSON]()
    
    @IBOutlet weak var btn_cancel: UIButton!
    let year = ["2076","2075", "2074", "2073"]
    let month=["All","Baishak", "Jestha", "Ashad","Shrawan", "Bhadra", "Ashoj","Kartik", "Mangsir", "Poush","Magh", "Falgun", "Chaitra"]
 
    let type = ["All", "Admission Fee", "University / Exam Fee","Sports Fee", "Book / Library Fee", "Uniform Fee","Extra Curricular Activity Fee", "Registration Fee", "Annual Fee","Computer Fee", "Security Deposit", "Lab Fee"]
    var grade:[String] = []
    var gradeid:[Int] = []

   
    override func viewDidLoad() {
        super.viewDidLoad()
        btn_cancel.backgroundColor = Color.Blue
        mainview.backgroundColor = UIColor.clear
        btn_cancel.contentEdgeInsets = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 0)
        btn_cancel.titleEdgeInsets=UIEdgeInsetsMake(0, 20, 0, 10);
        
        //drop down
        dropDownYear.anchorView = btn_Year
        dropDownMonth.anchorView = btn_Month
        dropDownType.anchorView = btn_type
        dropDownGrade.anchorView = btn_grade
        
       
       makeUpButton()
       

        
        
        dropDownYear.dataSource = year
        dropDownMonth.dataSource = month
        dropDownType.dataSource = type
        LoadMainOrg()
        dropDownYear.selectionAction = { [unowned self] (index: Int, item: String) in
            self.btn_Year.setTitle(item, for: .normal)
            userDefaults.set(item, forKey: "year")
            
            
        }
        dropDownMonth.selectionAction = { [unowned self] (index: Int, item: String) in
            self.btn_Month.setTitle(item, for: .normal)
            if index != 0 {
                userDefaults.set(String(index), forKey: "month")
                userDefaults.set(item, forKey: "monthv")
                self.btn_type.setTitle("All", for: .normal)
                userDefaults.set("All", forKey: "type")
                
            }else{
                userDefaults.set("All", forKey: "month")
                userDefaults.set("All", forKey: "monthv")
            }
            
            
            
        }
        dropDownType.selectionAction = { [unowned self] (index: Int, item: String) in
            self.btn_type.setTitle(item, for: .normal)
            if item != "All"{
                userDefaults.set("All", forKey: "month")
                userDefaults.set("All", forKey: "monthv")
                 userDefaults.set(item, forKey: "type")
                self.btn_Month.setTitle("All", for: .normal)
            }else{
                userDefaults.set(item, forKey: "type")
            }
           
           
            
            
        }
        dropDownGrade.selectionAction = { [unowned self] (index: Int, item: String) in
            self.btn_grade.setTitle(item, for: .normal)
            if index != 0{
                userDefaults.set(String(self.gradeid[index]), forKey: "grade")
                userDefaults.set(item, forKey: "gradev")
            }else{
                userDefaults.set("All", forKey: "grade")
                userDefaults.set(item, forKey: "gradev")
            }
          
           
            
            
        }
        
        
        
    
        
        

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if userDefaults.string(forKey: "month") != "All"{
           btn_Month.setTitle(userDefaults.string(forKey: "monthv"), for: .normal)
        }else{
            btn_Month.setTitle("All", for: .normal)
        }
        
        if userDefaults.string(forKey: "year") != "2075"{
            btn_Year.setTitle(userDefaults.string(forKey: "year"), for: .normal)
        }else{
            btn_Year.setTitle("2075", for: .normal)
        }
        
        if userDefaults.string(forKey: "type") != "All"{
            btn_type.setTitle(userDefaults.string(forKey: "type"), for: .normal)
        }else{
            btn_type.setTitle("All", for: .normal)
        }
        if userDefaults.string(forKey: "grade") != "All"{
           btn_grade.setTitle(userDefaults.string(forKey: "gradev"), for: .normal)
        }else{
            btn_grade.setTitle("All", for: .normal)
        }
        
       
        }
    
    func makeUpButton(){
        ok_btn.layer.backgroundColor = UIColor.white.cgColor
        ok_btn.layer.shadowColor = UIColor.gray.cgColor
        ok_btn.layer.borderColor = Color.Blue.cgColor
        ok_btn.layer.cornerRadius = 5
        ok_btn.layer.borderWidth = 2
        
    }
  
    @IBAction func cancelbtn(_ sender: Any) {
         dismiss(animated: true, completion: nil)
    }
    func LoadMainOrg(){
        print("inside loadleaveType")
        showHud("Loading...")
        var paramater = [String:AnyObject]()
        let orgID=userDefaults.integer(forKey:  "teamId")
        paramater = ["team_id":orgID as AnyObject]
        print(paramater)
        Alamofire.request(LibraryAPI.findAllgrades, method: .post, parameters: paramater)
            
            
            .responseJSON(completionHandler: { (response) -> Void in
                print(response)
                if(response.result.isSuccess) {
                    let json = JSON(response.data)
                    self.responseDatagrade = json.arrayValue
                    self.loadString()
                  
                    self.hideHUD()
                } else{
                    self.hideHUD()
                      LibraryToast.notifyUser("ALERT", message:"Something Went Wrong Please Try Again" , vc: self)
                    
                }
                
            })
        
    }
    func loadString(){
        grade.removeAll()
        gradeid.removeAll()
        grade.append("ALL")
        gradeid.append(0)
        for index in 0..<responseDatagrade.count{
            grade.append(responseDatagrade[index]["name"].stringValue)
            gradeid.append(responseDatagrade[index]["id"].intValue)
            print(grade)
        }
        print("before")
        dropDownGrade.dataSource = grade
        
    }
    
    
    @IBAction func btn_okay(_ sender: Any) {

         nc.post(name: Notification.Name("gotoAccount"), object: nil)
        dismiss(animated: true, completion: nil)
    }
    


    
    // Sets number of columns in picker view
  
    @IBAction func grade(_ sender: Any) {
        dropDownGrade.show()
    }
    @IBAction func month(_ sender: Any) {
        dropDownMonth.show()
    }
    
    @IBAction func type(_ sender: Any) {
        dropDownType.show()
    }
    @IBAction func year(_ sender: Any) {
            dropDownYear.show()
    }
  


}
