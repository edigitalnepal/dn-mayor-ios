//
//  NoticeFullViewPopup.swift
//  DNFounder
//
//  Created by Er. Aditya Raj on 5/15/18.
//  Copyright © 2018 Digital Nepal. All rights reserved.
//

import UIKit

class NoticeFullViewPopup: UIViewController,UIWebViewDelegate {
    var date = String()
    var heading = String()
    var details = String()
    
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet var mainview: UIView!
  
    @IBOutlet weak var label_date: UILabel!
    
    @IBOutlet weak var btn_cancel: UIButton!
    @IBOutlet weak var label_heading: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        webView.delegate = self
        self.showHud("Loading...")
        title = DateInString.stringDate(date: date)
        label_heading.text = heading
        label_date.text = ""
        webView.loadHTMLString(details, baseURL: nil)
        view.layoutIfNeeded()
       

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func btn_Cancel(_ sender: Any) {
        self.view.removeFromSuperview()
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
        
        self.webView.scalesPageToFit = true
        self.webView.contentMode = UIViewContentMode.scaleAspectFit
        self.hideHUD()
    }


}
extension Data {
    var html2AttributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: self, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            print("error:", error)
            return  nil
        }
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
}

extension String {
    var html2AttributedString: NSAttributedString? {
        return Data(utf8).html2AttributedString
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
}

