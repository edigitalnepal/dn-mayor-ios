//
//  ExaminationVC.swift
//  DNFounder
//
//  Created by Er. Aditya Raj on 5/15/18.
//  Copyright © 2018 Digital Nepal. All rights reserved.
//

import UIKit
import  Alamofire
import SwiftyJSON

class ExaminationVC: UIViewController {
    let cellIdentifier = "ExaminationCellNew"
    var responseData :[JSON] = [JSON]()
    let values:[String] = ["a","b","c","d","e","f","g","h"]
    var point:[Double] = []

    @IBOutlet weak var tableview_Examination: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavigationBarItem()
        self.title = "Examination Report"
        
        //tabel view setup
        tableview_Examination.delegate = self
        tableview_Examination.dataSource = self
        tableview_Examination.register(UINib(nibName: "ExaminationCellNew", bundle: Bundle.main), forCellReuseIdentifier: cellIdentifier)
        tableview_Examination.estimatedRowHeight = 120.0
        tableview_Examination.rowHeight = 120.0
        

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        loadFromAPI(offSetNormal:0)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func loadFromAPI(offSetNormal:Int){
       
            showHud("Loading...")
        
        var paramater = [String:AnyObject]()
        let id = userDefaults.integer(forKey:  "teamId")
        
        paramater = ["year":"2076" as AnyObject,"team_id":id as AnyObject]
        
        print(paramater)
        Alamofire.request(LibraryAPI.examURl, method: .post, parameters: paramater)
            
            
            .responseJSON(completionHandler: { (response) -> Void in
                print(response)
                if(response.result.isSuccess) {
                    
                    let json = JSON(response.data)
                    self.responseData = json["exams"].arrayValue
                    print(self.responseData)
                    self.tableview_Examination.reloadData()
                    
                    
                    
                    self.hideHUD()
                } else{
                    self.hideHUD()
                    LibraryToast.notifyUser("ALERT", message:"Something Went Wrong Please Try Again" , vc: self)
                    
                    
                }
             
                
                
            })
   
        
        
    }
    

  

}
extension ExaminationVC :UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return responseData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? ExaminationCellNew
        cell?.selectionStyle = UITableViewCellSelectionStyle.none
        cell?.end_date.text = DateInString.stringDate(date: responseData[indexPath.row]["end_date"].stringValue)
         cell?.startDate.text = DateInString.stringDate(date: responseData[indexPath.row]["start_date"].stringValue)
        if responseData[indexPath.row]["result_published"].boolValue{
            cell?.result_publish.text = "Published"
            cell?.result_publish.textColor = Color.total_present
            
        }else{
            cell?.result_publish.text = "Unpublished"
            cell?.result_publish.textColor = Color.total_absent_student
        }
        
         cell?.exam_title.text = responseData[indexPath.row]["name"].stringValue
        return cell!
    }
    
    
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let gunasoDetailVC = storyboard.instantiateViewController(withIdentifier: "ExaminationFullView") as! ExaminationFullView
        gunasoDetailVC.data = responseData[indexPath.row]
        self.navigationController?.pushViewController(gunasoDetailVC, animated: true)
        
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 120.0
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 120.0
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
       
    }
    
}
