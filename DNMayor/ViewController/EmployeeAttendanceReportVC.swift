//
//  EmployeeAttendanceReportVC.swift
//  DNFounder
//
//  Created by Er. Aditya Raj on 7/6/18.
//  Copyright © 2018 Digital Nepal. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import DZNEmptyDataSet
import Charts


class EmployeeAttendanceReportVC: UIViewController {
    @IBOutlet weak var present: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var view_top: UIView!
    
    @IBOutlet weak var absent: UILabel!
    @IBOutlet weak var EmployeeChart: PieChartView!
    @IBOutlet weak var tableview_Employee: UITableView!
    var type = Int()
    let cellIdentifier = "EmployeeAttendanceCell"
    var responseData :[JSON] = [JSON]()
    var totolcount = 0
    var pageNumber = 0
    var refresh:Bool = false
    var refreshControl = UIRefreshControl()
    var uicolorArray = [UIColor]()
    
    var total_absent_student = PieChartDataEntry(value: 0)
    var total_present = PieChartDataEntry(value: 0)
     var numentries = [PieChartDataEntry]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        EmployeeChart.chartDescription?.text = ""
        EmployeeChart.entryLabelColor = UIColor.black
        total_present.label = "Present(%)"
        total_absent_student.label = "Absent(%)"
        absent.textColor = Color.total_absent_student
        present.textColor = Color.total_present
         EmployeeChart.usePercentValuesEnabled = true
        
        tableview_Employee.delegate = self
        tableview_Employee.dataSource = self
        tableview_Employee.register(UINib(nibName: "EmployeeAttendanceCell", bundle: Bundle.main), forCellReuseIdentifier: cellIdentifier)
        tableview_Employee.estimatedRowHeight = 60.0
        tableview_Employee.rowHeight = 60.0
        loadFromAPI(offSetNormal: 0)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func setPieChart(){
        numentries.removeAll()
        uicolorArray.removeAll()
        if total_present.value != 0{
            numentries.append(total_present)
            uicolorArray.append(Color.total_present)
        }
        if total_absent_student.value != 0{
            numentries.append(total_absent_student)
         uicolorArray.append(Color.total_absent_student)
        }
    
        let dataset = PieChartDataSet(values: numentries, label: nil)
        let pieobject = PieChartData(dataSet: dataset)
        dataset.colors = uicolorArray
        EmployeeChart.data = pieobject
        
    }
    

    @objc func refresh(sender:AnyObject) {
        
        loadFromAPI(offSetNormal:0)
        refresh=true
        responseData.removeAll()
        tableview_Employee.reloadData()
        
    }
   
    
    func loadFromAPI(offSetNormal:Int){
        if refresh {
            
        }else{
            showHud("Loading...")
        }
        var paramater = [String:AnyObject]()
        let id = userDefaults.integer(forKey:  "teamId")
        
        paramater = ["team_id":id as AnyObject]
        
        print(paramater)
        Alamofire.request(LibraryAPI.emoloyeeAtn, method: .post, parameters: paramater)
            
            
            .responseJSON(completionHandler: { (response) -> Void in
                print(response)
                if(response.result.isSuccess) {
                    
                    let json = JSON(response.data)
                    let datas = json["data"]
                    if json["success"].boolValue{
                        self.view_top.isHidden = false
                        }else{
                        self.view_top.isHidden = true
                    }
                     self.responseData = datas["employeeAttendances"].arrayValue
                    self.total_present.value = datas["total_present"].doubleValue
                    self.total_absent_student.value = datas["total_absent"].doubleValue
                    self.absent.text = String(datas["total_present"].intValue)
                    self.present.text = String(datas["total_absent"].intValue)
                    self.date.text = datas["attendance_date"].stringValue
                    self.setPieChart()
                    self.tableview_Employee.reloadData()
                    self.hideHUD()
                } else{
                    self.hideHUD()
                    LibraryToast.notifyUser("ALERT", message:"Something Went Wrong Please Try Again" , vc: self)
                    
                    
                }
                self.refreshControl.endRefreshing()
                
                
            })
        self.tableview_Employee.emptyDataSetSource = self
        self.tableview_Employee.emptyDataSetDelegate = self
        refresh = false
        
        
    }
    
    
    
    
    
    
}
extension EmployeeAttendanceReportVC :UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return responseData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? EmployeeAttendanceCell
        cell?.selectionStyle = UITableViewCellSelectionStyle.none

        cell?.name.text = responseData[indexPath.row]["staff"]["name"].stringValue
        if  responseData[indexPath.row]["status"] .boolValue{
            cell?.status.textColor = Color.pass
              cell?.status.text = "Present"
            
        }else{
            cell?.status.textColor = Color.failed
            cell?.status.text = "Absent"
        }
        
        return cell!
    }
    
    
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
    }
    
}
extension EmployeeAttendanceReportVC : DZNEmptyDataSetDelegate, DZNEmptyDataSetSource {
    
    
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let str = ""
        let attrs = [NSAttributedStringKey.font: UIFont.preferredFont(forTextStyle: UIFontTextStyle.headline)]
        return NSAttributedString(string: str, attributes: attrs)
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let str = "No Data Available"
        let attrs = [NSAttributedStringKey.font: UIFont.preferredFont(forTextStyle: UIFontTextStyle.body)]
        return NSAttributedString(string: str, attributes: attrs)
    }
    
    
    func buttonTitle(forEmptyDataSet scrollView: UIScrollView!, for state: UIControlState) -> NSAttributedString! {
        let str = "RELOAD"
        let attrs = [NSAttributedStringKey.font: UIFont.preferredFont(forTextStyle: UIFontTextStyle.body)]
        return NSAttributedString(string: str, attributes: attrs)
    }
    
    func emptyDataSetDidTapButton(_ scrollView: UIScrollView!) {
        refresh = false
        responseData.removeAll()
        loadFromAPI(offSetNormal:0)
        
    }
}
