//
//  DashboardViewController.swift
//  DNFounder
//
//  Created by digital nepal on 3/24/19.
//  Copyright © 2019 E-Digital Nepal. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Charts



class DashboardViewController: UIViewController {
    
    //ui components
    @IBOutlet var label_Guardians: UILabel!
    @IBOutlet var label_Students: UILabel!
    @IBOutlet var label_Staffs: UILabel!
    @IBOutlet var label_Programs: UILabel!
    @IBOutlet var label_Websites: UILabel!
    @IBOutlet var label_Email: UILabel!
    @IBOutlet var label_Mobile: UILabel!
    @IBOutlet var label_Phone: UILabel!
    @IBOutlet var label_Address: UILabel!
    @IBOutlet var label_CollegeName: UILabel!
    @IBOutlet var tableview_Dashboard: UITableView!
    @IBOutlet var piechart_Account: PieChartView!
    @IBOutlet var uicollectionview_TotalStudents: UICollectionView!
    
    @IBOutlet var barChart_StaffAttendance: BarChartView!
    @IBOutlet var barChart_StudentAttendance: BarChartView!
    @IBOutlet var uicollectionview_TotalStaffs: UICollectionView!
    var basicInfoData :[JSON] = [JSON]()
    var gradewiseStudentData:[JSON] = [JSON]()
    var departmentWiseStaffData:[JSON] = [JSON]()
     var studentAttendanceData:[JSON] = [JSON]()
    var staffAttendanceData:[JSON] = [JSON]()
    
    // for refresh
    var refresh:Bool = false
    var refreshControl = UIRefreshControl()
    
    //barchart entry

    let months = ["Baishak","Jestha","Ashad","Shrawan","Bhadra","Ashoj","Kartik","Mangsir","Poush","Magh","Falgun","Chaitra"]
    var studentMale = [Double]()
    var studentFemale = [Double]()
      var studentOther = [Double]()
    
    //account info piechart
    var purchase_amt = PieChartDataEntry()
    var expenditure_amt = PieChartDataEntry()
    var cash_collection = PieChartDataEntry()
    var due_amt = PieChartDataEntry()
    var pieentires = [PieChartDataEntry]()
    var uiColorArray = [UIColor]()
    //for total students
    var male = PieChartDataEntry()
    var female = PieChartDataEntry()
    var other = PieChartDataEntry()
    var pieentiresTotalStudents = [PieChartDataEntry]()
    var uiColorArrayTotalStudents = [UIColor]()
    let maleColor = [UIColor.red,Color.male]
     let femaleColor = [UIColor.red,Color.female]
     let otherColor = [UIColor.red,Color.other]
    
    //total student
    private let reuseIdentifierTotalStudent = "TotalStudentsCollectionViewCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Dashboard"
        loadBasicInfo()
    
        //refresh setup
        self.refreshControl.attributedTitle = NSAttributedString(string: "Refreshing")
        refreshControl.addTarget(self, action: #selector(refresh(sender:)), for: .valueChanged)
        self.tableview_Dashboard?.addSubview(refreshControl)
        
        //setup for collection view
        uicollectionview_TotalStudents.delegate = self
        uicollectionview_TotalStudents.dataSource = self
        uicollectionview_TotalStaffs.delegate = self
        uicollectionview_TotalStaffs.dataSource = self
        
  // uicollectionview_TotalStudents.register(TotalStudentsCollectionViewCell.self, forCellWithReuseIdentifier: "TotalStudentsCollectionViewCell")
    
        
    }
    @objc func refresh(sender:AnyObject) {
        refreshControl.endRefreshing()
    }
    //load basic info of single school
    func loadBasicInfo(){
        
        if (!refresh){
            showHud("Loading...")
        }
        
        
        
        var paramater = [String:AnyObject]()
        let id=userDefaults.integer(forKey:"SuperId")
        let schoolId = userDefaults.integer(forKey: "schoolId")
        
        
        paramater = ["superUserId":id as AnyObject,"schoolId":schoolId as AnyObject]
        
        
        print(paramater)
        Alamofire.request(LibraryAPI.singleschoolbasicinfo, method: .post, parameters: paramater)
            
            .responseJSON(completionHandler: { (response) -> Void in
                print(response)
                if(response.result.isSuccess) {
                    let json = JSON(response.data)
                    let success = json["success"].boolValue
                    if (success){
                        let body = json["body"]
                        
                        //set basic info
                        self.label_CollegeName.text = body["name"].stringValue
                        self.label_Address.text = body["address"].stringValue
                        self.label_Phone.text = body["landlineNumber"].stringValue
                        self.label_Mobile.text = body["mobileNumber"].stringValue
                        self.label_Email.text = body["email"].stringValue
                        self.label_Websites.text = body["webAddress"].stringValue
                        self.label_Programs.text = body["programsOfSchoolOrCollege"].stringValue
                        self.label_Staffs.text = body["counter"]["totalStaffs"].stringValue
                        self.label_Students.text =  body["counter"]["totalStudents"].stringValue
                        self.label_Guardians.text = body["counter"]["totalGuardians"].stringValue
                        
                        self.loadAccoutInfo()
                        
                    }
                    self.hideHUD()
                } else{
                    self.hideHUD()
                    LibraryToast.notifyUser("ALERT", message:"Something Went Wrong Please Try Again" , vc: self)
                    
                }
                
                
                
            })
    }
    
    func loadAccoutInfo(){
        
        if (!refresh){
            showHud("Loading...")
        }
    
        var paramater = [String:AnyObject]()
        let id=userDefaults.integer(forKey:"teamId")
        
        
        paramater = ["teamId":id as AnyObject,"filterType":"yearly" as AnyObject]
        
        
        print(paramater)
        Alamofire.request(LibraryAPI.accountInfo, method: .post, parameters: paramater)
            
            .responseJSON(completionHandler: { (response) -> Void in
                print(response)
                if(response.result.isSuccess) {
                    let json = JSON(response.data)
                    let success = json["success"].boolValue
                    if (success){
                        let body = json["body"]
                        //setting piechart
                        self.due_amt.value = body["due_amt"].doubleValue
                        self.expenditure_amt.value = body["expenditure_amt"].doubleValue
                        self.purchase_amt.value = body["purchase_amt"].doubleValue
                          self.cash_collection.value = body["cash_collection"].doubleValue
                        self.setPieChart()
                        self.loadGradeWiseStudentData()
                    }
                    self.hideHUD()
                } else{
                    self.hideHUD()
                    LibraryToast.notifyUser("ALERT", message:"Something Went Wrong Please Try Again" , vc: self)
                }
                
                
                
            })
        
        
        
    }
    
    func loadGradeWiseStudentData(){
        
        if (!refresh){
            showHud("Loading...")
        }
        
        var paramater = [String:AnyObject]()
        let id=userDefaults.integer(forKey:"teamId")
        
        
        paramater = ["teamId":id as AnyObject,"year":"2076" as AnyObject]
        
        
        print(paramater)
        Alamofire.request(LibraryAPI.gradewisestudents, method: .post, parameters: paramater)
            
            .responseJSON(completionHandler: { (response) -> Void in
                print(response)
                if(response.result.isSuccess) {
            
                    let json = JSON(response.data)
                    let success = json["success"].boolValue
                    if (success){
                        self.gradewiseStudentData.removeAll()
                        self.gradewiseStudentData = json["body"].arrayValue
                        self.uicollectionview_TotalStudents.reloadData()
                       
                    }
                    self.hideHUD()
                    self.loadDepwiseStaffData()
                } else{
                    self.hideHUD()
                    LibraryToast.notifyUser("ALERT", message:"Something Went Wrong Please Try Again" , vc: self)
                    
                    
                }
                
                
                
            })
        
        
        
    }
    
    func loadStudentAttendance(){
        
        if (!refresh){
            showHud("Loading...")
        }
        
        var paramater = [String:AnyObject]()
        let id=userDefaults.integer(forKey:"teamId")
        
        
        paramater = ["teamId":id as AnyObject,"filterType":"yearly" as AnyObject]
        
        
        print(paramater)
        Alamofire.request(LibraryAPI.studentAttendance, method: .post, parameters: paramater)
            
            .responseJSON(completionHandler: { (response) -> Void in
                print(response)
                if(response.result.isSuccess) {
                    
                    let json = JSON(response.data)
                    let success = json["success"].boolValue
                    if (success){
                        self.studentAttendanceData.removeAll()
                        self.studentAttendanceData = json["body"].arrayValue
                        self.setBarchartStudentAttendance()
                        
                        
                    }
                    self.loadStaffAttendance()
                    self.hideHUD()
                } else{
                    self.hideHUD()
                    LibraryToast.notifyUser("ALERT", message:"Something Went Wrong Please Try Again" , vc: self)
                    
                    
                }
                
                
                
            })
        
        
        
    }
    func loadStaffAttendance(){
        
        if (!refresh){
            showHud("Loading...")
        }
        
        var paramater = [String:AnyObject]()
        let id=userDefaults.integer(forKey:"teamId")
        
        
        paramater = ["teamId":id as AnyObject,"filterType":"yearly" as AnyObject]
        
        
        print(paramater)
        Alamofire.request(LibraryAPI.staffAttendance, method: .post, parameters: paramater)
            
            .responseJSON(completionHandler: { (response) -> Void in
                print(response)
                if(response.result.isSuccess) {
                    
                    let json = JSON(response.data)
                    let success = json["success"].boolValue
                    if (success){
                        self.staffAttendanceData.removeAll()
                        self.staffAttendanceData = json["body"].arrayValue
                        self.setBarchartStaffAttendance()
                        
                        
                    }
                    self.hideHUD()
                } else{
                    self.hideHUD()
                    LibraryToast.notifyUser("ALERT", message:"Something Went Wrong Please Try Again" , vc: self)
                    
                    
                }
                
                
                
            })
        
        
        
    }
    
    func loadDepwiseStaffData(){
        
        if (!refresh){
            showHud("Loading...")
        }
        
        var paramater = [String:AnyObject]()
        let id=userDefaults.integer(forKey:"teamId")
        
        
        paramater = ["teamId":id as AnyObject]
        
        
        print(paramater)
        Alamofire.request(LibraryAPI.departwiseStaffs, method: .post, parameters: paramater)
            
            .responseJSON(completionHandler: { (response) -> Void in
                print(response)
                if(response.result.isSuccess) {
                    let json = JSON(response.data)
                    let success = json["success"].boolValue
                    if (success){
                        self.departmentWiseStaffData.removeAll()
                        self.departmentWiseStaffData = json["body"].arrayValue
                        self.uicollectionview_TotalStaffs.reloadData()
                    }
                    self.hideHUD()
                    self.loadStudentAttendance()
                } else{
                    self.hideHUD()
                    LibraryToast.notifyUser("ALERT", message:"Something Went Wrong Please Try Again" , vc: self)
                    
                    
                }
                
                
                
            })
        
        
        
    }
    
    func setPieChart(){
        due_amt.label = "Due Amt(%)"
      purchase_amt.label = "Purchase Amt(%)"
        expenditure_amt.label = "Expenditure Amt(%)"
        cash_collection.label = "Cash Collection(%)"
        piechart_Account.chartDescription?.text = ""
        piechart_Account.entryLabelColor = .black
        piechart_Account.usePercentValuesEnabled = true
        pieentires.removeAll()
        uiColorArray.removeAll()
        
        if due_amt.value != 0{
            pieentires.append(due_amt)
            uiColorArray.append(UIColor.blue)
        }
        if purchase_amt.value != 0{
            pieentires.append(purchase_amt)
            uiColorArray.append(UIColor.purple)
        }
        if expenditure_amt.value != 0{
            pieentires.append(expenditure_amt)
            uiColorArray.append(UIColor.red)
        }
        if cash_collection.value != 0{
            pieentires.append(cash_collection)
            uiColorArray.append(Color.total_present)
        }
        
        let dataset = PieChartDataSet(values: pieentires, label: nil)
        let pieobject = PieChartData(dataSet: dataset)
        dataset.colors = uiColorArray
        piechart_Account.data = pieobject
        piechart_Account.sizeToFit()
        piechart_Account.rotationEnabled = true
        
    }
    
    func setBarchartStudentAttendance(){
        barChart_StudentAttendance.noDataText = "You need to provide data for the chart."
        barChart_StudentAttendance.chartDescription?.text = ""
        //legend
        let legend = barChart_StudentAttendance.legend
        legend.enabled = false
        legend.horizontalAlignment = .right
        legend.verticalAlignment = .top
        legend.orientation = .horizontal
        legend.drawInside = true
        legend.yOffset = 7.0;
        legend.xOffset = 12.0;
        legend.yEntrySpace = 0.0;
        
        
        let xaxis = barChart_StudentAttendance.xAxis
        xaxis.drawGridLinesEnabled = true
        xaxis.labelPosition = .bottom
        xaxis.labelFont = UIFont.systemFont(ofSize: 10.0)
        xaxis.centerAxisLabelsEnabled = true
        xaxis.valueFormatter = IndexAxisValueFormatter(values:self.months)
        xaxis.granularity = 1
        
        
        let leftAxisFormatter = NumberFormatter()
        leftAxisFormatter.maximumFractionDigits = 1
        
        let yaxis = barChart_StudentAttendance.leftAxis
        yaxis.spaceTop = 0.15
        yaxis.axisMinimum = 0
        yaxis.drawGridLinesEnabled = false
        
        barChart_StudentAttendance.rightAxis.enabled = false
        barChart_StudentAttendance.noDataText = "You need to provide data for the chart."
        var dataEntries: [BarChartDataEntry] = []
        var dataEntries1: [BarChartDataEntry] = []
        var dataEntries2: [BarChartDataEntry] = []
        
        for i in 0..<self.studentAttendanceData.count{
            let item = self.studentAttendanceData[i]
//            self.months.append(self.studentAttendanceData[i]["month"].stringValue)
            let dataEntry = BarChartDataEntry(x: Double(i), yValues:  [item["absentMales"].doubleValue,item["presentMales"].doubleValue])
            dataEntries.append(dataEntry)
            let dataEntry1 = BarChartDataEntry(x: Double(i), yValues:  [item["absentFemales"].doubleValue,item["presentFemales"].doubleValue])
            dataEntries1.append(dataEntry1)
            let dataEntry2 = BarChartDataEntry(x: Double(i), yValues:  [item["absentOthers"].doubleValue,item["presentOthers"].doubleValue])
            dataEntries2.append(dataEntry2)
            
        }
        
   
        
        let chartDataSet = BarChartDataSet(values: dataEntries, label: "Male")
        let chartDataSet1 = BarChartDataSet(values: dataEntries1, label: "Female")
        let chartDataSet2 = BarChartDataSet(values: dataEntries2, label: "Other")
        chartDataSet.colors = maleColor
        chartDataSet1.colors = femaleColor
        chartDataSet2.colors = otherColor
        
        
        
        let dataSets: [BarChartDataSet] = [chartDataSet,chartDataSet1,chartDataSet2]
        
        
        //let chartData = BarChartData(dataSet: chartDataSet)
        
        let chartData = BarChartData(dataSets: dataSets)
        
        
        let groupSpace = 0.2275
        let barSpace = 0.03
        let barWidth = 0.2275
        
        // (0.3 + 0.05) * 2 + 0.3 = 1.00 -> interval per "group"
        
        //        let groupCount = self.months.count
        let startYear = 0
        
        
        chartData.barWidth = barWidth;
        barChart_StudentAttendance.xAxis.axisMinimum = Double(startYear)
        let gg = chartData.groupWidth(groupSpace: groupSpace, barSpace: barSpace)
        print("Groupspace: \(gg)")
        barChart_StudentAttendance.xAxis.axisMaximum =  Double(startYear) + gg * Double(months.count)
        
        chartData.groupBars(fromX: Double(startYear), groupSpace: groupSpace, barSpace: barSpace)
        //        chartData.groupWidth(groupSpace: groupSpace, barSpace: barSpace)
        barChart_StudentAttendance.data = chartData
        barChart_StudentAttendance.zoomToCenter(scaleX: 2, scaleY: 0)
    }
    
    func setBarchartStaffAttendance(){
        barChart_StaffAttendance.noDataText = "You need to provide data for the chart."
        barChart_StaffAttendance.chartDescription?.text = ""
        //legend
        let legend = barChart_StaffAttendance.legend
        legend.enabled = false
        legend.horizontalAlignment = .right
        legend.verticalAlignment = .top
        legend.orientation = .horizontal
        legend.drawInside = true
        legend.yOffset = 7.0;
        legend.xOffset = 12.0;
        legend.yEntrySpace = 0.0;
        
        
        let xaxis = barChart_StaffAttendance.xAxis
        xaxis.drawGridLinesEnabled = true
        xaxis.labelPosition = .bottom
        xaxis.labelFont = UIFont.systemFont(ofSize: 10.0)
        xaxis.centerAxisLabelsEnabled = true
        xaxis.valueFormatter = IndexAxisValueFormatter(values:self.months)
        xaxis.granularity = 1
        
        
        let leftAxisFormatter = NumberFormatter()
        leftAxisFormatter.maximumFractionDigits = 1
        
        let yaxis = barChart_StaffAttendance.leftAxis
        yaxis.spaceTop = 0.15
        yaxis.axisMinimum = 0
        yaxis.drawGridLinesEnabled = false
        
        barChart_StaffAttendance.rightAxis.enabled = false
        barChart_StaffAttendance.noDataText = "You need to provide data for the chart."
        var dataEntries: [BarChartDataEntry] = []
        var dataEntries1: [BarChartDataEntry] = []
        var dataEntries2: [BarChartDataEntry] = []
        
        for i in 0..<self.staffAttendanceData.count{
            let item = self.staffAttendanceData[i]
            //            self.months.append(self.studentAttendanceData[i]["month"].stringValue)
            let dataEntry = BarChartDataEntry(x: Double(i), yValues:  [item["absentMales"].doubleValue,item["presentMales"].doubleValue])
            dataEntries.append(dataEntry)
            let dataEntry1 = BarChartDataEntry(x: Double(i), yValues:  [item["absentFemales"].doubleValue,item["presentFemales"].doubleValue])
            dataEntries1.append(dataEntry1)
            let dataEntry2 = BarChartDataEntry(x: Double(i), yValues:  [item["absentOthers"].doubleValue,item["presentOthers"].doubleValue])
            dataEntries2.append(dataEntry2)
            
        }
        
        
        
        let chartDataSet = BarChartDataSet(values: dataEntries, label: "Male")
        let chartDataSet1 = BarChartDataSet(values: dataEntries1, label: "Female")
        let chartDataSet2 = BarChartDataSet(values: dataEntries2, label: "Other")
        chartDataSet.colors = maleColor
        chartDataSet1.colors = femaleColor
        chartDataSet2.colors = otherColor
        
        
        
        let dataSets: [BarChartDataSet] = [chartDataSet,chartDataSet1,chartDataSet2]
        
        
        //let chartData = BarChartData(dataSet: chartDataSet)
        
        let chartData = BarChartData(dataSets: dataSets)
        
        
        let groupSpace = 0.2275
        let barSpace = 0.03
        let barWidth = 0.2275
        
        // (0.3 + 0.05) * 2 + 0.3 = 1.00 -> interval per "group"
        
        //        let groupCount = self.months.count
        let startYear = 0
        
        
        chartData.barWidth = barWidth;
        barChart_StaffAttendance.xAxis.axisMinimum = Double(startYear)
        let gg = chartData.groupWidth(groupSpace: groupSpace, barSpace: barSpace)
        print("Groupspace: \(gg)")
        barChart_StaffAttendance.xAxis.axisMaximum =  Double(startYear) + gg * Double(months.count)
        
        chartData.groupBars(fromX: Double(startYear), groupSpace: groupSpace, barSpace: barSpace)
        //        chartData.groupWidth(groupSpace: groupSpace, barSpace: barSpace)
        barChart_StaffAttendance.data = chartData
        barChart_StaffAttendance.zoomToCenter(scaleX: 2, scaleY: 0)
    }
   
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension DashboardViewController:UICollectionViewDelegate,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if (collectionView == uicollectionview_TotalStudents){
            return gradewiseStudentData.count
        }else{
             return departmentWiseStaffData.count
        }
       
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == uicollectionview_TotalStudents{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifierTotalStudent, for: indexPath) as! TotalStudentsCollectionViewCell
            let item = gradewiseStudentData[indexPath.row]
            cell.label_ClassName.text = item["gradeName"].stringValue
            male.value = item["totalMales"].doubleValue
            female.value = item["totalFemales"].doubleValue
            other.value = item["totalOthers"].doubleValue
            cell.label_Male.text = item["totalMales"].stringValue
            cell.label_Female.text = item["totalFemales"].stringValue
            cell.label_Other.text = item["totalOthers"].stringValue
            male.label = "Male(%)"
            female.label = "Female(%)"
            other.label = "Other(%)"
            cell.piechart_Students.chartDescription?.text = ""
            cell.piechart_Students.usePercentValuesEnabled = true
            cell.piechart_Students.drawEntryLabelsEnabled = false
            pieentiresTotalStudents.removeAll()
            uiColorArrayTotalStudents.removeAll()
            
            if male.value != 0{
                pieentiresTotalStudents.append(male)
                uiColorArrayTotalStudents.append(Color.male)
            }
            if female.value != 0{
                pieentiresTotalStudents.append(female)
                uiColorArrayTotalStudents.append(Color.female)
            }
            if other.value != 0{
                pieentiresTotalStudents.append(other)
                uiColorArrayTotalStudents.append(Color.other)
            }
            let dataset = PieChartDataSet(values: pieentiresTotalStudents, label: nil)
            let pieobject = PieChartData(dataSet: dataset)
            dataset.entryLabelColor = UIColor.black
            dataset.colors = uiColorArrayTotalStudents
            cell.piechart_Students.data = pieobject
            cell.piechart_Students.sizeToFit()
            cell.piechart_Students.rotationEnabled = true
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier:"TotalStaffsCollectionViewCell", for: indexPath) as! TotalStaffsCollectionViewCell
            let item = departmentWiseStaffData[indexPath.row]
            cell.label_DepartName.text = item["departmentName"].stringValue
            male.value = item["totalMales"].doubleValue
            female.value = item["totalFemales"].doubleValue
            other.value = item["totalOthers"].doubleValue
            cell.label_Male.text = item["totalMales"].stringValue
            cell.label_Female.text = item["totalFemales"].stringValue
            cell.label_Other.text = item["totalOthers"].stringValue
            male.label = "Male(%)"
            female.label = "Female(%)"
            other.label = "Other(%)"
            cell.piechart_Students.chartDescription?.text = ""
            cell.piechart_Students.usePercentValuesEnabled = true
            cell.piechart_Students.drawEntryLabelsEnabled = false
            pieentiresTotalStudents.removeAll()
            uiColorArrayTotalStudents.removeAll()
            
            if male.value != 0{
                pieentiresTotalStudents.append(male)
                uiColorArrayTotalStudents.append(Color.male)
            }
            if female.value != 0{
                pieentiresTotalStudents.append(female)
                uiColorArrayTotalStudents.append(Color.female)
            }
            if other.value != 0{
                pieentiresTotalStudents.append(other)
                uiColorArrayTotalStudents.append(Color.other)
            }
            let dataset = PieChartDataSet(values: pieentiresTotalStudents, label: nil)
            let pieobject = PieChartData(dataSet: dataset)
            dataset.entryLabelColor = UIColor.black
            dataset.colors = uiColorArrayTotalStudents
            cell.piechart_Students.data = pieobject
            cell.piechart_Students.sizeToFit()
            cell.piechart_Students.rotationEnabled = false
            return cell
        }
   
        
    }
    
    
}
