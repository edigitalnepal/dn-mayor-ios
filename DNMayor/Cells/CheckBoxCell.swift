
//  CheckBoxCell.swift
//  DNFounder
//
//  Created by mac on 8/12/19.
//  Copyright © 2019 E-Digital Nepal. All rights reserved.
//

import UIKit

class CheckBoxCell: UITableViewCell {
    

    @IBOutlet weak var check_btn: UIButton!
    var s = false
    @IBOutlet var l_Value: UILabel!
    let selectedimgae: UIImage = UIImage(named: "check")!
    let unselectedimgae: UIImage = UIImage(named: "uncheck")!
    var btn_yes : (() -> Void)? = nil
    var btn_no : (() -> Void)? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    @IBAction func btnclick(_ sender: Any) {
        if s {
            s = false
            check_btn.setImage(unselectedimgae, for: .normal)
            if let absentClick = self.btn_no
            {
                absentClick()
            }
        }else{
            s = true
            check_btn.setImage(selectedimgae, for: .normal)
            if let presentClick = self.btn_yes
            {
                presentClick()
            }
            
        }
    }
    
}



