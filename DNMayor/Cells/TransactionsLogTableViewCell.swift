//
//  TransactionsLogTableViewCell.swift
//  DNFounder
//
//  Created by mac on 8/8/19.
//  Copyright © 2019 E-Digital Nepal. All rights reserved.
//

import UIKit

class TransactionsLogTableViewCell: UITableViewCell {
    @IBOutlet weak var l_first: UITextView!
    
    @IBOutlet weak var l_sixth: UITextView!
    @IBOutlet weak var l_seventh: UITextView!
    @IBOutlet weak var l_fifth: UITextView!
    @IBOutlet weak var l_fourth: UITextView!
    @IBOutlet weak var l_third: UITextView!
    @IBOutlet weak var l_second: UITextView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

