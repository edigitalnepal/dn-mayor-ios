//
//  NoticeCell.swift
//  DNFounder
//
//  Created by Er. Aditya Raj on 5/13/18.
//  Copyright © 2018 Digital Nepal. All rights reserved.
//

import UIKit

class NoticeCell: UITableViewCell {

    @IBOutlet weak var textview_Notice: UITextView!
    @IBOutlet weak var label_Date: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
