//
//  EmployeeAttendanceCell.swift
//  DNFounder
//
//  Created by Er. Aditya Raj on 7/6/18.
//  Copyright © 2018 Digital Nepal. All rights reserved.
//

import UIKit

class EmployeeAttendanceCell: UITableViewCell {
    @IBOutlet weak var name: UILabel!
    
    @IBOutlet weak var status: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
