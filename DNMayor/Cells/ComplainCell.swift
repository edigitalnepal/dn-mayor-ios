//
//  ComplainCell.swift
//  DNFounder
//
//  Created by Er. Aditya Raj on 7/5/18.
//  Copyright © 2018 Digital Nepal. All rights reserved.
//

import UIKit

class ComplainCell: UITableViewCell {

    @IBOutlet weak var label_Status: UILabel!
    @IBOutlet weak var label_By: UILabel!
    @IBOutlet weak var label_Date: UILabel!
    @IBOutlet weak var label_Title: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
