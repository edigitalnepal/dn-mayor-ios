//
//  TotalStudentsCollectionViewCell.swift
//  DNFounder
//
//  Created by digital nepal on 3/24/19.
//  Copyright © 2019 E-Digital Nepal. All rights reserved.
//

import UIKit
import Charts

class TotalStudentsCollectionViewCell: UICollectionViewCell {
    @IBOutlet var label_Other: UILabel!
    
    @IBOutlet var piechart_Students: PieChartView!
    @IBOutlet var label_ClassName: UILabel!
    @IBOutlet var label_Female: UILabel!
    @IBOutlet var label_Male: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
