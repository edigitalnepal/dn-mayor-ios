//
//  DaybookCashbook.swift
//  DNMayor
//
//  Created by mac on 8/19/19.
//  Copyright © 2019 E-Digital Nepal. All rights reserved.
//

import UIKit

class DaybookCashbook: UITableViewCell {
    @IBOutlet weak var label_Particular: UITextView!
    
    @IBOutlet weak var label_Amount: UITextView!
    @IBOutlet weak var label_Narration: UITextView!
    @IBOutlet weak var label_Date: UITextView!
    @IBOutlet weak var label_Sn: UITextView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
