//
//  HeaderViewCell.swift
//  DNFounder
//
//  Created by mac on 8/5/19.
//  Copyright © 2019 E-Digital Nepal. All rights reserved.
//

import UIKit

class HeaderViewCell: UITableViewCell {
    
    @IBOutlet weak var l_Header: UILabel!
    
    @IBOutlet weak var btn_ShowHide: UIButton!
    
    let up = UIImage(named: "up") as UIImage?
    let drop = UIImage(named: "drop") as UIImage?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setExapanded(){
        btn_ShowHide.setImage(up, for: .normal)
        
    }
    
    func setCollapsed(){
        btn_ShowHide.setImage(drop, for: .normal)
    }
    
}
