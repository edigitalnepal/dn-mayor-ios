//
//  LeftMenuCellTableViewCell.swift
//  DNFounder
//
//  Created by Er. Aditya Raj on 5/14/18.
//  Copyright © 2018 Digital Nepal. All rights reserved.
//

import UIKit

class LeftMenuCellTableViewCell: UITableViewCell {
    @IBOutlet weak var image_MenuImage: UIImageView!
    
    @IBOutlet weak var label_Name: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
