//
//  BankTransactionTC.swift
//  DNMayor
//
//  Created by mac on 8/19/19.
//  Copyright © 2019 E-Digital Nepal. All rights reserved.
//

import UIKit

class BankTransactionTC: UITableViewCell {

    @IBOutlet weak var l_Amount: UITextView!
    @IBOutlet weak var l_BankAccount: UITextView!
    @IBOutlet weak var l_BankName: UITextView!
    @IBOutlet weak var l_ConductedBy: UITextView!
    @IBOutlet weak var l_Transactions: UITextView!
    @IBOutlet weak var l_Date: UITextView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

