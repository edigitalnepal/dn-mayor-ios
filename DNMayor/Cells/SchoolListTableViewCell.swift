//
//  SchoolListTableViewCell.swift
//  DNFounder
//
//  Created by digital nepal on 3/22/19.
//  Copyright © 2019 E-Digital Nepal. All rights reserved.
//

import UIKit

class SchoolListTableViewCell: UITableViewCell {
    @IBOutlet var image_SchoolImage: UIImageView!
    
    @IBOutlet var t_address: UILabel!
    @IBOutlet var l_schoolname: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
