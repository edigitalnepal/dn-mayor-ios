//
//  StudentListCell.swift
//  DNFounder
//
//  Created by Er. Aditya Raj on 7/2/18.
//  Copyright © 2018 Digital Nepal. All rights reserved.
//

import UIKit

class StudentListCell: UITableViewCell {

    @IBOutlet weak var student_Phone: UILabel!
    @IBOutlet weak var student_Id: UILabel!
    @IBOutlet weak var student_Name: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setColor(bool:Bool){
        if bool{
         student_Phone.textColor = Color.pass
            student_Name.textColor = Color.pass
            student_Id.textColor = Color.pass
            
        }else{
            student_Phone.textColor = Color.total_absent_student
            student_Name.textColor = Color.total_absent_student
            student_Id.textColor = Color.total_absent_student
        }
    }
    
}
