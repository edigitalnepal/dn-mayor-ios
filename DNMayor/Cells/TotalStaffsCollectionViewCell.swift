//
//  TotalStaffsCollectionViewCell.swift
//  DNFounder
//
//  Created by digital nepal on 3/25/19.
//  Copyright © 2019 E-Digital Nepal. All rights reserved.
//

import UIKit
import Charts

class TotalStaffsCollectionViewCell: UICollectionViewCell {
    @IBOutlet var label_Other: UILabel!
    
    @IBOutlet var piechart_Students: PieChartView!
    @IBOutlet var label_DepartName: UILabel!
    @IBOutlet var label_Female: UILabel!
    @IBOutlet var label_Male: UILabel!
    
}
