//
//  LeftMenuContorllerCell.swift
//  DNFounder
//
//  Created by Er. Aditya Raj on 5/11/18.
//  Copyright © 2018 Digital Nepal. All rights reserved.
//

import UIKit

class LeftMenuContorllerCell: UITableViewCell {
    

    @IBOutlet weak var imageView_menuIcon: UIImageView!
    @IBOutlet weak var label_menuName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
