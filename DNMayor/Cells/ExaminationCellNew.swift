//
//  ExaminationCellNew.swift
//  DNFounder
//
//  Created by Er. Aditya Raj on 6/28/18.
//  Copyright © 2018 Digital Nepal. All rights reserved.
//

import UIKit

class ExaminationCellNew: UITableViewCell {

    @IBOutlet weak var result_publish: UILabel!
    @IBOutlet weak var end_date: UILabel!
    @IBOutlet weak var startDate: UILabel!
    @IBOutlet weak var exam_title: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
