//
//  ExaminationCell.swift
//  DNFounder
//
//  Created by Er. Aditya Raj on 5/16/18.
//  Copyright © 2018 Digital Nepal. All rights reserved.
//

import UIKit
import  Charts

class ExaminationCell: UITableViewCell {
    
    @IBOutlet weak var label_totalstuden: UILabel!
    @IBOutlet weak var label_examatn: UILabel!

    
    @IBOutlet weak var label_ExamName: UILabel!
    
    @IBOutlet weak var label_Present: UILabel!
    @IBOutlet weak var label_Absent: UILabel!
    @IBOutlet weak var barview: BarChartView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        label_totalstuden.textColor = Color.Blue
        label_Present.textColor = Color.total_present
        label_Absent.textColor = Color.total_absent_student
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setBarChart(point:[Double],string:[String]){
        barview.noDataText = "No Data Available"
        barview.chartDescription?.text = ""
        var dataentryset:[BarChartDataEntry] = []
        var counter = 0.0
        for i in 0..<point.count{
            counter += 1.0
            let dataEntry = BarChartDataEntry(x: counter, y: point[i])
            dataentryset.append(dataEntry)
            
            
        }
        let chardDataset = BarChartDataSet(values: dataentryset, label: nil)
        let chartData = BarChartData()
        chartData.addDataSet(chardDataset)
        barview.data = chartData
        chardDataset.colors = [Color.Blue,Color.total_absent_student,Color.total_present]
        barview.animate(xAxisDuration: 1.0, yAxisDuration: 1.0)
     
        
        
    }
    
}
