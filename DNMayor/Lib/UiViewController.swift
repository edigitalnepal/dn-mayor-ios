//
//  UiViewController.swift
//  DNFounder
//
//  Created by Er. Aditya Raj on 5/11/18.
//  Copyright © 2018 Digital Nepal. All rights reserved.
//

import UIKit
import MBProgressHUD

extension UIViewController {

    func setNavigationBarItem() {
        self.addLeftBarButtonWithImage(UIImage(named: "menu.png")!)
        //        self.addRightBarButtonWithImage(UIImage(named: "ic_notifications_black_24dp")!)
        self.slideMenuController()?.removeLeftGestures()
        self.slideMenuController()?.removeRightGestures()
        self.slideMenuController()?.addLeftGestures()
        self.slideMenuController()?.addRightGestures()
    }

    func removeNavigationBarItem() {
        self.navigationItem.leftBarButtonItem = nil
        self.navigationItem.rightBarButtonItem = nil
        self.slideMenuController()?.removeLeftGestures()
        self.slideMenuController()?.removeRightGestures()
    }
}
extension UIViewController {
   func showHud(_ message: String) {
    let loadingNotification = MBProgressHUD.showAdded(to: (self.view)!, animated: true)
     loadingNotification.mode = MBProgressHUDMode.indeterminate
      loadingNotification.label.text = "Loading"
       loadingNotification.bezelView.color = UIColor.clear
      loadingNotification.contentColor = Color.Blue
     loadingNotification.backgroundColor = UIColor.clear
       loadingNotification.isUserInteractionEnabled = false
 }
  func hideHUD() {
      MBProgressHUD.hide(for: self.view, animated: true)
   }
}
