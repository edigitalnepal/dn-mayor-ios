//
//  ApiRequest.swift
//  DNFounder
//
//  Created by Er. Aditya Raj on 5/13/18.
//  Copyright © 2018 Digital Nepal. All rights reserved.
//

import Foundation
import Alamofire
import MBProgressHUD
import UIKit

class APIRequest: NSObject {
    
    
    static func request(self_View:UIView? = nil,showProgressHud:Bool? = false,method: Alamofire.HTTPMethod, URLString: URLConvertible, parameters: [String: AnyObject]?, completionHandler: @escaping (_ status : Bool, _ message : AnyObject, _ data : AnyObject) -> Void) {
        // print("Request URL: \(urlString.URLString)")
        
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.alpha = 0.3
        blurEffectView.frame = (self_View?.bounds)!
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self_View?.addSubview(blurEffectView)
        
        if self_View != nil && showProgressHud != nil {
            let loadingNotification = MBProgressHUD.showAdded(to: (self_View)!, animated: true)
            loadingNotification.mode = MBProgressHUDMode.indeterminate
            loadingNotification.label.text = "Loading"
            loadingNotification.bezelView.color = UIColor.clear
            loadingNotification.contentColor = Color.Blue
            loadingNotification.backgroundColor = UIColor.clear
            loadingNotification.isUserInteractionEnabled = false
        }
        
        
        
        Alamofire.request(URLString, method: method, parameters: parameters)
            .responseJSON(completionHandler: { (response) -> Void in
                
                
                print(URLString)
                print(response)
                MBProgressHUD.hide(for: self_View!, animated: true)
                
                if(response.result.isSuccess) {
                    
                    if let success = (response.result.value! as! NSDictionary).value(forKey: "success") {
                        
                        let data = (response.result.value! as! NSDictionary).value(forKey: "data")
                        if (success as! Bool) {
                            MBProgressHUD.hide(for: self_View!, animated: true)
                            blurEffectView.isHidden = true
                            
                            completionHandler(true, (response.result.value as! NSDictionary).value(forKey: "Message") as AnyObject, data as AnyObject)
                        }
                        else {
                            MBProgressHUD.hide(for: self_View!, animated: true)
                            blurEffectView.isHidden = true
                            
                            
                            if let errorMessage = (response.result.value as! NSDictionary).value(forKey: "error") {
                                completionHandler(false, errorMessage as AnyObject, data as AnyObject)
                            }
                            else {
                                
                                MBProgressHUD.hide(for: self_View!, animated: true)
                                blurEffectView.isHidden = true
                                
                                completionHandler(false, (response.result.value as! NSDictionary).value(forKey: "Message") as AnyObject, data as AnyObject)
                                
                            }
                        }
                    }
                }
                else {
                    //        print((response.result.value!))
                    //
                    
                    blurEffectView.isHidden = true
                    
                    if self_View != nil && showProgressHud != nil {
                        MBProgressHUD.hide(for: self_View!, animated: true)
                        
                    }
                    
                    completionHandler(false, response.result.error!.localizedDescription as AnyObject, response.result.error!.localizedDescription as AnyObject )
                }
            })
    }
    
    static func request1(self_View:UIView? = nil,showProgressHud:Bool? = false,method: Alamofire.HTTPMethod, URLString: URLConvertible, parameters: [String: AnyObject]?, completionHandler: @escaping (_ status : Bool, _ message : AnyObject, _ data : AnyObject) -> Void) {
        // print("Request URL: \(urlString.URLString)")
        
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.alpha = 0.3
        blurEffectView.frame = (self_View?.bounds)!
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self_View?.addSubview(blurEffectView)
        
        if self_View != nil && showProgressHud != nil {
            let loadingNotification = MBProgressHUD.showAdded(to: (self_View)!, animated: true)
            loadingNotification.mode = MBProgressHUDMode.indeterminate
            loadingNotification.label.text = "Loading"
            loadingNotification.bezelView.color = UIColor.clear
            loadingNotification.contentColor = Color.Blue
            loadingNotification.isUserInteractionEnabled = false
        }
        
        print(parameters)
        
        Alamofire.request(URLString, method: method, parameters: parameters)
            .responseJSON(completionHandler: { (response) -> Void in
                
                
                print(URLString)
                print(response)
                if(response.result.isSuccess) {
                    
                    if let success = (response.result.value! as! NSDictionary).value(forKey: "Success") {
                        
                        let data = (response.result.value! as! NSDictionary)
                        if (success as! Bool) {
                            MBProgressHUD.hide(for: self_View!, animated: true)
                            blurEffectView.isHidden = true
                            
                            completionHandler(true, (response.result.value as! NSDictionary).value(forKey: "Message") as AnyObject, data as AnyObject)
                        }
                        else {
                            MBProgressHUD.hide(for: self_View!, animated: true)
                            blurEffectView.isHidden = true
                            
                            if let errorMessage = (response.result.value as! NSDictionary).value(forKey: "error") {
                                completionHandler(false, errorMessage as AnyObject, data as AnyObject)
                            }
                            else {
                                
                                MBProgressHUD.hide(for: self_View!, animated: true)
                                blurEffectView.isHidden = true
                                
                                completionHandler(false, (response.result.value as! NSDictionary).value(forKey: "Message") as AnyObject, data as AnyObject)
                            }
                        }
                    }
                }
                else {
                    
                    blurEffectView.isHidden = true
                    
                    if self_View != nil && showProgressHud != nil {
                        MBProgressHUD.hide(for: self_View!, animated: true)
                        
                    }
                    
                    completionHandler(false, response.result.error!.localizedDescription as AnyObject, response.result.error!.localizedDescription as AnyObject )
                }
            })
    }
}



class UserValues: NSObject {
    
    var userId = ""
}

class LibraryAPI: NSObject {
    
    // Realtime URL
//    static let baseURL: String = "http://edigitalnepal.com/sms/apis/v2"
    static let baseURL: String = "https://edigitalnepal.com/mis/apis/v2/"
    static let baseURLF: String = "https://edigitalnepal.com/mis/apis/v2/founder/"
    //demo
//    static let baseURLF: String = "http://202.51.74.174/demo/apis/v2/"
    
    //tomcat
//    static let baseURL: String = "http://tomcat.edigitalnepal.com/sms/apis/staff/"
//    static let baseURLF: String = "http://tomcat.edigitalnepal.com/sms/apis/v2/founder/"
    
    
    //    static let baseURL: String = ""
    static let loginUrl : String = baseURL + "superuser/login"
    static let NoticUrl:String = baseURL+"founder/notice/findbyteamid"
    static let attendanceUrl = baseURL+"founder/todaysattendancereport"
    static let dailyRoutineUrl = baseURL+"founder/dailyroutine/findbyteamid"
    static let inventoryURl = baseURL+"founder/findinventoryreport"
    static let examURl = baseURL+"founder/exam/findall"
    static let accountdetails = baseURL+"founder/academicaccountsreport"
    static let findAllgrades = baseURL+"founder/findallgrades"
    static let examfullview = baseURL+"founder/exam/viewreport"
    static let todaysCollection = baseURL+"founder/gettodayscollection"
    static let complainapi = baseURL+"founder/casecomplain/findall"
    static let leaveurl = baseURL+"founder/employee/leaveapplication/findall"
    static let takeAction = baseURL+"founder/employee/leaveapplication/takeaction"
    static let emoloyeeAtn = baseURL+"founder/employee/attendance/todays"
    static let superUserBasicInfo = baseURL+"superuser/basicinfo/fetch"
    static let fetchSchoollist = baseURL+"superuser/associatedschools/fetchall"
    static let singleschoolbasicinfo = baseURL+"superuser/associatedschool/basicinfo/fetch"
    static let accountInfo = baseURL+"superuser/associatedschool/accounts/basicinfo"
    static let gradewisestudents = baseURL+"superuser/associatedschool/student/gradewise/count"
    static let departwiseStaffs = baseURL+"superuser/associatedschool/staff/departmentwise/count"
    static let studentAttendance = baseURL+"superuser/associatedschool/student/attendancereport/fetch"
    static let staffAttendance = baseURL+"superuser/associatedschool/employee/attendancereport/fetch"
    static let staffOveralAttendance = baseURL+"superuser/associatedschools/employee/attendancereport/fetch"
     static let studentOveralAttendance = baseURL+"superuser/associatedschools/student/attendancereport/fetch"
    static let datetype = baseURL+"common/settings/fetch"
    static let fetchBatch = baseURL+"common/batch/findallbydaterange"
    static let feeParticular = baseURL+"common/feeparticular/filterbybatch"
    static let newAccountReport = baseURLF+"accounts/academic/report/fetch"
    static let daybook = baseURL+"accounts/report/populatedaybookreport"
    static let transactionsLog = baseURL+"accounts/report/populatetransactionlog"
    
 
  
   
}
class LibraryToast:NSObject{
    
    static   func notifyUser(_ title: String, message: String, vc: UIViewController) -> Void
    {
        let alert = UIAlertController(title: title,
                                      message: message,
                                      preferredStyle: UIAlertControllerStyle.alert)
        
        let cancelAction = UIAlertAction(title: "OK",
                                         style: .cancel, handler: nil)
        
        alert.addAction(cancelAction)
        //vc will be the view controller on which you will present your alert as you cannot use self because this method is static.
        vc.present(alert, animated: true, completion: nil)
    }
    
}
class Color:NSObject{
    static let zeroLevel: UIColor = UIColor(hexString: "FF6347")
    static let oneLevel: UIColor = UIColor(hexString: "FF7F50")
    static let twoLevel: UIColor = UIColor(hexString: "FFA500")
    static let threeLevel: UIColor = UIColor(hexString: "90EE90")
    static let fourLelvel :UIColor = UIColor(hexString:"4169E1")
    static let fiveLevel :UIColor = UIColor(hexString:"7B68EE")
    static let sixLevel :UIColor = UIColor(hexString:"FFDEAD")
    static let GreenLight :UIColor = UIColor(hexString:"e0f8ea")
    static let Blue :UIColor = UIColor(hexString:"#2874f0")
    static let BlueLight :UIColor = UIColor(hexString:"e5eff8")
    static let Yellow:UIColor = UIColor(hexString:"e7ac44")
    static let YellowLight:UIColor = UIColor(hexString:"fcf3e2")
    static let pariyojanaDetail:UIColor = UIColor(hexString:"edd5e8")
    static let swikrit:UIColor = UIColor(hexString:"99DE9B")
    static let aaswikrit:UIColor = UIColor(hexString:"F6ADB6")
    static let gunasoExpired:UIColor = UIColor(hexString:"#FFA07A")
    static let lightGreay:UIColor = UIColor(hexString:"#D3D3D3")
     static let colorFb:UIColor = UIColor(hexString: "#3B5998")
    static let loginSkyColor:UIColor = UIColor(hexString: "#81BFF0")
    //color Attendance
    static let total_absent_student:UIColor = UIColor(hexString:"F00")
    static let total_attendance_done:UIColor = UIColor(hexString:"F6ADB6")
    static let total_classes:UIColor = UIColor(hexString:"#FFA07A")
    static let total_present:UIColor = UIColor(hexString:"#006400")
    static let total_students:UIColor = UIColor(hexString: "#FB6542")
    static let purchase:UIColor = UIColor(hexString: "#FF420E")
    static let salary:UIColor = UIColor(hexString: "#80BD93")
    static let expenduter:UIColor = UIColor(hexString: "#89DA59")
    //color account
    static let penalty:UIColor = UIColor(hexString:"#000000")
    static let disscount:UIColor = UIColor(hexString: "#9C27B0")
    static let receivedamt:UIColor = UIColor(hexString: "#ffBC75")
    static let schoolarship:UIColor = UIColor(hexString: "#4CAF50")
    static let dueamount:UIColor = UIColor(hexString: "#FF0000")
    static let receiableamt:UIColor = UIColor(hexString: "#3AAFFF")
    //exam color
    static let totalstuden:UIColor = UIColor(hexString:"#000000")
    static let distinction:UIColor = UIColor(hexString: "#9C27B0")
    static let first:UIColor = UIColor(hexString: "#ffBC75")
    static let second:UIColor = UIColor(hexString: "#4CAF50")
    static let third:UIColor = UIColor(hexString: "#FF0000")
    static let examattend:UIColor = UIColor(hexString: "#3AAFFF")
    static let failed:UIColor = UIColor(hexString: "#F6ADB6")
    static let pass:UIColor = UIColor(hexString: "#006400")
    static let male:UIColor = UIColor(hexString: "#725E55")
    static let female:UIColor = UIColor(hexString: "#A8B0C7")
    static let other:UIColor = UIColor(hexString: "#0087C9")
    
    
    
}
