//
//  DateInString.swift
//  DNFounder
//
//  Created by digital nepal on 8/21/18.
//  Copyright © 2018 E-Digital Nepal. All rights reserved.
//

import Foundation
class DateInString:NSObject{
    static func stringDate(date:String!) -> String{
        var array:[String] = []
        let splitter = date.components(separatedBy: "-")
        let monthenglish = splitter[1]
        print(monthenglish)
        let month:Int = DateConverter().convertADToBS(nepaliDate:date, spliter: "-").month
        let day:Int = DateConverter().convertADToBS(nepaliDate:date , spliter: "-").dayOfMonth
        let year:Int = DateConverter().convertADToBS(nepaliDate:date , spliter: "-").year
        array.append(String(year))
        array.append(String(month))
            array.append(String(day))
        return  array.joined(separator:"-")
    }
    static func oneMonthLate() -> String{
        var array:[String] = []
        
        if getCurrentMonthNepali() != 1{
            array.append(getCurrentYearNepali())
            array.append("\(getCurrentMonthNepali()-1)")
            array.append("\(getCurrentDayNepali())")
        }else{
            array.append("\(Int(getCurrentYearNepali())!-1)")
            array.append("\(12)")
            array.append("\(getCurrentDayNepali())")
        }
        
        
        
        return  array.joined(separator:"-")
    }
    static func getCurrentDateEng() -> String{
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy.MM.dd"
        let result = formatter.string(from: date)
        let dateinenglish = result.replace(".", with: "-")
        return dateinenglish
    }
    static func getCurrentDateNep() -> String{
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy.MM.dd"
        let result = formatter.string(from: date)
        print(result)
        
        let dateinenglish = result.replace(".", with: "-")
        let nepDate = stringDate(date: dateinenglish)
        return nepDate
    }
    static func getCurrentYearEng()->String{
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy.MM.dd"
        let result = formatter.string(from: date)
        let dateinenglish = result.replace(".", with: "-")
        let engdate = dateinenglish.components(separatedBy: "-")
        return engdate[0]
        
    }
    
    static func getCurrentYearNepali()->String{
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy.MM.dd"
        let result = formatter.string(from: date)
        let dateinenglish = result.replace(".", with: "-")
        let npdate = stringDate(date: dateinenglish)
        let engdate = npdate.components(separatedBy: "-")
        return engdate[0]
        
    }
    static func getCurrentMonthEng()->Int{
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy.MM.dd"
        let result = formatter.string(from: date)
        let dateinenglish = result.replace(".", with: "-")
        let engdate = dateinenglish.components(separatedBy: "-")
        return Int(engdate[1])!
        
    }
    static func getCurrentMonthNepali()->Int{
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy.MM.dd"
        let result = formatter.string(from: date)
        let dateinenglish = result.replace(".", with: "-")
        let npdate = stringDate(date: dateinenglish)
        let engdate = npdate.components(separatedBy: "-")
        return Int(engdate[1])!
        
    }
    
    static func getCurrentDayNepali()->String{
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy.MM.dd"
        let result = formatter.string(from: date)
        let dateinenglish = result.replace(".", with: "-")
        let npdate = stringDate(date: dateinenglish)
        let engdate = npdate.components(separatedBy: "-")
        return engdate[2]
        
    }
    
}


