//
//  Helper.swift
//  DNFounder
//
//  Created by Er. Aditya Raj on 5/11/18.
//  Copyright © 2018 Digital Nepal. All rights reserved.
//

import Foundation
import UIKit

class Helper: NSObject {
    static let Super: UIColor = UIColor(hexString: "ff5733")
    static let SuperLight: UIColor = UIColor(hexString: "ffbbad")
    static let Purple: UIColor = UIColor(hexString: "794ea7")
    static let PurpleLight: UIColor = UIColor(hexString: "eee0f9")
    static let Red :UIColor = UIColor(hexString:"bc4139")
    static let RedLight :UIColor = UIColor(hexString:"f8e3e2")
    static let Green :UIColor = UIColor(hexString:"158e3f")
    static let GreenLight :UIColor = UIColor(hexString:"e0f8ea")
    static let Blue :UIColor = UIColor(hexString:"5799d3")
    static let BlueLight :UIColor = UIColor(hexString:"e5eff8")
    static let Yellow:UIColor = UIColor(hexString:"e7ac44")
    static let YellowLight:UIColor = UIColor(hexString:"fcf3e2")
    static func showAlertViewController(view: UIViewController , title : String, message: String){
        
        let alertView = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: { (alert) in
            
        })
        alertView.addAction(action)
        view.present(alertView, animated: true, completion: nil)
    }
    
}

extension UIColor {
    convenience init(hexString: String) {
        let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt32()
        Scanner(string: hex).scanHexInt32(&int)
        let a, r, g, b: UInt32
        switch hex.characters.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (1, 1, 1, 0)
        }
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
}
