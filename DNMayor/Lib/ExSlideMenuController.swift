//
//  ExSlideMenuController.swift
//  DNFounder
//
//  Created by Er. Aditya Raj on 5/11/18.
//  Copyright © 2018 Digital Nepal. All rights reserved.
//

import UIKit

class ExSlideMenuController : SlideMenuController {
    
    override func isTagetViewController() -> Bool {
        if UIApplication.topViewController() != nil {
            //            if vc is SelectPlaceVC  {
            //                return true
            //            }
        }
        return false
    }
    
    override func track(_ trackAction: TrackAction) {
        switch trackAction {
        case .leftTapOpen: break
        //print("TrackAction: left tap open.")
        case .leftTapClose: break
        //print("TrackAction: left tap close.")
        case .leftFlickOpen: break
        //print("TrackAction: left flick open.")
        case .leftFlickClose: break
        //print("TrackAction: left flick close.")
        case .rightTapOpen: break
        //print("TrackAction: right tap open.")
        case .rightTapClose: break
        //print("TrackAction: right tap close.")
        case .rightFlickOpen: break
        //print("TrackAction: right flick open.")
        case .rightFlickClose: break
            //print("TrackAction: right flick close.")
        }
    }
}
