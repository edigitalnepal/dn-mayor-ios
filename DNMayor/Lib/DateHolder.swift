//
//  DateHolder.swift
//  DNFounder
//
//  Created by digital nepal on 8/21/18.
//  Copyright © 2018 E-Digital Nepal. All rights reserved.
//

import Foundation
class DateHolder {
    var year = Int();
    var month = Int()
    var dayOfMonth = Int()
    var dayOfWeek = Int()
    
    init(year : Int, month : Int, dayOfMonth : Int) {
        self.year = year
        self.month = month
        self.dayOfMonth = dayOfMonth
    }
    
    init(year : Int, month : Int, dayOfMonth : Int, dayOfWeek : Int) {
        self.year = year
        self.month = month
        self.dayOfMonth = dayOfMonth
        self.dayOfWeek = dayOfWeek
        
    }
    
    
}
