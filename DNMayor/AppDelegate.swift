//
//  AppDelegate.swift
//  DNFounder
//
//  Created by Er. Aditya Raj on 5/11/18.
//  Copyright © 2018 Digital Nepal. All rights reserved.
//
let appDelegate = UIApplication.shared.delegate as! AppDelegate
 let userDefaults = UserDefaults.standard
import UIKit
import UserNotifications
import IQKeyboardManagerSwift


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    let userDefaults = UserDefaults.standard
    var userType = Int()
    var udid : String? = ""
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
    //        let mainViewController = PagerTabViewController()
    var mainViewController = UIViewController()
    func createMenuView() {
        // create viewController code...
        
        
        var navigationBarAppearace = UINavigationBar.appearance()
        navigationBarAppearace.titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        
        UINavigationBar.appearance().tintColor = UIColor.white
        
        mainViewController = storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        var  leftViewController = storyboard.instantiateViewController(withIdentifier: "LeftViewController") as! LeftViewController
        let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)
           let slideMenuController = ExSlideMenuController(mainViewController: nvc, leftMenuViewController: leftViewController)
        
  
        self.window?.backgroundColor = UIColor.white
        self.window?.rootViewController = slideMenuController
        self.window?.makeKeyAndVisible()
        
        
        
      
    }


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
     
         IQKeyboardManager.shared.enable = true
        
        UINavigationBar.appearance().barTintColor = Color.Blue
        UINavigationBar.appearance().tintColor = .white
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        UINavigationBar.appearance().isTranslucent = false
        
        //hide backbutton text
        let BarButtonItemAppearance = UIBarButtonItem.appearance()
        BarButtonItemAppearance.setTitleTextAttributes([NSAttributedStringKey.foregroundColor: Color.Blue], for: .normal)
        let status:Bool = userDefaults.bool(forKey: "status")
        

        if status{
    
            

        }

        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

